<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.php">KARIKAL</a>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">		
		<li>
			<a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
		</li>
		<li class="dropdown">
			<a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-newspaper-o fa-fw"></i> Novedades<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?op=verNotas"> Ver Novedades</a>
				</li>
				<li>
					<a href="index.php?op=agregarNotas"> Agregar Novedades</a>
				</li>
			</ul>
		</li> 
		<li class="dropdown">
			<a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart fa-fw"></i> Productos <span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?op=verPortfolio"> Ver Productos</a>
				</li>
				<li>
					<a href="index.php?op=agregarPortfolio"> Agregar Productos</a>
				</li>
			</ul>
		</li>   
		<li class="dropdown">
			<a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bullhorn  fa-fw"></i> Slider<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?op=verSlider"> Ver Sliders</a>
				</li>
				<li>
					<a href="index.php?op=agregarSlider"> Agregar Sliders</a>
				</li>
			</ul>
 		</li>
 		<li class="dropdown">
			<a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bullhorn  fa-fw"></i> Testimonio<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?op=verTestimonio"> Ver Testimonios</a>
				</li>
				<li>
					<a href="index.php?op=agregarTestimonio"> Agregar Testimonios</a>
				</li>
			</ul>
 		</li>
		<li class="dropdown">
			<a href="#"  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa  fa-pencil"></i> Contenidos<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li>
					<a href="index.php?op=verContenidos"> Ver Contenidos</a>
				</li> 
			</ul>
		</li>
		<li style="display: none">
			<a href="index.php?op=verContacto"><i class="fa fa-comment fa-fw"></i> Consultas de Contacto</a>
		</li>
        <li>
            <a href="index.php?op=landing"><i class="fa fa-bullhorn fa-fw"></i> Landing</a>
        </li>
		<li>
			<a href="index.php?op=salir"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
		</li>
	</ul>
	<!-- /.navbar-top-links -->	

</nav>
