<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<title>Contacto :: Karikal</title>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
	function onSubmit(token) {
		document.getElementById("kari_form").submit();
	}
	</script>
</head>
<body>

	<?php include("inc/nav.inc.php") ?>
	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background11.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Contactate</div>
				<div class="title">Envianos tus consultas</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Contacto</li>
				</ol>
			</div> 
		</div> 
	</div> 

	<div class="section white no-padding-bottom">
		<div class="inner">
			<div class="container">
				<h3 class="red"><span>Contacto</span>Envianos tus consultas<small>contactate con nosotros</small></h3>
				<hr />
				

				<form id="kari_form" method="post" >

					<?php
					$error="none";

					if(isset($_POST["enviar"])) {
						$nombre= antihack($_POST["nombre"]);
						$email= antihack($_POST["email"]);
						$asunto= antihack($_POST["asunto"]);
						$telefono= antihack($_POST["telefono"]);
						$provincia= antihack($_POST["provincia"]);
						$localidad= antihack($_POST["localidad"]);
						$mensaje= antihack($_POST["mensaje"]);

						$mensajeFinal = "<b>Nombre</b>: ".$nombre." <br/>";
						$mensajeFinal .= "<b>Email</b>: ".$email."<br/>";
						$mensajeFinal .= "<b>Asunto</b>: ".$asunto." <br/>";
						$mensajeFinal .= "<b>Teléfono</b>: ".$telefono." <br/>";
						$mensajeFinal .= "<b>provincia</b>: ".$provincia." <br/>";
						$mensajeFinal .= "<b>Localidad</b>: ".$localidad." <br/>";
						$mensajeFinal .= "<b>Mensaje</b>: ".$mensaje."<br/>";

						if(isset($_POST["g-recaptcha-response"])){
							$captcha=$_POST["g-recaptcha-response"];
						}

						if(!$captcha){
							$error="block;color:red;padding:5px 0px";
						}

						$secretKey = "6Ldx5D0UAAAAANNV3xJgy6dXF32tcHV5fV1gQ9D8";
						$ip = $_SERVER['REMOTE_ADDR'];
						$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
						$responseKeys = json_decode($response,true);
						if(intval($responseKeys["success"]) !== 1) {
								//echo '<h2>You are spammer ! Get the @$%K out</h2>';
						} else {
							$mensajeFinal = "Gracias por tu mensaje de contacto, te dejamos la consulta que nos pasaste.<br/><b>Nombre:</b> $nombre<br/><b>Email:</b> $email<br/><b>Teléfono:</b> $telefono<br/><b>Localidad:</b> $localidad<br/><b>Mensaje:</b> $mensaje<br/>";
							$mensajeAdmin = "<br/><b>Nombre:</b> $nombre<br/><b>Email:</b> $email<br/><b>Teléfono:</b> $telefono<br/><b>Localidad:</b> $localidad<br/><b>Mensaje:</b> $mensaje<br/>";
							Enviar_User("Gracias por tu mensaje de contacto", $mensajeFinal, $email);
							Enviar_User_Admin("Nuevo Mensaje de Contacto", $mensajeAdmin, $email);


                      $receptor = "karikal_sanfco@karikal.com.ar"; // receptor
                      Enviar_User_Admin($asunto,$mensajeFinal,$receptor);
                      Enviar_User($asunto, $mensajeFinal, $email);
                  }

              }
              ?>
              <div class="row">
              	<div class="col-sm-4">
              		<div class="form-group">
              			<input type="text" name="nombre" placeholder="Nombre y Apellido" required />
              		</div>
              	</div>
              	<div class="col-sm-4">
              		<div class="form-group">
              			<input type="email" name="email" placeholder="Email" required />
              		</div>
              	</div>
              	<div class="col-sm-4">
              		<div class="form-group">
              			<input type="tel" name="telefono" placeholder="Celular / Teléfono" />
              		</div>
              	</div>
              	<div class="col-sm-6">
              		<div class="form-group">
              			<select name="provincia" id="provincia" placeholder="Provincia">
              				<option value="Buenos Aires">Buenos Aires</option>
              				<option value="Catamarca">Catamarca</option>
              				<option value="Chaco">Chaco</option>
              				<option value="Chubut">Chubut</option>
              				<option value="Córdoba">Córdoba</option>
              				<option value="Corrientes">Corrientes</option>
              				<option value="Entre Ríos">Entre Ríos</option>
              				<option value="Formosa">Formosa</option>
              				<option value="Jujuy">Jujuy</option>
              				<option value="La Pampa">La Pampa</option>
              				<option value="La Rioja">La Rioja</option>
              				<option value="Mendoza">Mendoza</option>
              				<option value="Misiones">Misiones</option>
              				<option value="Neuquén">Neuquén</option>
              				<option value="Río Negro">Río Negro</option>
              				<option value="Salta">Salta</option>
              				<option value="San Juan">San Juan</option>
              				<option value="San Luis">San Luis</option>
              				<option value="Santa Cruz">Santa Cruz</option>
              				<option value="Santa Fe">Santa Fe</option>
              				<option value="Santiago del Estero">Santiago del Estero</option>
              				<option value="Tierra del Fuego">Tierra del Fuego</option>
              				<option value="Tucumán">Tucumán</option>
              			</select>
              		</div>
              	</div>
              	<div class="col-sm-6">
              		<div class="form-group">
              			<input type="text" name="localidad" placeholder="Localidad" />
              		</div>
              	</div>
              	<div class="col-sm-12">
              		<div class="form-group">
              			<textarea name="mensaje" placeholder="Consulta" required rows="3"></textarea>
              		</div>
              	</div>
              	<div class="col-sm-12">
              		<h5 style="display: <?php echo $error; ?>">Error de validación</h5>
              		<div class="g-recaptcha" data-sitekey="6Ldx5D0UAAAAAInw_gy2FUglpuZvOwyhnLdlsqPb"></div><br/> 
              	</div>
              </div>
              <div class="form-group">
              	<button type="submit" name="enviar" class="button">Enviar consulta</button>
              </div>
          </form>
          <div class="row">
          	<?php Traer_Contenidos("contacto") ?>
          </div>
      </div>  
  </div> 
</div> 
<?php include("inc/footer.inc.php") ?>
</body>
</html>