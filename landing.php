<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include("PHPMailer/class.phpmailer.php");
    include("admin/dal/data.php");
    ?>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo BASE_URL ?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Simple-Line-Icons-Webfont -->
    <link href="<?php echo BASE_URL ?>/fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" rel="stylesheet">
    <!-- FlexSlider -->
    <link href="<?php echo BASE_URL ?>/scripts/FlexSlider/flexslider.css" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="<?php echo BASE_URL ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/css/owl.theme.default.css" rel="stylesheet">
    <!-- Nivo Lightbox -->
    <link href="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/nivo-lightbox.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/themes/default/default.css" rel="stylesheet">
    <!-- Style.css -->
    <link href="<?php echo BASE_URL ?>/lightbox/lightbox.css" rel="stylesheet">
    <link href="<?php echo BASE_URL ?>/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="icon" href="<?php echo BASE_URL ?>/images/logo.png">

    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4368046.js"></script>
    <!-- End of HubSpot Embed Code -->

    <title>EVENTO KARIKAL</title>
</head>
<body>
<header class="header">
    <style>
        @media screen and (max-width: 900px) {
            .hidden-xs {
                display: none;
            }
        }

        @media screen and (min-width: 900px) {
            .visible-xs {
                display: none;
            }
        }
    </style>
    <div class="navigation">
        <div class="container clearfix">
            <div class="row hidden-xs" style="margin-top: 15px;">
                <div class="col-md-2" style="margin-top: 25px">
                    <div class="logo">
                        <a href="<?php echo BASE_URL ?>/landing.php"><img src="<?php echo BASE_URL ?>/images/logo.png" alt="Architech" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-md-10">
                    <nav class="main-nav" style="float: left;margin-left: 80px">
                        <ul class="list-unstyled">
                            <li style="margin-right: 25px;margin-top:15px">
                                <a style="border: none;cursor: pointer;">
                                    <i class="fa fa-paste" style="width: 45px;height: 45px;border-radius:50%;background-color: black;border:2px solid #ee1d23;font-size: 26px;text-align: center;vertical-align: middle;color:white;padding: 7px"></i>
                                    <strong style="margin-left:5px">DISERTANTES</strong>
                                </a>
                                <ul style="top: 100px;padding-top:5px;padding-bottom: 0px;margin-left:0px;min-width: 0px;    transition: .5s;">
                                    <li>
                                        <a href="<?php echo BASE_URL ?>/landing.php?tab=cv-1">
                                            Arq. Andrea Fuks
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo BASE_URL ?>/landing.php?tab=cv-2">
                                            Ing. Fabián Calcagno
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li style="margin-right: 25px;margin-top:15px">
                                <a style="border: none;cursor: pointer;" href="<?php echo BASE_URL ?>/landing.php?tab=evento">
                                    <i class="fa fa-map-marker-alt" style="width: 45px;height: 45px;border-radius:50%;background-color: black;border:2px solid #ee1d23;font-size: 32px;text-align: center;vertical-align: middle;color:white;padding: 4px"></i>
                                    <strong style="margin-left:5px">Evento</strong>
                                </a>
                            </li>
                            <li style="margin-right: 25px;margin-top:15px">
                                <a style="border: none;cursor: pointer;" href="<?php echo BASE_URL ?>/landing.php?tab=cronograma">
                                    <i class="fa fa-calendar-alt" style="width: 45px;height: 45px;border-radius:50%;background-color: black;border:2px solid #ee1d23;font-size: 27px;text-align: center;vertical-align: middle;color:white;padding: 5px"></i>
                                    <strong style="margin-left:5px">Cronograma</strong>
                                </a>
                            </li>
                            <li style="margin-right: 25px;margin-top:15px">
                                <a style="border: none;cursor: pointer;" href="<?php echo BASE_URL ?>/landing.php">
                                    <i class="fa fa-clipboard-list" style="width: 45px;height: 45px;border-radius:50%;background-color: #ee1d23;border:2px solid black;font-size: 30px;text-align: center;vertical-align: middle;color:white;padding: 5px"></i>
                                    <strong style="margin-left:5px">Inscripciones</strong>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="logo visible-xs">
                <a href="<?php echo BASE_URL ?>/landing.php"><img src="<?php echo BASE_URL ?>/images/logo.png" alt="Architech" class="img-responsive">
                </a>
            </div>
            <a href="<?php echo BASE_URL ?>/" class="responsive-menu-open"><i class="fa fa-bars"></i></a>
        </div>
    </div>
</header>
<div class="responsive-menu">
    <a href="<?php echo BASE_URL ?>/" class="responsive-menu-close"><i class="icon-close"></i></a>
    <nav class="responsive-nav"></nav>
</div>
<div class="page-title hidden-xs" style="background-image: url('<?php echo BASE_URL ?>/images/landing/slide-pc.png');">
    <div class="inner" style="height: 420px">
    </div>
</div>
<div class="visible-xs" style="margin-top: 15px">
    <div id="carouselExampleSlidesOnly" class="carousel slide " data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="col-img"
                     style="height:380px;background:url(<?php echo BASE_URL ?>/images/landing/slide-mobile.png) no-repeat center center/cover;">
                </div>
            </div>
        </div>
    </div>
</div>
<?php

if (isset($_POST["enviar"])) {

    $nombre = antihack(isset($_POST["nombre"]) ? $_POST["nombre"] : '');
    $apellido = antihack(isset($_POST["apellido"]) ? $_POST["apellido"] : '');
    $email = antihack(isset($_POST["email"]) ? $_POST["email"] : '');
    $celular = antihack(isset($_POST["celular"]) ? $_POST["celular"] : '');

    $profesion = antihack(isset($_POST["profesion"]) ? $_POST["profesion"] : '');
    if ($profesion == 'Otro') {
        $otro = antihack(isset($_POST["t-otro"]) ? $_POST["t-otro"] : '');
        $profesion = $profesion . ' - ' . $otro;
    }

    guardarLanding($nombre, $apellido, $email, $celular, $profesion);

    $mensajeUsuario = "Tu solicitud de inscripción al Evento 50 Sombras del BIM, se ha enviado correctamente! Te esperamos el 01 de agosto en el Quorum Córdoba Hotel a las 9:30 h.<br><br>";
    $mensajeAdmin = "Tienes un nuevo inscripto al Evento 50 Sombras del BIM.<br><br>";

    $mensajeFinal = "<b>Nombre</b>: " . $nombre . " <br/>";
    $mensajeFinal .= "<b>Apellido</b>: " . $apellido . " <br/>";
    $mensajeFinal .= "<b>Email</b>: " . $email . "<br/>";
    $mensajeFinal .= "<b>Celular</b>: " . $celular . " <br/>";
    $mensajeFinal .= "<b>Profesión</b>: " . $profesion . " <br/>";

    $asunto = "Evento 50 Sombras del BIM";

    $receptor = 'ldijou@karikal.com.ar';

    //Enviar_User_Admin($asunto, $mensajeAdmin . $mensajeFinal, $receptor);
    //Enviar_User($asunto, $mensajeUsuario . $mensajeFinal, $email);

    $data = array(
        "nombre" => $nombre,
        "apellido" => $apellido,
        "email" => $email,
        "celular" => $celular,
        "profesion" => $profesion,
    );

    $response =curlLanding("", "http://c1521282.ferozo.com/test.php", $data);

    echo '<div class="clearfix"></div><div class="container"><div class="row"><div class="alert alert-success" role="alert">El formulario fue enviado exitosamente.</div></div></div>';
}

if (isset($_GET['tab'])) {
    switch ($_GET['tab']) {
        case "cv-1":
            ?>
            <div class="section white no-padding-bottom">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row" style="    display: flex;    justify-content: center;    align-items: center;">
                                    <div class="col-md-3" style="border-right: 5px solid black">
                                        <img src="<?php echo BASE_URL ?>/images/landing/Andrea.jpeg" width="100%">
                                    </div>
                                    <div class="col-md-9">
                                        <h4>Arq. Andrea Fuks</h4>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:15px">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 0px;color:black">-CEO theBOX.lat Escuela de Negocios de Arquitectura y Construcción.</p>
                                        <p style="margin-bottom: 0px;color:black">-Directora Académica de Miller&Co, ATM (Autorized Training Center) de Autodesk.</p>
                                        <p style="margin-bottom: 0px;color:black">-Responsable de convenios institucionales de Control P Max.</p>
                                        <p style="margin-bottom: 0px;color:black">-Ex Coordinadora Ejecutiva del programa REM - Real Estate Managment - Universidad Torcuato Di Tella.</p>
                                        <p style="margin-bottom: 0px;color:black">-Ex Directora Académica y Titular de NAyC Centro de Estudios de Negocios de Arquitectura y Construcción.</p>
                                        <p style="margin-bottom: 0px;color:black">-Ex Coordinadora del Depto. de Arquitectura "Fundación Hillel Argentina".</p>
                                        <p style="margin-bottom: 0px;color:black">-Expositora, Conferencista y Moderadora en Congresos y Conferencias Internacionales.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        case "cv-2":
            ?>
            <div class="section white no-padding-bottom">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row" style="    display: flex;    justify-content: center;    align-items: center;">
                                    <div class="col-md-3" style="border-right: 5px solid black">
                                        <img src="<?php echo BASE_URL ?>/images/landing/Fabian.jpg" width="100%">
                                    </div>
                                    <div class="col-md-9">
                                        <h4>Ing. Fabián Calcagno</h4>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:15px">
                                    <div class="col-md-12">
                                        <p style="margin-bottom: 0px;color:black">-Especialista en Costos de Obra e Implementación BIM en Latinoamérica y España.</p>
                                        <p style="margin-bottom: 0px;color:black">-Director de MillerCo.</p>
                                        <p style="margin-bottom: 0px;color:black">-Director BIM Forum Argentina.</p>
                                        <p style="margin-bottom: 0px;color:black">-Fundador y Titular de CPW2000: desarrollo del Software para Control y Gestión de Obra: Control P Max.</p>
                                        <p style="margin-bottom: 0px;color:black">-Cocreador del Soft Pplan.</p>
                                        <p style="margin-bottom: 0px;color:black">-Disertante en inumerables congresos y exposiciones en Latinoamerica y España.vvv</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        case "evento":
            ?>
            <div class="section white no-padding-bottom">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1><label>INFO</label><label style="color:red">EVENTO</label></h1>

                                        <label style="color: red;font-size:18px">Fecha </label><label style="margin-left:5px;font-size:18px"> jueves 01 de agosto</label><br>
                                        <label style="color: red;font-size:18px">Hora </label><label style="margin-left:5px;font-size:18px"> de 9.30 a 13.00 h</label><br>
                                        <label style="color: red;font-size:18px"> Lugar </label><label style="margin-left:5px;font-size:18px"> Quorum Córdoba Hotel</label><br>
                                        <hr>
                                        <label style="color:red;margin-top:10px;font-size:19px">PRESENTACIÓN DE KARIKAL</label><br>
                                        <b style="color:black;font-size:18px">Materiales Inteligentes</b><br>
                                        <label style="font-size:18px;color:black;font-weight: 400"> Horario de 10.45 a 11.55 h</label><br><br>

                                        <label style="font-size:18px;color:black;font-weight: 400"> -KOMPAK: placas tecnológicas</label><br>
                                        <label style="font-size:18px;color:black;font-weight: 400"> -I/O LIGHT: zócalos inteligentes</label><br>
                                        <label style="font-size:18px;color:black;font-weight: 400"> -ACUSTIK: diseños Inteligentes</label><br><br>

                                        <label style="color:red;margin-top:10px;font-size:19px">Dos miradas, el mismo concepto BIM: Arq. & Ing.</label><br>
                                        <label style="font-size:18px;color:black;font-weight: 400"> Horario de 12 a 12.45 h</label><br><br>

                                        <label style="font-size:18px;color:black;font-weight: 400"> Arq. Andrea Fuks <a href="<?php echo BASE_URL ?>/landing.php?tab=cv-1">(Ver Curriculum)</a></label><br>
                                        <label style="font-size:18px;color:black;font-weight: 400"> Ing. Fabián Calcagno <a href="<?php echo BASE_URL ?>/landing.php?tab=cv-2">(Ver Curriculum)</a></label><br>
                                        <small>TheBOX.lat y Millerco. Presentación de<br>
                                            ejemplos BIM en Latinoamérica.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        case "cronograma":
            ?>
            <div class="section white no-padding-bottom">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1><label>CRONOGRAMA</label><label style="color:red">EVENTO</label></h1>
                                        <strong style="font-size: 15px;color: black">9.30 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Acreditaciones</label> <br>
                                        <strong style="font-size: 15px;color: black">10 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Breakfast</label> <br>
                                        <strong style="font-size: 15px;color: black">10.30 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Presentación de la empresa</label> <br>
                                        <strong style="font-size: 15px;color: black">10.45 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Comienzo de disertación de productos</label> <br>
                                        <strong style="font-size: 15px;color: black">10.45 a 11.05 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Inteligencia en fachadas: Kompak: placas tecnológicas (fachadas, boxes, mesadas y demás aplicaciones)</label> <br>
                                        <strong style="font-size: 15px;color: black">11.10 a 11.30 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">I/O LEDs: El siguiente nivel de la construcción</label> <br>
                                        <strong style="font-size: 15px;color: black">11.35 a 11.55 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">¿Cómo bajar la acústica sin perder diseño? Karikal Acustik: Diseños Inteligentes</label> <br>
                                        <strong style="font-size: 15px;color: black">12.00 a 12.45 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Ing. Fabián Calcagno y Arq. Andrea Fuks: BIM + Charla Inspiradora</label> <br>
                                        <strong style="font-size: 15px;color: black">13.00 h</strong><br>
                                        <label style="margin-left: 20px;color: grey">Cierre</label> <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        default:
            ?>
            <div class="section white no-padding-bottom">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1>INSCRIBÍTE AQUÍ:</h1>
                                <form method="post" id="demo-form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">Nombre*:
                                                <input type="text" name="nombre" class="form-control" required placeholder="Nombre">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">Apellido*:
                                                <input type="text" name="apellido" class="form-control" required placeholder="Apellido">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">Correo electrónico*:
                                                <input type="email" name="email" class="form-control" required placeholder="Correo electrónico">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">Celular*:
                                                <input type="text" name="celular" class="form-control" required placeholder="Celular">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="pro" class="form-group">Profesión*:<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Arquitecto" required> Arquitecto<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Ingeniero"> Ingeniero<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Técnico"> Técnico<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Constructor"> Constructor<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Urbanista"> Urbanista<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Maestro mayor de obra"> Maestro mayor de obra<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Estudiante"> Estudiante<br>
                                                <input style=" margin-top: 15px" type="radio" name="profesion" value="Otro"> Otro
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br/>
                                        <div class="col-md-12">
                                            <input class="btn btn-warning btn-send" name="enviar" type="submit" value="Enviar">
                                        </div>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
    }
} else {
    ?>
    <div class="section white no-padding-bottom">
        <div class="inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>INSCRIBÍTE AQUÍ:</h1>
                        <form method="post" id="demo-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">Nombre*:
                                        <input type="text" name="nombre" class="form-control" required placeholder="Nombre">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">Apellido*:
                                        <input type="text" name="apellido" class="form-control" required placeholder="Apellido">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">Correo electrónico*:
                                        <input type="email" name="email" class="form-control" required placeholder="Correo electrónico">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">Celular*:
                                        <input type="text" name="celular" class="form-control" required placeholder="Celular">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="pro" class="form-group">Profesión*:<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Arquitecto" required> Arquitecto<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Ingeniero"> Ingeniero<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Técnico"> Técnico<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Constructor"> Constructor<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Urbanista"> Urbanista<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Maestro mayor de obra"> Maestro mayor de obra<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Estudiante"> Estudiante<br>
                                        <input style=" margin-top: 15px" type="radio" name="profesion" value="Otro"> Otro
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <br/>
                                <div class="col-md-12">
                                    <input class="btn btn-warning btn-send" name="enviar" type="submit" value="Enviar">
                                </div>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-12">
            <img width="100%" src="<?php echo BASE_URL ?>/images/landing/pie.png">
        </div>
    </div>
</div>
<br/><br/>
<footer class="footer">
    <div class="bottom clearfix">
        <div class="container">
            <div class="copyright">Copyright &copy; 2017. Estudio Rocha & Asociados</div>
            <div class="social-icons">
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
</footer>


<script src="<?php echo BASE_URL ?>/js/jquery-1.11.2.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/bootstrap.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/lightbox.js"></script>

<script src="<?php echo BASE_URL ?>/js/jquery.inview.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script src="<?php echo BASE_URL ?>/scripts/FlexSlider/jquery.flexslider-min.js"></script>

<script src="<?php echo BASE_URL ?>/js/owl.carousel.min.js"></script>

<script src="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/nivo-lightbox.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/isotope.pkgd.min.js"></script>
<script src="<?php echo BASE_URL ?>/js/imagesloaded.pkgd.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/jquery.waypoints.min.js"></script>
<script src="<?php echo BASE_URL ?>/js/jquery.counterup.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/scripts.js"></script>

<script>
    $("input[name='profesion']").click(function () {
        if ($(this).val() == "Otro") {
            if ($("#texto").length) {

            } else {
                $("#pro").append("<input class='form-control' style='margin-left:20px;width: 50%;display: inline-block' id='texto' type='text' name='t-otro' placeholder='Profesión?' required>");
            }
        } else {
            $("#texto").remove();
        }
    })
</script>
</body>
</html>