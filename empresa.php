<!DOCTYPE html>
<html lang="en">
<head>
    <?php //include("PHPMailer/class.phpmailer.php"); ?>
    <?php include("inc/header.inc.php"); ?>
    <?php
    $title = "Empresa | Top Floor";
    $keywords = "pisos flotantes en san francisco, pisos flotantes";
    $description = "Siguiendo una política de fuertes inversiones en infraestructura, tecnología y capacitación de recursos humanos se instalaron maquinarias de origen italiano con tecnología de avanzada.";
    $canonical = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $autor = "Top Floor";
    $made = "info@topfloor.com.ar";
    $pais = "Argentina";
    $place = "San Francisco";
    $position = "-31.4419372,-62.1279763";
    $copy = "2017";
    ?>
    <title><?php echo $title; ?></title>
    <meta http-equiv="title" content="<?php echo $title; ?>" />
    <meta name="description" lang="es" content="<?php echo $description; ?>">
    <meta name="keywords" lang="es" content="<?php echo $keywords; ?>">
    <meta name="author" lang="es" content="<?php echo $autor; ?>">
    <link href="<?php echo BASE_URL ?>/img/logo.png" rel="Shortcut Icon">
    <link rel="author" href="<?php echo $made; ?>" rel="nofollow"> 
    <meta name="copyright" content="<?php echo $copy; ?>">
    <link rel="canonical" href="<?php echo $canonical; ?>"> 
    <meta name="revisit-after" content="15 days"> 
    <meta name="distribution" content="global"> 
    <meta name="robots" content="all"> 
    <meta name="rating" content="general"> 
    <meta name="content-language" content="es-ar"> 

    <meta name="DC.identifier" content="<?php echo $canonical; ?>">
    <meta name="DC.format" content="text/html">
    <meta name="DC.coverage" content="<?php echo $pais; ?>"> 
    <meta name="DC.title" content="<?php echo $title; ?>" />
    <meta name="DC.subject" content="<?php echo $description; ?>">
    <meta name="DC.description" content="<?php echo $description; ?>">
    <meta name="DC.language" content="es-ar">
    <meta http-equiv="window-target" content="_top">   
    <meta name="robots" content="all" /> 
    <meta http-equiv="content-language" content="es-ES" />
    <meta name="google" content="notranslate" />
    <meta name="geo.region" content="AR-X" />
    <meta name="geo.placename" content="<?php echo $place; ?>" />
    <meta name="geo.position" content="<?php echo $position; ?>" />
    <meta name="ICBM" content="<?php echo $position; ?>" /> 
    <meta content="public" name="Pragma">
    <meta http-equiv="pragma" content="public">
    <meta http-equiv="cache-control" content="public">
</head>  
<body>

	<?php include("inc/nav.inc.php") ?>
	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background01.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Sobre</div>
				<div class="title">Nuestra empresa</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Nuestra empresa</li>
				</ol>
			</div> 
		</div> 
	</div> 

	<div class="section white no-padding-bottom">
		<div class="inner">
			<div class="container">
				<?php Traer_Contenidos("empresa") ?>
			</div> 
		</div> 
	</div> 

	<div class="section white no-padding-bottom">
		<div class="inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="timeline">
							<div class="item">
								<div class="year">1953</div>
								<div class="content">
									<h5>Fundación de Plástica Magnano como Fábrica de Sillas</h5>
								</div> 
							</div> 
							<div class="item">
								<div class="year">1961</div>
								<div class="content">
									<h5>Importación de la primera línea de fabricación de laminados plásticos de alta presión del pais.</h5>
								</div> 
							</div> 
							<div class="item">
								<div class="year">1979</div>
								<div class="content">
									<h5>Quiebra Plástica Magnano</h5>
 								</div> 
							</div> 
							<div class="item">
								<div class="year">1982</div>
								<div class="content">
									<h5>Centro S.A: Compra la empresa y reanuda las actividades reincorporando todo el personal</h5>
 								</div> 
							</div> 
							<div class="item">
								<div class="year">1991</div>
								<div class="content">
									<h5>Se fijan nuevas estrategias de inversiones apostando en 3 pilares fundamentales: Tecnología, Infraestructura y RRHH</h5>									
								</div> 
							</div> 
							<div class="item">
								<div class="year">1993</div>
								<div class="content">
									<h5>Se compran líneas de baja presión (tableros melaminicos), postformado, escuadrado, mecanizado y canteado de mueble apartes y semi-elaborados</h5>									
								</div> 
							</div> 
							<div class="item">
								<div class="year">1997</div>
								<div class="content">
									<h5>Se compra la prensa Pagnoni, valuada en 4 millones de dolares y sexta en su tipo en el mundo.</h5>									
								</div> 
							</div> 
							<div class="item">
								<div class="year">1998</div>
								<div class="content">
									<h5>Se certifican las normas de calidad ISO 9000</h5>									
								</div> 
							</div> 
							<div class="item">
								<div class="year">2006</div>
								<div class="content">
									<h5>Se inaugura la planta de pisos flotantes en el Parque Industrial San Francisco</h5>									
								</div> 
							</div> 
							<div class="item">
								<div class="year">2016</div>
								<div class="content">
									<h5>sSe instala en la planta de top floor, una nueva linea con tecnologia de punta para la fabricación de pisos flotantes</h5>									
								</div> 
							</div> 
						</div> 
					</div> 
					<div class="col-sm-3">
						<div class="panel yellow">
							<div class="panel-body blancoFont">
								<h2><span>Nuestra</span>Visión</h2>
								<p>Conservar el liderazgo nacional de diseño, innovación y calidad en la provisión de revestimientos para los usuarios finales y para otras industrias. Mantener la participación y el reconocimiento de nuestros productos a nivel mundial.</p>
							</div> 
						</div> 
						<div class="panel">
							<div class="panel-body">
								<h2><span>Nuestra</span>Misión</h2>
								<p>Nuestro objetivo es brindar al mercado productos que provean soluciones en superficie, que cumplan con los requerimientos de nuestros clientes y mediante una rentabilidad sustentable tener un crecimiento sostenible.</p>
							</div> 
						</div> 
					</div> 
				</div> 

			</div> 
		</div> 
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>