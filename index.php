<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<title>Materiales para Construcción :: Karikal</title>
</head>
<body>

	<?php include("inc/nav.inc.php") ?>

	<div id="welcome-slider" class="welcome-slider flex-slider">
		<div class="slides clearfix">
			<!--<div class="slide" style="background-image: url('<?php echo BASE_URL ?>/images/slide.jpg');"></div>-->
			<?php Slider_Read("inicio"); ?> 			 
		</div> 
	</div> 

	<div class="section light  no-padding-bottom">
		<div class="inner">
			<div class="container">
				<div class="row">
					<div class="column-spacer"></div>
					<div class="col-sm-12">
						<?php Traer_Contenidos("Inicio Box") ?>						
					</div> 
				</div> 
			</div> 
		</div> 
	</div> 


	<div class="section light no-padding-bottom">
		<div class="inner">
			<div class="container">
				<h3 class="pull-left"><span>Últimas</span>Novedades</h3>
				<div id="portfolio-filters" class="portfolio-filters">
					<button class="active" data-filter="*">Todos</button>
					<?php Categoria_Read_Notas() ?>
				</div>
			</div> 
			<div id="portfolio" class="portfolio">
				<div class="portfolio-sizer"></div>
				<?php Notas_Read_Front_Index() ?> 
			</div> 
		</div> 
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>