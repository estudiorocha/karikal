<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php");
	$id = isset($_GET["id"]) ? $_GET["id"] : '';
	$nota = Nota_TraerPorId($id);
	$fecha = explode("-",$nota["FechaNotas"]);
	$cod = $nota["CodNotas"]; 
	$url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$imagen = Notas_Slide_Read_Principal($cod);
	$imagen = $imagen[0];
	?>

	<title><?php echo $nota["TituloNotas"] ?> :: Karikal</title>
	<meta name="description" content="<?php echo strip_tags(substr($nota["DesarrolloNotas"],0,100)); ?>" />
 	<meta name="author" content="Estudio Rocha & Asociados" />
	
	<meta itemprop="name" content="<?php echo $nota["TituloNotas"] ?>">
	<meta itemprop="description" content="<?php echo substr($nota["DesarrolloNotas"],0,200); ?>">
	<meta itemprop="image" content="<?php echo BASE_URL."/".$imagen ?>">
	
	<meta name="twitter:card" content="<?php echo BASE_URL."/".$imagen ?>">
	<meta name="twitter:site" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
	<meta name="twitter:title" content="<?php echo $nota["TituloNotas"] ?>">
	<meta name="twitter:description" content="<?php echo strip_tags(substr($nota["DesarrolloNotas"],0,200)); ?>">
	<meta name="twitter:image:src" content="<?php echo BASE_URL."/".$imagen ?>">
	
	<meta property="og:title" content="<?php echo $nota["TituloNotas"] ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:image" content="<?php echo BASE_URL."/".$imagen ?>" />
	<meta property="og:description" content="<?php echo strip_tags(substr($nota["DesarrolloNotas"],0,200)); ?>" />
	<meta property="og:site_name" content="<?php echo $nota["TituloNotas"] ?>" /> 
	<meta property="article:section" content="article" />
</head>
<body>

	<?php include("inc/nav.inc.php") ?>

	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background10.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Nuestras</div>
				<div class="title">Novedades</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Novedades</li>
				</ol>
			</div> 
		</div> 
	</div> 

	<div class="section white">
		<div class="inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<div class="blog">
							<div class="blog blog-detail">
								<div class="blog-post">
									<div class="blog-post-media">
										<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
											<div class="carousel-inner" role="listbox">
												<?php Notas_Slide_Read($cod) ?>
											</div>
											<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
												<span class='glyphicon glyphicon-menu-left icon-prev'></span>
												<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
												<span class='glyphicon glyphicon-menu-right icon-next'></span>
												<span class="sr-only">Next</span>
											</a>
										</div>
										<div class="clearfix"></div><br/>
									</div> 
									<div class="blog-post-content">
										<a href=""><h4 class="blog-post-title"><span>8 April</span><?php echo $nota["TituloNotas"]; ?></h4></a>
										<p><?php echo $nota["DesarrolloNotas"]; ?></p>										
									</div> 
								</div> 
								<div class="share-post">
									<span>Compartir en:</span>
									<div class="social-icons">
										<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>"><i class="fa fa-facebook"></i></a>
										<a target="_blank" href="https://twitter.com/home?status=<?php echo $url; ?>"><i class="fa fa-twitter"></i></a>
										<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>"><i class="fa fa-linkedin"></i></a>
										<a target="_blank" href="https://plus.google.com/share?url=<?php echo $url; ?>"><i class="fa fa-google-plus"></i></a>
									</div> 
								</div>     
							</div>
						</div> 
					</div>
					<div class="col-sm-4"> 
						<div class="widget">
							<h5 class="widget-title"><span>Últimas</span>Novedades</h5>
							<?php Notas_Read_Front_Side() ?>
						</div> 
						<div class="widget">
							<h5 class="widget-title"><span>ver</span>categorías</h5>
							<div class="inner">
								<ul class="fa-ul">
									<?php Categoria_Read_Agregar() ?>
								</ul>
							</div> 
						</div> 

					</div> 
				</div> 
			</div> 
		</div> 
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>