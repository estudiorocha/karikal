<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<?php $tipo = isset($_GET["tipo"]) ? $_GET["tipo"] : ''; ?>
	<title>Novedades :: Karikal</title>
</head>
<body>

	<?php include("inc/nav.inc.php") ?>

	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background10.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Nuestras</div>
				<div class="title">Novedades</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Novedades</li>
				</ol>
			</div> 
		</div> 
	</div> 

	<div class="section white">
		<div class="inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<div class="blog">
							<?php Notas_Read_Front($tipo) ?>
						</div> 
					</div>
					<div class="col-sm-4"> 
						<div class="widget">
							<h5 class="widget-title"><span>Últimas</span>Novedades</h5>
							<?php Notas_Read_Front_Side() ?>
						</div> 
						<div class="widget">
							<h5 class="widget-title"><span>ver</span>categorías</h5>
							<div class="inner">
								<ul class="fa-ul">
									<?php Categoria_Read_Agregar() ?>
								</ul>
							</div> 
						</div> 

					</div> 
				</div> 
			</div> 
		</div> 
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>