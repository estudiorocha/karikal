<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$contenido = new Clases\Contenidos();
$config = new Clases\Config();
$usuario = new Clases\Usuarios();
$enviar = new Clases\Email();
$hub = new Clases\Hubspot();
///Datos
$emailData = $config->viewEmail();
$userData = $usuario->viewSession();
$captchaData = $config->viewCaptcha();
if (!empty($userData)) {
    $funciones->headerMove(URL . '/sesion');
}
$link = $funciones->antihack_mysqli(isset($_GET["link"]) ? $_GET["link"] : '');
//
$template->set("title", "Usuarios | " . TITULO);
$template->set("description", "Panel de para usuarios de " . TITULO);
$template->set("keywords", "");
$template->set("body", "contacts");
$template->themeInit();
?>
    <!-- Page Title #4
           ============================================= -->
    <section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
        <div class="bg-section">
            <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="page-title title-4 pt-90">
                        <div class="pull-left">
                            <h2>INGRESO / REGISTRO DE USUARIOS</h2>
                        </div>
                        <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                            <li>
                                <a href="<?= URL ?>">INICIO</a>
                            </li>
                            <li class="active">INGRESO / REGISTRO</li>
                        </ol>
                    </div>
                    <!-- .page-title end -->
                </div>
                <!-- .col-md-12 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <div class="section-empty">
        <div class="container content pt-15 pb-20">
            <div class="row">
                <div class="col-md-4">
                    <h3>INGRESO</h3>
                    <?php

                    if (isset($_POST["ingresar"])) {
                        $email = $funciones->antihack_mysqli(isset($_POST["l-email"]) ? $_POST["l-email"] : '');
                        $password = $funciones->antihack_mysqli(isset($_POST["l-password"]) ? $_POST["l-password"] : '');

                        $usuario->set("email", $email);
                        $usuario->set("password", $password);
                        $result = $usuario->login();
                        if (isset($result['error'])) {
                            switch ($result["error"]) {
                                case 1:
                                    echo "<div class='alert alert-warning alerta'>Su cuenta aún no está activada.</div><div class='clearfix'></div>";
                                    break;
                                case 2:
                                    echo "<div class='alert alert-warning alerta'>E-mail o contraseña incorrectos.</div><div class='clearfix'></div>";
                                    break;
                            }
                        } else {
                            if (!empty($link)) {
                                $funciones->headerMove(URL . '/' . $link);
                            } else {
                                $funciones->headerMove(URL . '/sesion/pedidos');
                            }
                        }
                    }
                    ?>
                    <form class="form-box formulario-usuarios" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Email</p>
                                <input name="l-email" placeholder="" type="email" class="form-control form-value"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Contraseña</p>
                                <input name="l-password" placeholder="" type="password" class="form-control form-value"
                                       required>
                            </div>
                        </div>
                        <a href="<?= URL ?>/recuperar">
                            Olvidaste tu contraseña? Haz click aquí.
                        </a>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn-md btn" name="ingresar" type="submit">
                                    INGRESAR
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-8">
                    <h3>REGISTRO</h3>
                    <?php
                    if (isset($_POST['registrar'])) {
                        //Google reCAPTCHA API secret key
                        $secretKey = 'Your_reCAPTCHA_Secret_Key';

                        // Verify the reCAPTCHA response
                        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $captchaData['data']['captcha_secret'] . '&response=' . $_POST['g-recaptcha-response']);

                        // Decode json data
                        $responseData = json_decode($verifyResponse);
                        if ($responseData->success) {
                            $pass1_ = $funciones->antihack_mysqli($_POST["r-password1"]);
                            $pass2_ = $funciones->antihack_mysqli($_POST["r-password2"]);
                            if ($pass1_ == $pass2_) {
                                $nombre = $funciones->antihack_mysqli(isset($_POST["r-nombre"]) ? $_POST["r-nombre"] : '');
                                $apellido = $funciones->antihack_mysqli(isset($_POST["r-apellido"]) ? $_POST["r-apellido"] : '');
                                $email = $funciones->antihack_mysqli(isset($_POST["r-email"]) ? $_POST["r-email"] : '');
                                $password = $funciones->antihack_mysqli(isset($_POST["r-password1"]) ? $_POST["r-password1"] : '');
                                $direccion = $funciones->antihack_mysqli(isset($_POST["r-direccion"]) ? $_POST["r-direccion"] : '');
                                $localidad = $funciones->antihack_mysqli(isset($_POST["r-localidad"]) ? $_POST["r-localidad"] : '');
                                $provincia = $funciones->antihack_mysqli(isset($_POST["r-provincia"]) ? $_POST["r-provincia"] : '');
                                $postal = $funciones->antihack_mysqli(isset($_POST["r-postal"]) ? $_POST["r-postal"] : '');
                                $telefono = $funciones->antihack_mysqli(isset($_POST["r-telefono"]) ? $_POST["r-telefono"] : '');
                                $cod = substr(md5(uniqid(rand())), 0, 10);
                                $fecha = getdate();
                                $fecha = $fecha['year'] . '-' . $fecha['mon'] . '-' . $fecha['mday'];

                                $usuario->set("cod", $cod);
                                $usuario->set("nombre", $nombre);
                                $usuario->set("apellido", $apellido);
                                $usuario->set("email", $email);
                                $usuario->set("password", $password);
                                $usuario->set("direccion", $direccion);
                                $usuario->set("postal", $postal);
                                $usuario->set("localidad", $localidad);
                                $usuario->set("provincia", $provincia);
                                $usuario->set("telefono", $telefono);
                                $usuario->set("minorista", 1);
                                $usuario->set("invitado", "0");
                                $usuario->set("descuento", 0);
                                $usuario->set("fecha", $fecha);
                                $usuario->set("estado", 1);

                                $hub->set("nombre", $nombre);
                                $hub->set("apellido", $apellido);
                                $hub->set("email", $email);
                                $hub->set("direccion", $direccion);
                                $hub->set("telefono", $telefono);
                                $hub->set("celular", "");
                                $hub->set("localidad", $localidad);
                                $hub->set("provincia", $provincia);
                                $hub->set("postal", $postal);
                                $response = $usuario->validateV2();

                                if ($response["status"] && $response["data"]["invitado"] == 0)
                                {
                                    ?>
                                    <br/>
                                    <div class="alert alert-danger alerta" role="alert">El email ya está registrado.
                                    </div>
                                    <?php
                                } else {

                                    $hub->addContact();
                                    if (!empty($response['data']['invitado'])) {
                                        $usuario->set("cod",$response['data']['cod']);
                                        $usuario->edit();
                                    } else {
                                        $usuario->add();
                                    }
                                    $usuario->set("password", $password);
                                    $usuario->set("email", $email);

                                    $usuario->login();

                                    //Envio de mail al usuario
                                    $mensaje = 'Gracias por registrarse ' . ucfirst($nombre) . '<br/>';
                                    $asunto = TITULO . ' - Registro';
                                    $receptor = $email;
                                    $emisor = $emailData['data']['remitente'];
                                    $enviar->set("asunto", $asunto);
                                    $enviar->set("receptor", $receptor);
                                    $enviar->set("emisor", $emisor);
                                    $enviar->set("mensaje", $mensaje);
                                    $enviar->emailEnviar();
                                    //Envio de mail a la empresa
                                    $mensaje2 = 'El usuario ' . ucfirst($nombre) . ' ' . ucfirst($apellido) . ' acaba de registrarse en nuestra plataforma' . '<br/>';
                                    $asunto2 = TITULO . ' - Registro';
                                    $receptor2 = $emailData['data']['remitente'];
                                    $emisor2 = $emailData['data']['remitente'];
                                    $enviar->set("asunto", $asunto2);
                                    $enviar->set("receptor", $receptor2);
                                    $enviar->set("emisor", $emisor2);
                                    $enviar->set("mensaje", $mensaje2);
                                    $enviar->emailEnviar();

                                   $funciones->headerMove(URL . '/sesion');
                                }
                            } else {
                                ?>
                                <br/>
                                <div class="alert alert-danger alerta" role="alert">Las contraseñas no coinciden.</div>
                                <?php
                            }
                        } else {
                            echo '<div class="alert alert-danger alerta" role="alert">¡Completar el CAPTCHA correctamente!</div>';
                        }
                    }
                    ?>
                    <form class="form-box formulario-usuarios" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Nombre</p>
                                <input name="r-nombre" placeholder="" type="text" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-nombre"]) ? $_POST["r-nombre"] : '')); ?>"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <p>Apellido</p>
                                <input name="r-apellido" placeholder="" type="text" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-apellido"]) ? $_POST["r-apellido"] : '')); ?>"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Email</p>
                                <input name="r-email" placeholder="" type="email" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-email"]) ? $_POST["r-email"] : '')); ?>"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Password</p>
                                <input name="r-password1" placeholder="" type="password" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-password1"]) ? $_POST["r-password1"] : '')); ?>"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <p>Repetir password</p>
                                <input name="r-password2" placeholder="" type="password" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-password2"]) ? $_POST["r-password2"] : '')); ?>"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Dirección</p>
                                <input name="r-direccion" placeholder="" type="text" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-direccion"]) ? $_POST["r-direccion"] : '')); ?>"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Provincia</p>
                                <div class="input-group w-100">
                                    <select class="pull-right form-control h40" name="r-provincia" id="provincia"
                                            required>
                                        <option value="" selected disabled>Provincia</option>
                                        <?php $funciones->provincias() ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p>Localidad</p>
                                <div class="input-group w-100">
                                    <select class="form-control h40" name="r-localidad" id="localidad" required>
                                        <option value="" selected disabled>Localidad</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-6">
                                <p>Código postal</p>
                                <input name="r-postal" placeholder="" type="text" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-postal"]) ? $_POST["r-postal"] : '')); ?>"
                                       required>
                            </div>
                            <div class="col-md-6">
                                <p>Teléfono</p>
                                <input name="r-telefono" placeholder="" type="text" class="form-control form-value"
                                       value="<?= $funciones->antihack_mysqli((isset($_POST["r-telefono"]) ? $_POST["r-telefono"] : '')); ?>"
                                       required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="g-recaptcha"
                                     data-sitekey="<?= $captchaData['data']['captcha_key'] ?>"></div>
                            </div>
                            <div class="col-md-12 mt-10">
                                <button class="btn-md btn" name="registrar" type="submit">
                                    REGISTRARSE
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
$template->themeEnd();
?>