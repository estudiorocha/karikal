<?php
$config = new Clases\Config();
$funcion = new Clases\PublicFunction();
$tab = $funcion->antihack_mysqli(isset($_GET["tab"]) ? $_GET["tab"] : '');
$emailData = $config->viewEmail();
$marketingData = $config->viewMarketing();
$contactoData = $config->viewContact();
$socialData = $config->viewSocial();
$mercadoLibreData = $config->viewMercadoLibre();
$captchaData = $config->viewCaptcha();
//Metodos de pagos
$config->set("id", 1);
$pagosData1 = $config->viewPayment();
$config->set("id", 2);
$pagosData2 = $config->viewPayment();
$config->set("id", 3);
$pagosData3 = $config->viewPayment();
$config->set("id", 4);
$pagosData4 = $config->viewPayment();


?>
<section id="tabs" class="project-tab">
    <div class="row">
        <div class="col-md-12">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="email-tab" data-toggle="tab" href="#email-home" role="tab" aria-controls="nav-home" aria-selected="true">Configuración Email</a>
                    <a class="nav-item nav-link" id="marketing-tab" data-toggle="tab" href="#marketing-home" role="tab" aria-controls="nav-profile" aria-selected="false">Marketing</a>
                    <a class="nav-item nav-link" id="contact-tab" data-toggle="tab" href="#contact-home" role="tab" aria-controls="nav-contact" aria-selected="false">Datos de contacto</a>
                    <a class="nav-item nav-link" id="social-tab" data-toggle="tab" href="#social-home" role="tab" aria-controls="nav-home" aria-selected="true">Redes sociales</a>
                    <a class="nav-item nav-link" id="mercadolibre-tab" data-toggle="tab" href="#ml-home" role="tab" aria-controls="nav-profile" aria-selected="false">MercadoLibre</a>
                    <a class="nav-item nav-link" id="pagos-tab" data-toggle="tab" href="#pagos-home" role="tab" aria-controls="nav-contact" aria-selected="false">Pagos</a>
                    <a class="nav-item nav-link" id="captcha-tab" data-toggle="tab" href="#captcha-home" role="tab" aria-controls="nav-contact" aria-selected="false">Captcha</a>
                </div>
            </nav>
            <div class="tab-content mt-15" id="nav-tabContent">
                <?php
                if (isset($_POST["agregar-email"])) {
                    $config->set("remitente", $funcion->antihack_mysqli(isset($_POST["e-remitente"]) ? $_POST["e-remitente"] : ''));
                    $config->set("smtp", $funcion->antihack_mysqli(isset($_POST["e-smtp"]) ? $_POST["e-smtp"] : ''));
                    $config->set("smtp_secure", $funcion->antihack_mysqli(isset($_POST["e-smtp-secure"]) ? $_POST["e-smtp-secure"] : ''));
                    $config->set("puerto", $funcion->antihack_mysqli(isset($_POST["e-puerto"]) ? $_POST["e-puerto"] : ''));
                    $config->set("email_", $funcion->antihack_mysqli(isset($_POST["e-email"]) ? $_POST["e-email"] : ''));
                    $config->set("password", $funcion->antihack_mysqli(isset($_POST["e-password"]) ? $_POST["e-password"] : ''));
                    $error = $config->addEmail();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=email-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade show active" id="email-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=email-tab">
                        <div class="row mt-5">
                            <label class="col-md-6">
                                Remitente:<br/>
                                <input type="email" class="form-control" name="e-remitente"
                                       value="<?= $emailData['data']["remitente"] ? $emailData['data']["remitente"] : '' ?>" required/>
                            </label>
                            <label class="col-md-6">
                                Email:<br/>
                                <input type="email" class="form-control" name="e-email"
                                       value="<?= $emailData['data']["email"] ? $emailData['data']["email"] : '' ?>" required/>
                            </label>
                            <label class="col-md-4">
                                SMTP Server:<br/>
                                <input type="text" class="form-control" name="e-smtp"
                                       value="<?= $emailData['data']["smtp"] ? $emailData['data']["smtp"] : '' ?>" required/>
                            </label>
                            <label class="col-md-2">
                                SMTP Secure:<br/>
                                <select name="e-smtp-secure" required>
                                    <?php
                                    if (!empty($emailData['data']['smtp_secure'])) {
                                        $secure = $emailData['data']['smtp_secure'];
                                        ?>
                                        <option value="tls" <?php if ($secure == "tls") {
                                            echo "selected";
                                        } ?>>
                                            TLS
                                        </option>
                                        <option value="ssl" <?php if ($secure == "ssl") {
                                            echo "selected";
                                        } ?>>
                                            SSL
                                        </option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="tls">TLS</option>
                                        <option value="ssl">SSL</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </label>
                            <label class="col-md-2">
                                Puerto:<br/>
                                <input type="number" class="form-control" name="e-puerto"
                                       value="<?= $emailData['data']["puerto"] ? $emailData['data']["puerto"] : '' ?>" required/>
                            </label>
                            <label class="col-md-4">
                                Password:<br/>
                                <input type="password" class="form-control" name="e-password"
                                       value="<?= $emailData['data']["password"] ? $emailData['data']["password"] : '' ?>" required/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-email">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["agregar-marketing"])) {
                    $config->set("googleDataStudioId", $funcion->antihack_mysqli(isset($_POST["m-google-id"]) ? $_POST["m-google-id"] : ''));
                    $config->set("googleAnalytics", $funcion->antihack_mysqli(isset($_POST["m-google-analytics"]) ? $_POST["m-google-analytics"] : ''));
                    $config->set("hubspot", $funcion->antihack_mysqli(isset($_POST["m-hubspot"]) ? $_POST["m-hubspot"] : ''));
                    $config->set("mailrelay", $funcion->antihack_mysqli(isset($_POST["m-mailrelay"]) ? $_POST["m-mailrelay"] : ''));
                    $config->set("onesignal", $funcion->antihack_mysqli(isset($_POST["m-onesignal"]) ? $_POST["m-onesignal"] : ''));
                    $config->set("facebookPixel", $funcion->antihack_mysqli(isset($_POST["m-facebook-pixel"]) ? $_POST["m-facebook-pixel"] : ''));
                    $error = $config->addMarketing();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=marketing-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade" id="marketing-home" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=marketing-tab">
                        <div class="row mt-5">
                            <label class="col-md-12">
                                Google Data Studio ID:<br/>
                                <input type="text" class="form-control" name="m-google-id"
                                       value="<?= $marketingData['data']["google_data_studio_id"] ? $marketingData['data']["google_data_studio_id"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Google Analytics:<br/>
                                <input type="text" class="form-control" name="m-google-analytics"
                                       value="<?= $marketingData['data']["google_analytics"] ? $marketingData['data']["google_analytics"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Hubspot:<br/>
                                <input type="text" class="form-control" name="m-hubspot"
                                       value="<?= $marketingData['data']["hubspot"] ? $marketingData['data']["hubspot"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Mailrelay:<br/>
                                <input type="text" class="form-control" name="m-mailrelay"
                                       value="<?= $marketingData['data']["mailrelay"] ? $marketingData['data']["mailrelay"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                OneSignal:<br/>
                                <input type="text" class="form-control" name="m-onesignal"
                                       value="<?= $marketingData['data']["onesignal"] ? $marketingData['data']["onesignal"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Facebook Pixel:<br/>
                                <input type="text" class="form-control" name="m-facebook-pixel"
                                       value="<?= $marketingData['data']["facebook_pixel"] ? $marketingData['data']["facebook_pixel"] : '' ?>"/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-marketing">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["agregar-contacto"])) {
                    $config->set("email", $funcion->antihack_mysqli(isset($_POST["email"]) ? $_POST["email"] : ''));
                    $config->set("telefono", $funcion->antihack_mysqli(isset($_POST["telefono"]) ? $_POST["telefono"] : ''));
                    $config->set("whatsapp", $funcion->antihack_mysqli(isset($_POST["whatsapp"]) ? $_POST["whatsapp"] : ''));
                    $config->set("domicilio", $funcion->antihack_mysqli(isset($_POST["domicilio"]) ? $_POST["domicilio"] : ''));
                    $config->set("localidad", $funcion->antihack_mysqli(isset($_POST["localidad"]) ? $_POST["localidad"] : ''));
                    $config->set("provincia", $funcion->antihack_mysqli(isset($_POST["provincia"]) ? $_POST["provincia"] : ''));
                    $config->set("pais", $funcion->antihack_mysqli(isset($_POST["pais"]) ? $_POST["pais"] : ''));
                    $error = $config->addContact();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=contact-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade" id="contact-home" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=contact-tab">
                        <div class="row mt-5">
                            <label class="col-md-12">
                                Email:<br/>
                                <input type="email" class="form-control" name="email"
                                       value="<?= $contactoData['data']["email"] ? $contactoData['data']["email"] : '' ?>" required/>
                            </label>
                            <label class="col-md-6">
                                Teléfono:<br/>
                                <input type="text" class="form-control" name="telefono"
                                       value="<?= $contactoData['data']["telefono"] ? $contactoData['data']["telefono"] : '' ?>" required/>
                            </label>
                            <label class="col-md-6">
                                Whatsapp:<br/>
                                <input type="text" class="form-control" name="whatsapp"
                                       value="<?= $contactoData['data']["whatsapp"] ? $contactoData['data']["whatsapp"] : '' ?>" />
                            </label>
                            <label class="col-md-3">
                                Domicilio:<br/>
                                <input type="text" class="form-control" name="domicilio"
                                       value="<?= $contactoData['data']["domicilio"] ? $contactoData['data']["domicilio"] : '' ?>" required/>
                            </label>
                            <label class="col-md-3">
                                Localidad:<br/>
                                <input type="text" class="form-control" name="localidad"
                                       value="<?= $contactoData['data']["localidad"] ? $contactoData['data']["localidad"] : '' ?>" required/>
                            </label>
                            <label class="col-md-3">
                                Provincia:<br/>
                                <input type="text" class="form-control" name="provincia"
                                       value="<?= $contactoData['data']["provincia"] ? $contactoData['data']["provincia"] : '' ?>" required/>
                            </label>
                            <label class="col-md-3">
                                País:<br/>
                                <input type="text" class="form-control" name="pais"
                                       value="<?= $contactoData['data']["pais"] ? $contactoData['data']["pais"] : '' ?>" required/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-contacto">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["agregar-redes"])) {
                    $config->set("facebook", $funcion->antihack_mysqli(isset($_POST["s-facebook"]) ? $_POST["s-facebook"] : ''));
                    $config->set("twitter", $funcion->antihack_mysqli(isset($_POST["s-twitter"]) ? $_POST["s-twitter"] : ''));
                    $config->set("instagram", $funcion->antihack_mysqli(isset($_POST["s-instagram"]) ? $_POST["s-instagram"] : ''));
                    $config->set("linkedin", $funcion->antihack_mysqli(isset($_POST["s-linkedin"]) ? $_POST["s-linkedin"] : ''));
                    $config->set("youtube", $funcion->antihack_mysqli(isset($_POST["s-youtube"]) ? $_POST["s-youtube"] : ''));
                    $config->set("googleplus", $funcion->antihack_mysqli(isset($_POST["s-google"]) ? $_POST["s-google"] : ''));
                    $error = $config->addSocial();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=social-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade" id="social-home" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=social-tab">
                        <div class="row mt-5">
                            <label class="col-md-12">
                                Facebook:<br/>
                                <input type="text" class="form-control" name="s-facebook"
                                       value="<?= $socialData['data']["facebook"] ? $socialData['data']["facebook"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Twitter:<br/>
                                <input type="text" class="form-control" name="s-twitter"
                                       value="<?= $socialData['data']["twitter"] ? $socialData['data']["twitter"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Instragram:<br/>
                                <input type="text" class="form-control" name="s-instagram"
                                       value="<?= $socialData['data']["instagram"] ? $socialData['data']["instagram"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Linkedin:<br/>
                                <input type="text" class="form-control" name="s-linkedin"
                                       value="<?= $socialData['data']["linkedin"] ? $socialData['data']["linkedin"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                YouTube:<br/>
                                <input type="text" class="form-control" name="s-youtube"
                                       value="<?= $socialData['data']["youtube"] ? $socialData['data']["youtube"] : '' ?>"/>
                            </label>
                            <label class="col-md-12">
                                Google Plus:<br/>
                                <input type="text" class="form-control" name="s-google"
                                       value="<?= $socialData['data']["googleplus"] ? $socialData['data']["googleplus"] : '' ?>"/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-redes">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["agregar-ml"])) {
                    $config->set("app_id", $funcion->antihack_mysqli(isset($_POST["ml-id"]) ? $_POST["ml-id"] : ''));
                    $config->set("app_secret", $funcion->antihack_mysqli(isset($_POST["ml-secret"]) ? $_POST["ml-secret"] : ''));
                    $error = $config->addMercadoLibre();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=mercadolibre-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade" id="ml-home" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=mercadolibre-tab">
                        <div class="row mt-5">
                            <label class="col-md-12">
                                APP ID:<br/>
                                <input type="text" class="form-control" name="ml-id"
                                       value="<?= $mercadoLibreData['data']["app_id"] ? $mercadoLibreData['data']["app_id"] : '' ?>" required/>
                            </label>
                            <label class="col-md-12">
                                APP SECRET:<br/>
                                <input type="text" class="form-control" name="ml-secret"
                                       value="<?= $mercadoLibreData['data']["app_secret"] ? $mercadoLibreData['data']["app_secret"] : '' ?>" required/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-ml">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["p1-guardar"])) {
                    $config->set("variable1", $funcion->antihack_mysqli(isset($_POST["p1-v1"]) ? $_POST["p1-v1"] : ''));
                    $config->set("variable2", $funcion->antihack_mysqli(isset($_POST["p1-v2"]) ? $_POST["p1-v2"] : ''));
                    $config->set("variable3", $funcion->antihack_mysqli(isset($_POST["p1-v3"]) ? $_POST["p1-v3"] : ''));
                    $config->set("id", 1);
                    $error = $config->updatePayment();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=pagos-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                if (isset($_POST["p2-guardar"])) {
                    $config->set("variable1", $funcion->antihack_mysqli(isset($_POST["p2-v1"]) ? $_POST["p2-v1"] : ''));
                    $config->set("variable2", $funcion->antihack_mysqli(isset($_POST["p2-v2"]) ? $_POST["p2-v2"] : ''));
                    $config->set("variable3", $funcion->antihack_mysqli(isset($_POST["p2-v3"]) ? $_POST["p2-v3"] : ''));
                    $config->set("id",2);
                    $error = $config->updatePayment();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=pagos-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                if (isset($_POST["p3-guardar"])) {
                    $config->set("variable1", $funcion->antihack_mysqli(isset($_POST["p3-v1"]) ? $_POST["p3-v1"] : ''));
                    $config->set("variable2", $funcion->antihack_mysqli(isset($_POST["p3-v2"]) ? $_POST["p3-v2"] : ''));
                    $config->set("variable3", $funcion->antihack_mysqli(isset($_POST["p3-v3"]) ? $_POST["p3-v3"] : ''));
                    $config->set("id",3);
                    $error = $config->updatePayment();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=pagos-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                if (isset($_POST["p4-guardar"])) {
                    $config->set("variable1", $funcion->antihack_mysqli(isset($_POST["p4-v1"]) ? $_POST["p4-v1"] : ''));
                    $config->set("variable2", $funcion->antihack_mysqli(isset($_POST["p4-v2"]) ? $_POST["p4-v2"] : ''));
                    $config->set("variable3", $funcion->antihack_mysqli(isset($_POST["p4-v3"]) ? $_POST["p4-v3"] : ''));
                    $config->set("id",4);
                    $error = $config->updatePayment();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=pagos-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                $empresa1 = $pagosData1['data']["empresa"] ? $pagosData1['data']["empresa"] : '';
                $empresa2 = $pagosData2['data']["empresa"] ? $pagosData2['data']["empresa"] : '';
                $empresa3 = $pagosData3['data']["empresa"] ? $pagosData3['data']["empresa"] : '';
                $empresa4 = $pagosData4['data']["empresa"] ? $pagosData4['data']["empresa"] : '';
                ?>
                <div class="tab-pane fade" id="pagos-home" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <form method="post" class="row" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=pagos-tab">
                        <div class="col-md-2">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="<?= $empresa1 ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="KEY" name="p1-v1"
                                   value="<?= $pagosData1['data']["variable1"] ? $pagosData1['data']["variable1"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="SECRET" name="p1-v2"
                                   value="<?= $pagosData1['data']["variable2"] ? $pagosData1['data']["variable2"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="" name="p1-v3"
                                   value="<?= $pagosData1['data']["variable3"] ? $pagosData1['data']["variable3"] : '' ?>">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary mb-2" name="p1-guardar">Guardar</button>
                        </div>
                    </form>
                    <hr>
                    <form method="post" class="row" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=pagos-tab">
                        <div class="col-md-2">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="<?= $empresa2 ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="KEY" name="p2-v1"
                                   value="<?= $pagosData2['data']["variable1"] ? $pagosData2['data']["variable1"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="SECRET" name="p2-v2"
                                   value="<?= $pagosData2['data']["variable2"] ? $pagosData2['data']["variable2"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="" name="p2-v3"
                                   value="<?= $pagosData2['data']["variable3"] ? $pagosData2['data']["variable3"] : '' ?>">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary mb-2" name="p2-guardar">Guardar</button>
                        </div>
                    </form>
                    <hr>
                    <form method="post" class="row" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=pagos-tab">
                        <div class="col-md-2">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="<?= $empresa3 ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="KEY" name="p3-v1"
                                   value="<?= $pagosData3['data']["variable1"] ? $pagosData3['data']["variable1"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="SECRET" name="p3-v2"
                                   value="<?= $pagosData3['data']["variable2"] ? $pagosData3['data']["variable2"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="" name="p3-v3"
                                   value="<?= $pagosData3['data']["variable3"] ? $pagosData3['data']["variable3"] : '' ?>">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary mb-2" name="p3-guardar">Guardar</button>
                        </div>
                    </form>
                    <hr>
                    <form method="post" class="row" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=pagos-tab">
                        <div class="col-md-2">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="<?= $empresa4 ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="KEY" name="p4-v1"
                                   value="<?= $pagosData4['data']["variable1"] ? $pagosData4['data']["variable1"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="SECRET" name="p4-v2"
                                   value="<?= $pagosData4['data']["variable2"] ? $pagosData4['data']["variable2"] : '' ?>" required>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="" name="p4-v3"
                                   value="<?= $pagosData4['data']["variable3"] ? $pagosData4['data']["variable3"] : '' ?>">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary mb-2" name="p4-guardar">Guardar</button>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($_POST["agregar-captcha"])) {
                    $config->set("captcha_key", $funcion->antihack_mysqli(isset($_POST["c-key"]) ? $_POST["c-key"] : ''));
                    $config->set("captcha_secret", $funcion->antihack_mysqli(isset($_POST["c-secret"]) ? $_POST["c-secret"] : ''));
                    $error = $config->addCaptcha();
                    if ($error) {
                        $funcion->headerMove(URL . '/index.php?op=configuracion&accion=modificar&tab=captcha-tab');
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
                    }
                }
                ?>
                <div class="tab-pane fade" id="captcha-home" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <form method="post" action="<?= URL ?>/index.php?op=configuracion&accion=modificar&tab=captcha-tab">
                        <div class="row mt-5">
                            <label class="col-md-12">
                                CAPTCHA KEY:<br/>
                                <input type="text" class="form-control" name="c-key"
                                       value="<?= $captchaData['data']["captcha_key"] ? $captchaData['data']["captcha_key"] : '' ?>" required/>
                            </label>
                            <label class="col-md-12">
                                CAPTCHA SECRET:<br/>
                                <input type="text" class="form-control" name="c-secret"
                                       value="<?= $captchaData['data']["captcha_secret"] ? $captchaData['data']["captcha_secret"] : '' ?>" required/>
                            </label>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit" name="agregar-captcha">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function () {
        $('#<?= $tab ?>').click();
    })
</script>