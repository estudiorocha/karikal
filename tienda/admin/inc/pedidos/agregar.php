<?php
$pedidos = new Clases\Pedidos();
$detalle = new Clases\DetallePedidos();
$productos = new Clases\Productos();
$carrito = new Clases\Carrito();
$envios = new Clases\Envios();
$pagos = new Clases\Pagos();
$usuarios = new Clases\Usuarios();
//
$carroData = $carrito->return();
$carroEnvio = $carrito->checkEnvio();
$carroPago = $carrito->checkPago();
$productos_array = $productos->list("", "id desc", "50");

$reset = $funcionesNav->antihack_mysqli(isset($_GET['reset']) ? $_GET['reset'] : '0');
if ($reset == 1) {
    unset ($_SESSION['usuarios']);
    $carrito->destroy();
}
if (isset($_GET['usuario'])) {
    $envioData = $carrito->checkEnvio();
    $pagoData = $carrito->checkPago();
    $userCod = $funcionesNav->antihack_mysqli(isset($_GET['usuario']) ? $_GET['usuario'] : '');
    $usuarios->set("cod", $userCod);
    $usuarioData = $usuarios->view();
    if (!empty($usuarioData['estado'])) {
        $usuarios->cod = $usuarioData['cod'];
        $usuarios->nombre = $usuarioData['nombre'];
        $usuarios->apellido = $usuarioData['apellido'];
        $usuarios->email = $usuarioData['email'];
        $usuarios->doc = $usuarioData['doc'];
        $usuarios->direccion = $usuarioData['direccion'];
        $usuarios->localidad = $usuarioData['localidad'];
        $usuarios->provincia = $usuarioData['provincia'];
        $usuarios->telefono = $usuarioData['telefono'];
        $usuarios->userSession();
    } else {
        $funcionesNav->headerMove(URL . '/index.php?op=usuarios&accion=ver');
    }
} else {
    unset ($_SESSION['usuarios']);
    $funcionesNav->headerMove(URL . '/index.php?op=usuarios&accion=ver&pedido=1');
}

if (isset($_POST["buscar"])) {
    $titulo = $funcionesNav->antihack_mysqli(isset($_POST["buscar"]) ? $_POST["buscar"] : '');
    $titulo = explode(" ", $titulo);
    $buscar = '';
    foreach ($titulo as $tit) {
        $buscar .= "titulo like '%$tit%' AND ";
    }
    $productos_array = $productos->list(array(substr($buscar, 0, -4)), "", "");
}
$error = '';
if (isset($_POST["id_carrito"])) {
    $carrito->delete($carroEnvio);
    $carrito->delete($carroPago);
    $carrito->set("id", $_POST['id_carrito']);
    $carrito->set("cantidad", $_POST["cantidad"]);
    $carrito->set("titulo", $_POST['titulo']);
    $carrito->set("precio", $_POST['precio']);
    $carrito->set("stock", $_POST['stock']);
    if ($carrito->add() == false) {
        $error = "El producto que ya está en el carrito en su máximo stock disponible";
    }
    $funcionesNav->headerMove(CANONICAL);
    $carroData = $carrito->return();
    $carroEnvio = $carrito->checkEnvio();
    $carroPago = $carrito->checkPago();
}
?>
<div class="mt-20 col-md-12">
    <div class="row">
        <div class="col-md-8">
            <h4>Agregar Pedido</h4>
        </div>
        <div class="col-md-3">
            <form method="post">
                <input type="text" placeholder="buscar en productos" name="buscar" value="<?= isset($_POST["buscar"]) ? $_POST["buscar"] : '' ?>"/>
            </form>
        </div>
        <div class='pull-right'>
            <a href="<?= URL ?>/index.php?op=pedidos&accion=ver" class="btn btn-success">VER PEDIDOS</a>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr/>
    <?php
    if (isset($_SESSION['usuarios'])) {
        ?>
        <div class="alert alert-success" role="alert">
            Usted está armando un carrito para el usuario: <strong><?= $_SESSION['usuarios']['nombre'] . ' ' . $_SESSION['usuarios']['apellido'] ?></strong><br>
            Si desea eliminar este carrito para hacer uno nuevo con otro usuario, haga click <a href="<?= URL . '/index.php?op=pedidos&accion=agregar&reset=1' ?>">aquí</a>
        </div>
        <?php
    }
    ?>
    <?php
    if (!empty($error)) {
        ?>
        <div class="alert alert-danger" role="alert">
            <?= $error ?>
        </div>
        <?php
    }
    ?>
</div>
<div class="col-md-12">
    <div class="row">
        <!-- CARRITO -->
        <div class="col-md-4">
            <h5>Pedido</h5>
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                <th>PRODUCTO</th>
                <th>PRECIO</th>
                <th>CANTIDAD</th>
                <th>TOTAL</th>
                <th></th>
                </thead>
                <tbody>
                <?php
                if (isset($_GET["remover"])) {
                    $carroPago = $carrito->checkPago();
                    if ($carroPago != '') {
                        $carrito->delete($carroPago);
                    }
                    $carroEnvio = $carrito->checkEnvio();
                    if ($carroEnvio != '') {
                        $carrito->delete($carroEnvio);
                    }
                    $carrito->delete($_GET["remover"]);
                    $funcionesNav->headerMove(URL . "/index.php?op=pedidos&accion=agregar&usuario=" . $_SESSION['usuarios']['cod']);
                }
                $i = 0;
                $precio = 0;
                foreach ($carroData as $key => $carroItem) {
                    $precio += ($carroItem["precio"] * $carroItem["cantidad"]);
                    ?>
                    <tr>
                        <td><b><?= mb_strtoupper($carroItem["titulo"]); ?></b></td>
                        <td><span class="<?= $none ?>"><?= "$" . $carroItem["precio"]; ?></span></td>
                        <td><span class="<?= $none ?>"><?= $carroItem["cantidad"]; ?></span></td>
                        <td>
                            <?php
                            if ($carroItem["precio"] != 0) {
                                echo "$" . ($carroItem["precio"] * $carroItem["cantidad"]);
                            } else {
                                echo "¡Gratis!";
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="<?= CANONICAL ?>&remover=<?= $key ?>"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                <tr>
                    <td><b>TOTAL</b></td>
                    <td></td>
                    <td></td>
                    <td><b>$<?= number_format($carrito->totalPrice(), "2", ",", "."); ?></b></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <div class="envio" id="formulario-envio">
                <?php
                $metodos_de_envios = $envios->list(array("peso >= " . $carrito->finalWeight() . " OR peso = 0"), '', '');
                if ($carroEnvio == '' && !empty($carroData) == true) {
                    echo "<b>Seleccioná el envió que más te convenga:</b>";
                    if (isset($_POST["envio"])) {
                        if ($carroEnvio != '') {
                            $carrito->delete($carroEnvio);
                        }
                        $envio_final = $_POST["envio"];
                        $envios->set("cod", $envio_final);
                        $envio_final_ = $envios->view();
                        $carrito->set("id", "Envio-Seleccion");
                        $carrito->set("cantidad", 1);
                        $carrito->set("titulo", $envio_final_['data']["titulo"]);
                        $carrito->set("precio", $envio_final_['data']["precio"]);
                        $carrito->add();
                        $funcionesNav->headerMove(CANONICAL . "");
                    }
                    ?>
                    <form method="post" id="envio">
                        <select name="envio" class="form-control" id="envio" onchange="this.form.submit()">
                            <option value="" selected disabled>Elegir envío</option>
                            <?php
                            foreach ($metodos_de_envios as $metodos_de_envio_) {
                                if ($metodos_de_envio_['data']["precio"] == 0) {
                                    $metodos_de_envio_precio = "¡Gratis!";
                                } else {
                                    $metodos_de_envio_precio = "$" . $metodos_de_envio_['data']["precio"];
                                }
                                echo "<option value='" . $metodos_de_envio_['data']["cod"] . "'>" . mb_strtoupper($metodos_de_envio_['data']["titulo"]) . " -> " . $metodos_de_envio_precio . "</option>";
                            }
                            ?>
                        </select>
                    </form>
                    <hr/>
                    <?php
                }
                ?>
            </div>
            <div class="pago" id="formulario-pago">
                <form method="post">
                    <?php
                    if ($carroPago == '' && !empty($carroEnvio) == true) {
                        echo "<b>Seleccioná el método de pago que más te convenga:</b>";
                        $metodo = $funcionesNav->antihack_mysqli(isset($_POST["metodos-pago"]) ? $_POST["metodos-pago"] : '');
                        $metodo_get = $funcionesNav->antihack_mysqli(isset($_GET["metodos-pago"]) ? $_GET["metodos-pago"] : '');
                        if ($metodo != '') {
                            $key_metodo = $carrito->checkPago();
                            $carrito->delete($key_metodo);
                            $pagos->set("cod", $metodo);
                            $pago__ = $pagos->view();
                            $precio_final_metodo = $carrito->totalPrice();
                            if ($pago__['data']["aumento"] != 0 || $pago__['data']["disminuir"] != '') {
                                if ($pago__['data']["aumento"]) {
                                    $numero = (($precio_final_metodo * $pago__['data']["aumento"]) / 100);
                                    $carrito->set("id", "Metodo-Pago");
                                    $carrito->set("cantidad", 1);
                                    $carrito->set("titulo", "CARGO +" . $pago__['data']['aumento'] . "% / " . mb_strtoupper($pago__['data']["titulo"]));
                                    $carrito->set("precio", $numero);
                                    $carrito->add();
                                    $funcionesNav->headerMove(CANONICAL . "");
                                } else {
                                    $numero = (($precio_final_metodo * $pago__['data']["disminuir"]) / 100);
                                    $carrito->set("id", "Metodo-Pago");
                                    $carrito->set("cantidad", 1);
                                    $carrito->set("titulo", "DESCUENTO -" . $pago__['data']['disminuir'] . "% / " . mb_strtoupper($pago__['data']["titulo"]));
                                    $carrito->set("precio", "-" . $numero);
                                    $carrito->add();
                                    $funcionesNav->headerMove(CANONICAL . "");
                                }
                            }
                        }
                        ?>
                        <div class="form-bd">
                            <?php $lista_pagos = $pagos->list(array(" estado = 1 "), '', ''); ?>
                            <select name="metodos-pago" class="form-control" id="metodos-pago" onchange="this.form.submit()">
                                <option value="" selected disabled>Elegir metodo de pago</option>
                                <?php
                                foreach ($lista_pagos as $pago) {
                                    echo "<option value='" . $pago['data']["cod"] . "'>" . mb_strtoupper($pago['data']["titulo"]) . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </form>
            </div>
            <div class="usuario" id="formulario-usuario">
                <?php
                if ($carroPago != '' && $carroEnvio != '' && isset($_SESSION['usuarios']) == true) {
                    if (isset($_POST["registrarmeBtn2"])) {
                        $factura = $funcionesNav->antihack_mysqli(isset($_POST["factura"]) ? $_POST["factura"] : '0');
                        if ($factura == 1) {
                            $factura_ = "<b>SOLICITÓ FACTURA A CON EL CUIT: </b>" . $_SESSION["usuarios"]["doc"];
                        } else {
                            $factura_ = '';
                        }
                        $precio = $carrito->totalPrice();
                        $fecha = date("Y-m-d");

                        $pedidos->set("cod", $_SESSION["cod_pedido"]);
                        $pedidos->set("total", $precio);
                        $pedidos->set("estado", 1);
                        $pedidos->set("tipo", $carroData[$carroPago]["titulo"]);
                        $pedidos->set("usuario", $_SESSION['usuarios']['cod']);
                        $pedidos->set("detalle", $factura_);
                        $pedidos->set("fecha", $fecha);
                        $pedidos->set("hub_cod", "");
                        $pedidos->add();

                        foreach ($carroData as $carroItem) {
                            if ($carroItem['id'] != "Envio-Seleccion" && $carroItem['id'] != "Metodo-Pago") {
                                $productoData = $productos->list(array("id='" . $carroItem['id'] . "'"), '', 1);
                                foreach ($productoData as $prod_) {
                                    $productos->set("cod", $prod_['data']["cod"]);
                                    $productos->editSingle("stock", $prod_['data']['stock'] - $carroItem['cantidad']);
                                }
                            }
                            $detalle->set("cod", $_SESSION["cod_pedido"]);
                            $detalle->set("producto", $carroItem["titulo"]);
                            $detalle->set("cantidad", $carroItem["cantidad"]);
                            $detalle->set("precio", $carroItem["precio"]);
                            $detalle->set("variable1", "");
                            $detalle->set("variable2", "");
                            $detalle->set("variable3", "");
                            $detalle->set("variable4", "");
                            $detalle->set("variable5", "");
                            $detalle->add();
                        }
                        unset($_SESSION["cod_pedido"]);
                        unset($_SESSION["usuarios"]);
                        $carrito->destroy();
                        echo "<script>alert('Perfecto, tu carrito fue cargado exitosamente');</script>";
                        $funcionesNav->headerMove(URL . "/index.php?op=usuarios&accion=ver");
                    }
                    $docGet = $funcionesNav->antihack_mysqli(isset($_GET["doc"]) ? $_GET["doc"] : '0');
                    ?>
                    <form method="post">
                        <?php
                        if ($docGet == 1) {
                            ?>
                            <label class="col-md-12 col-xs-12 mt-10 mb-10" style="font-size:16px">
                                <input type="checkbox" disabled checked> Solicitar FACTURA A
                                <input type="hidden" name="factura" value="1">
                            </label>
                            <?php
                        } else {
                            if (!empty($_SESSION['usuarios']['doc'])) {
                                ?>
                                <label class="col-md-12 col-xs-12 mt-10 mb-10" style="font-size:16px">
                                    <input type="checkbox" name="factura" value="1"> Solicitar FACTURA A
                                </label>
                                <?php
                            } else {
                                ?>
                                <label class="col-md-12 col-xs-12 mt-10 mb-10" style="font-size:16px">
                                    <input onclick="document.location.href='<?= URL . '/index.php?op=usuarios&accion=modificar&cod=' . $usuarioData['data']['cod'] . '&pedido=2'; ?>'"
                                           type="checkbox" name="factura" value="0"> Solicitar FACTURA A
                                </label>
                                <?php
                            }
                        }
                        ?>
                        <div class="col-md-12 col-xs-12 mb-50">
                            <input class="btn btn-success" type="submit" value="¡Finalizar la compra!" name="registrarmeBtn2"/>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>
        <!-- FIN CARRITO -->
        <!-- LISTADO Y MODAL -->
        <div class="col-md-8">
            <h5>Productos</h5>
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                <th>PRODUCTO</th>
                <th>STOCK</th>
                <th>PRECIO</th>
                <th></th>
                </thead>
                <tbody>

                <?php
                foreach ($productos_array as $producto_) {
                    echo "<tr>";
                    echo "<td>" . mb_strtoupper($producto_['data']["titulo"]) . "</td>";
                    echo "<td>" . $producto_['data']["stock"] . "</td>";
                    echo "<td>$" . $producto_['data']["precio"] . "</td>";
                    ?>
                    <td>
                        <?php
                        if (!empty($producto_['data']['precio']) && $producto_['data']['stock'] > 0) {
                            ?>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal<?= $producto_['data']["id"]; ?>"><i class="fa fa-shopping-cart"></i> Agregar carrito</button>
                            <?php
                        }
                        ?>
                    </td>
                    <?php
                    echo "</tr>";
                    ?>
                    <div id="myModal<?= $producto_['data']["id"]; ?>" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="pull-left modal-title">Agregar a Carrito</h4>
                                </div>
                                <div class="modal-body" id="contenidoForm">
                                    <form class="agregarACarrito" method="post" action="<?= CANONICAL ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <b>Título: </b>
                                                <input type="hidden" name="id_carrito" class="form-control" value="<?= $producto_['data']["id"]; ?>"/>
                                                <input type="hidden" name="stock" class="form-control" value="<?= $producto_['data']["stock"]; ?>"/>
                                                <input type="text" name="titulo" readonly value="<?= $producto_['data']["titulo"]; ?>"/>
                                            </div>
                                            <div class="col-md-6 mt-10">
                                                <b>Precio: </b>
                                                <input type="text" readonly name="precio" class="form-control" value="<?= $producto_['data']["precio"]; ?>" required=""/>
                                            </div>
                                            <div class="col-md-6 mt-10">
                                                <b>Cantidad: </b>
                                                <input type="number" name="cantidad" min="1" class="form-control" value="1" max="<?= $producto_['data']["stock"]; ?>" required=""/>
                                            </div>
                                            <div class="col-md-12">
                                                <hr/>
                                            </div>
                                            <div class="col-md-6 mt-10">
                                                <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"> Cancelar</button>
                                            </div>
                                            <div class="col-md-6 mt-10">
                                                <button type="submit" class="pull-right btn btn-success btn-sm" name="enviar">Agregar a carrito</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <!-- FIN LISTADO Y MODAL -->
    </div>
</div>
