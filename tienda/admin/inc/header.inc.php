<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://bootswatch.com/4/simplex/bootstrap.min.css"/>
<link rel="stylesheet" href="<?= URL ?>/css/style.css"/>
<meta charset="UTF-8"/>
<title><?= TITULO_ADMIN ?></title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?= URL ?>/js/bootstrap-input-spinner.js"></script>

<?php
$config = new Clases\Config();
$meli = new Meli($config->meli["data"]["app_id"], $config->meli["data"]["app_secret"]);
?>