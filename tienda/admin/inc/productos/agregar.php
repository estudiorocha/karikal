<?php
$productos = new Clases\Productos();
$imagenes = new Clases\Imagenes();
$zebra = new Clases\Zebra_Image();
$categoria = new Clases\Categorias();

$cod = $funcionesNav->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');
$borrarImg = $funcionesNav->antihack_mysqli(isset($_GET["borrarImg"]) ? $_GET["borrarImg"] : '');
$categoriasData = $categoria->list(array("area = 'productos'"), "titulo ASC", "");

if (isset($_POST["agregar"])) {
    $count = 0;
    $cod = substr(md5(uniqid(rand())), 0, 10);
    $img_meli = '';
    $productos->set("cod", $funcionesNav->antihack_mysqli(isset($cod) ? $cod : ''));
    $productos->set("titulo", $funcionesNav->antihack_mysqli(isset($_POST["titulo"]) ? $_POST["titulo"] : ''));
    $productos->set("cod_producto", $funcionesNav->antihack_mysqli(isset($_POST["cod_producto"]) ? $_POST["cod_producto"] : ''));
    $productos->set("precio", $funcionesNav->antihack_mysqli(isset($_POST["precio"]) ? $_POST["precio"] : ''));
    $productos->set("precio_descuento", $funcionesNav->antihack_mysqli(isset($_POST["precio_descuento"]) ? $_POST["precio_descuento"] : ''));
    $productos->set("stock", $funcionesNav->antihack_mysqli(isset($_POST["stock"]) ? $_POST["stock"] : ''));
    $productos->set("desarrollo", $funcionesNav->antihack_mysqli(isset($_POST["desarrollo"]) ? $_POST["desarrollo"] : ''));
    $productos->set("categoria", $funcionesNav->antihack_mysqli(isset($_POST["categoria"]) ? $_POST["categoria"] : ''));
    $productos->set("subcategoria", $funcionesNav->antihack_mysqli(isset($_POST["subcategoria"]) ? $_POST["subcategoria"] : ''));
    $productos->set("keywords", $funcionesNav->antihack_mysqli(isset($_POST["keywords"]) ? $_POST["keywords"] : ''));
    $productos->set("description", $funcionesNav->antihack_mysqli(isset($_POST["description"]) ? $_POST["description"] : ''));
    $productos->set("fecha", $funcionesNav->antihack_mysqli(isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d")));
    $productos->set("meli", $funcionesNav->antihack_mysqli(isset($_POST["meli"]) ? $_POST["meli"] : ''));
    $productos->set("url", $funcionesNav->antihack_mysqli(isset($_POST["url"]) ? $_POST["url"] : ''));

    $cod_meli = $funcionesNav->antihack_mysqli(isset($_POST["cod_meli"]) ? $_POST["cod_meli"] : null);


    foreach ($_FILES['files']['name'] as $f => $name) {
        $imgInicio = $_FILES["files"]["tmp_name"][$f];
        $tucadena = $_FILES["files"]["name"][$f];
        $partes = explode(".", $tucadena);
        $dom = (count($partes) - 1);
        $dominio = $partes[$dom];
        $prefijo = substr(md5(uniqid(rand())), 0, 10);
        if ($dominio != '') {
            $destinoFinal = "../assets/archivos/" . $prefijo . "." . $dominio;
            move_uploaded_file($imgInicio, $destinoFinal);
            chmod($destinoFinal, 0777);
            $destinoRecortado = "../assets/archivos/recortadas/a_" . $prefijo . "." . $dominio;

            $zebra->source_path = $destinoFinal;
            $zebra->target_path = $destinoRecortado;
            $zebra->jpeg_quality = 80;
            $zebra->preserve_aspect_ratio = true;
            $zebra->enlarge_smaller_images = true;
            $zebra->preserve_time = true;

            if ($zebra->resize(800, 700, ZEBRA_IMAGE_NOT_BOXED)) {
                unlink($destinoFinal);
            }

            $imagenes->set("cod", $cod);
            $imagenes->set("ruta", str_replace("../", "", $destinoRecortado));
            $img_meli .= '{"source":"' . URLSITE . str_replace("../", "/", $destinoRecortado) . '"},';
            $imagenes->add();
        }
        $count++;
    }

    $error = '';
    if (isset($_POST['meli'])) {
        if (isset($_SESSION['access_token'])) {
            $productos->img = $img_meli;
            $add_meli = $productos->addMeli();
            if (isset($add_meli['error'])) {
                foreach ($add_meli['error'] as $err) {
                    $error .= "- " . $err['message'] . "<br>";
                }
            } else {
                $productos->set("meli", $add_meli['data']["id"]);
                $productos->add();
                $funcionesNav->headerMove(URL . "/index.php?op=productos");
            }
        } else {
            $error = "No te logueaste con MercadoLibre.";
        }
    } else {
        if ($cod_meli != null) {
            $productos->meli = $cod_meli;
            if ($productos->validateItem() != false) {
                $productos->set("meli", $cod_meli);
                $productos->add();
                $funcionesNav->headerMove(URL . "/index.php?op=productos");
            } else {
                $error = "El código de producto en MercadoLibre ingresado es incorrecto.";
            }
        } else {
            $productos->add();
            $funcionesNav->headerMove(URL . "/index.php?op=productos");
        }
    }
}
?>

<div class="col-md-12">
    <h4>
        Productos
    </h4>
    <hr/>
    <?php
    if (!empty($error)) {
        ?>
        <div class="alert alert-danger" role="alert"><?= $error; ?></div>
        <?php
    }
    ?>
    <form method="post" class="row" enctype="multipart/form-data">
        <label class="col-md-4">Título:<br/>
            <input type="text" name="titulo" value="<?= isset($_POST["titulo"]) ? $_POST["titulo"] : ''; ?>" required>
        </label>
        <label class="col-md-3">
            Categoría:<br/>
            <select name="categoria">
                <option value="">-- categorías --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                    echo "<option value='" . $categoria["data"]["cod"] . "'>" . mb_strtoupper($categoria["data"]["titulo"]) . "</option>";
                }
                ?>
            </select>
        </label>
        <label class="col-md-3">
            Subcategoría:<br/>
            <select name="subcategoria">
                <option value="">-- Sin subcategoría --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                    ?>
                    <optgroup label="<?= mb_strtoupper($categoria["data"]['titulo']) ?>">
                        <?php
                        foreach ($categoria["subcategories"] as $subcategorias) {
                            echo "<option value='" . $subcategorias["data"]["cod"] . "'>" . mb_strtoupper($subcategorias["data"]["titulo"]) . "</option>";
                        }
                        ?>
                    </optgroup>
                    <?php
                }
                ?>
            </select>
        </label>
        <label class="col-md-2">Código:<br/>
            <input type="text" name="cod_producto" value="<?= isset($_POST["cod_producto"]) ? $_POST["cod_producto"] : ''; ?>">
        </label>
        <label class="col-md-3">Precio:<br/>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="number" class="form-control" min="0" id="precio" value="<?= isset($_POST["precio"]) ? $_POST["precio"] : 0; ?>" name="precio" required>
            </div>
        </label>
        <label class="col-md-3">Precio descuento:<br/>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="number" class="form-control" min="0" value="<?= isset($_POST["precio_descuento"]) ? $_POST["precio_descuento"] : 0; ?>" name="precio_descuento">
            </div>
        </label>


        <label class="col-md-1">Stock:<br/>
            <input type="number" name="stock" id="stock" min="0" value="<?= isset($_POST["stock"]) ? $_POST["stock"] : 0; ?>" required>
        </label>
        <div class="clearfix">
        </div>

        <div class="clearfix">
        </div>
        <label class="col-md-12">
            Desarrollo:<br/>
            <textarea name="desarrollo" class="ckeditorTextarea"><?= isset($_POST["desarrollo"]) ? $_POST["desarrollo"] : ''; ?></textarea>
        </label>
        <div class="clearfix">
        </div>
        <label class="col-md-12">
            Palabras claves dividas por ,<br/>
            <input type="text" name="keywords" value="<?= isset($_POST["keywords"]) ? $_POST["keywords"] : ''; ?>">
        </label>
        <label class="col-md-12">
            Descripción breve<br/>
            <textarea name="description"><?= isset($_POST["description"]) ? $_POST["description"] : ''; ?></textarea>
        </label>
        <br/>
        <hr>
        <div class="col-md-12">
            <div class="form-group form-check">
                <?php
                if (isset($_SESSION['access_token'])) {
                    ?>
                    <div class="row">
                        <div class="col-md-6 centro" style="margin-top:5px;">
                            <input type="checkbox" class="form-check-input" id="meli" value="1" name="meli" onchange="$('#cod_meli').attr('disabled',true); $('#stock').attr('min', 1);">
                            <label class="form-check-label display-inline" for="meli">¿Publicar en MercadoLibre?</label>
                        </div>
                        <div class="col-md-6 centro">
                            <label> Ya está publicado en ML? Ingresar código:</label>
                            <input type="text" class="col-md-3 display-inline" id="cod_meli" name="cod_meli" onkeypress="$('#meli').attr('disabled',true)" value="<?= isset($_POST["cod_meli"]) ? $_POST["cod_meli"] : ''; ?>">
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="ml-0 pl-0 mt-20 mb-20">
                        <?php echo '<a class="nav-link" id="mercadolibre-nav" target="_blank" href="' . $meli->getAuthUrl(URL, Meli::$AUTH_URL["MLA"]) . '"><img src="' . URL . '/img/meli.png" width="30" /> ¿Vincularme a Mercadolibre <i class="fa fa-square green">?</i></a>'; ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <hr>
        <label class="col-md-7 mb-40">
            Imágenes:<br/>
            <input type="file" id="file" name="files[]" multiple="multiple" accept="image/*"/>
        </label>
        <div class="clearfix">
        </div>
        <br/>
        <div class="col-md-12">
            <input type="submit" class="btn btn-primary" name="agregar" value="Crear Producto"/>
        </div>
    </form>
</div>
<script>
    $("#pes").inputSpinner();

    setInterval(ML, 1000);

    function ML() {
        if ($('#meli').prop('checked') == false && $('#cod_meli').val() == '') {
            $('#cod_meli').attr('disabled', false);
            $('#meli').attr('disabled', false);
            $('#stock').attr('min', 0);
        }
    }
</script>