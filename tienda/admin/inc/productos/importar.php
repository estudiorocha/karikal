<?php
$productos = new Clases\Productos();
$imagenes = new Clases\Imagenes();
$conexion = new Clases\Conexion();
$con = $conexion->con();
include "../vendor/phpoffice/phpexcel/Classes/PHPExcel.php";
require "../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php";
?>
<div class="col-md-12">
    <form action="index.php?op=productos&accion=importar" method="post" enctype="multipart/form-data">
        <h3>Importar productos de Excel a la Web (<a href="upload/modelo.xlsx" target="_blank">descargar modelo</a>)
        </h3>
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <input type="file" name="excel" class="form-control" required/>
            </div>
            <div class="col-md-12 mt-10 mb-10">
                <label>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="defaultUnchecked" name="vaciar" value="0" checked>
                        <label class="custom-control-label" for="defaultUnchecked">Vaciar base de datos e importar la base de datos</label>
                    </div>
                </label>
            </div>
            <div class="col-md-6">
                <input type="submit" name="submit" value="Verifica e importar archivo" class='btn  btn-info'/>
            </div>
        </div>
    </form>
    <?php
    if (isset($_POST['submit'])) {
        if (isset($_FILES['excel']['name']) && $_FILES['excel']['name'] != "") {
            $allowedExtensions = array("xls", "xlsx");
            $objPHPExcel = PHPEXCEL_IOFactory::load($_FILES['excel']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $numCols = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
            $numCols = (ord(strtolower($numCols)) - 96);

            if (isset($_POST["vaciar"])) {
                $productos->truncate();
            }

            $importar = '';
            if ($numCols == 10) {
                for ($row = 2; $row <= $numRows; $row++) {
                    $cod = substr(md5(uniqid(rand())), 0, 5);
                    $titulo = $objPHPExcel->getActiveSheet()->getCell("A" . $row)->getCalculatedValue();
                    $precio = $objPHPExcel->getActiveSheet()->getCell("B" . $row)->getCalculatedValue();
                    $peso = $objPHPExcel->getActiveSheet()->getCell("C" . $row)->getCalculatedValue();
                    $precio_mayorista = $objPHPExcel->getActiveSheet()->getCell("D" . $row)->getCalculatedValue();
                    $precio_descuento = $objPHPExcel->getActiveSheet()->getCell("E" . $row)->getCalculatedValue();
                    $stock = $objPHPExcel->getActiveSheet()->getCell("F" . $row)->getCalculatedValue();
                    $desarrollo = $objPHPExcel->getActiveSheet()->getCell("G" . $row)->getCalculatedValue();
                    $categoria = $objPHPExcel->getActiveSheet()->getCell("H" . $row)->getCalculatedValue();
                    $subcategoria = $objPHPExcel->getActiveSheet()->getCell("I" . $row)->getCalculatedValue();
                    $cod_producto = $objPHPExcel->getActiveSheet()->getCell("j" . $row)->getCalculatedValue();

                    $productos->set("cod", $funcionesNav->antihack_mysqli(isset($cod) ? $cod : substr(md5(uniqid(rand())), 0, 5)));
                    $productos->set("titulo", $funcionesNav->antihack_mysqli(isset($titulo) ? $titulo : ''));
                    $productos->set("peso", $funcionesNav->antihack_mysqli(isset($peso) ? $peso : 0));
                    $productos->set("precio", $funcionesNav->antihack_mysqli(isset($precio) ? $precio : 0));
                    $productos->set("precio_mayorista", $funcionesNav->antihack_mysqli(isset($precio_mayorista) ? $precio_mayorista : 0));
                    $productos->set("precio_descuento", $funcionesNav->antihack_mysqli(isset($precio_descuento) ? $precio_descuento : 0));
                    $productos->set("stock", $funcionesNav->antihack_mysqli(isset($stock) ? $stock : 0));
                    $productos->set("desarrollo", $funcionesNav->antihack_mysqli(isset($desarrollo) ? $desarrollo : ''));
                    $productos->set("categoria", $funcionesNav->antihack_mysqli(isset($categoria) ? $categoria : ''));
                    $productos->set("subcategoria", $funcionesNav->antihack_mysqli(isset($subcategoria) ? $subcategoria : ''));
                    $productos->set("cod_producto", $funcionesNav->antihack_mysqli(isset($cod_producto) ? $cod_producto : ''));
                    $productos->set("fecha", date('Y-m-d'));
                    $importar .= $productos->add();
                }

                $productos->query($importar);
                //echo $importar;
            } else {
                echo '<span class="alert alert-danger">Hay errores en el excel que intetas subir. Descargar aquí el ejemplo</span>';
            }
        } else {
            echo '<span class="alert alert-danger">Seleccionar primero el archivo a subir.</span>';
        }
    }
    ?>
</div>
