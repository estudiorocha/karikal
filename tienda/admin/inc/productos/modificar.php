<?php
$productos = new Clases\Productos();
$imagenes = new Clases\Imagenes();
$zebra = new Clases\Zebra_Image();
$categoria = new Clases\Categorias();

$cod = $funcionesNav->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');
$borrarMeli = $funcionesNav->antihack_mysqli(isset($_GET["borrarMeli"]) ? $_GET["borrarMeli"] : '');
$borrarImg = $funcionesNav->antihack_mysqli(isset($_GET["borrarImg"]) ? $_GET["borrarImg"] : '');
$categoriasData = $categoria->list(array("area = 'productos'"), "titulo ASC", "");

$productos->set("cod", $cod);
$producto = $productos->view();

//CAMBIAR ORDEN DE LAS IMAGENES
if (isset($_GET["ordenImg"]) && isset($_GET["idImg"])) {
    $imagenes->set("id", $_GET["idImg"]);
    $imagenes->set("orden", $_GET["ordenImg"]);
    $imagenes->setOrder();
    $funcionesNav->headerMove(URL . "/index.php?op=productos&accion=modificar&cod=$cod");
}

//BORRAR IMAGEN
if ($borrarImg != '') {
    $imagenes->set("id", $borrarImg);
    $imagenes->delete();
    $funcionesNav->headerMove(URL . "/index.php?op=productos&accion=modificar&cod=$cod");
}

//Borrar meli cod
if ($borrarMeli != '') {
    $productos->editSingle("meli", "null");
    $funcionesNav->headerMove(URL . "/index.php?op=productos&accion=modificar&cod=$cod");
}

if (isset($_POST["agregar"])) {
    $count = 0;
    $cod = $producto["data"]["cod"];
    $productos->set("id", $producto["data"]["id"]);
    $productos->set("cod", $cod);
    $productos->set("titulo", $funcionesNav->antihack_mysqli(isset($_POST["titulo"]) ? $_POST["titulo"] : ''));
    $productos->set("cod_producto", $funcionesNav->antihack_mysqli(isset($_POST["cod_producto"]) ? $_POST["cod_producto"] : ''));
    $productos->set("precio", $funcionesNav->antihack_mysqli(isset($_POST["precio"]) ? $_POST["precio"] : ''));
    $productos->set("precio_descuento", $funcionesNav->antihack_mysqli(isset($_POST["precio_descuento"]) ? $_POST["precio_descuento"] : ''));
    $productos->set("precio_mayorista", $funcionesNav->antihack_mysqli(isset($_POST["precio_mayorista"]) ? $_POST["precio_mayorista"] : ''));
    $productos->set("peso", $funcionesNav->antihack_mysqli(isset($_POST["peso"]) ? $_POST["peso"] : 0));
    $stock = $funcionesNav->antihack_mysqli(isset($_POST["stock"]) ? $_POST["stock"] : 0);
    $productos->set("stock", $stock);
    $productos->set("desarrollo", $funcionesNav->antihack_mysqli(isset($_POST["desarrollo"]) ? $_POST["desarrollo"] : ''));
    $productos->set("categoria", $funcionesNav->antihack_mysqli(isset($_POST["categoria"]) ? $_POST["categoria"] : ''));
    $productos->set("subcategoria", $funcionesNav->antihack_mysqli(isset($_POST["subcategoria"]) ? $_POST["subcategoria"] : ''));
    $productos->set("keywords", $funcionesNav->antihack_mysqli(isset($_POST["keywords"]) ? $_POST["keywords"] : ''));
    $productos->set("description", $funcionesNav->antihack_mysqli(isset($_POST["description"]) ? $_POST["description"] : ''));
    $productos->set("fecha", $funcionesNav->antihack_mysqli(isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d")));
    $productos->set("meli", $funcionesNav->antihack_mysqli(isset($_POST["meli"]) ? $_POST["meli"] : ''));
    $productos->set("url", $funcionesNav->antihack_mysqli(isset($_POST["url"]) ? $_POST["url"] : ''));

    $cod_meli = $funcionesNav->antihack_mysqli(isset($_POST["cod_meli"]) ? $_POST["cod_meli"] : null);

    $productos->set("variable1", "");
    $productos->set("variable2", "");
    $productos->set("variable3", $funcionesNav->antihack_mysqli(isset($_POST["unidades"]) ? $_POST["unidades"] : ''));
    $productos->set("variable4", $funcionesNav->antihack_mysqli(isset($_POST["unidad"]) ? $_POST["unidad"] : ''));
    $productos->set("variable5", $funcionesNav->antihack_mysqli(isset($_POST["medidas"]) ? $_POST["medidas"] : ''));
    $productos->set("variable6", $funcionesNav->antihack_mysqli(isset($_POST["densidad"]) ? $_POST["densidad"] : ''));
    $productos->set("variable7", "");
    $productos->set("variable8", "");
    $productos->set("variable9", "");
    $productos->set("variable10", "");

    foreach ($_FILES['files']['name'] as $f => $name) {
        $imgInicio = $_FILES["files"]["tmp_name"][$f];
        $tucadena = $_FILES["files"]["name"][$f];
        $partes = explode(".", $tucadena);
        $dom = (count($partes) - 1);
        $dominio = $partes[$dom];
        $prefijo = substr(md5(uniqid(rand())), 0, 10);
        if ($dominio != '') {
            $destinoFinal = "../assets/archivos/" . $prefijo . "." . $dominio;
            move_uploaded_file($imgInicio, $destinoFinal);
            chmod($destinoFinal, 0777);
            $destinoRecortado = "../assets/archivos/recortadas/a_" . $prefijo . "." . $dominio;

            $zebra->source_path = $destinoFinal;
            $zebra->target_path = $destinoRecortado;
            $zebra->jpeg_quality = 80;
            $zebra->preserve_aspect_ratio = true;
            $zebra->enlarge_smaller_images = true;
            $zebra->preserve_time = true;

            if ($zebra->resize(800, 700, ZEBRA_IMAGE_NOT_BOXED)) {
                unlink($destinoFinal);
            }

            $imagenes->set("cod", $cod);
            $imagenes->set("ruta", str_replace("../", "", $destinoRecortado));
            $imagenes->add();
        }
        $count++;
    }

    $error = '';
    if (isset($_POST['meli'])) {
        $img_meli = '';
        $imgMeliPreview = $imagenes->list(array("cod = '$cod'"), 'orden ASC', '');
        foreach ($imgMeliPreview as $imgMeli) {
            $img_meli .= '{"source":"' . URLSITE . "/" . $imgMeli["ruta"] . '"},';
        }
        if (isset($_SESSION['access_token'])) {
            if ($producto['data']["meli"] == '') {
                $productos->img = $img_meli;
                $_meli = $productos->addMeli();
                if (isset($_meli['error'])) {
                    foreach ($_meli['error'] as $err) {
                        $error .= "- " . $err['message'] . "<br>";
                    }
                } else {
                    $productos->set("meli", $_meli["id"]);
                    $productos->edit();
                    $funcionesNav->headerMove(URL . "/index.php?op=productos");
                }
            } else {
                $productos->meli = $producto['data']['meli'];
                $productos->img = $img_meli;
                $productos->stock = $stock;
                $_meli = $productos->editMeli();
                if (isset($_meli['error'])) {
                    $error .= "- " . $_meli['error'] . "<br>";
                } else {
                    $productos->set("meli", $producto['data']['meli']);
                    $productos->set("stock", $stock);
                    $productos->edit();
                    $funcionesNav->headerMove(URL . "/index.php?op=productos");
                }
            }
        } else {
            $error = "No te logueaste con MercadoLibre.";
        }
    } else {
        if ($cod_meli != null) {
            $productos->meli = $cod_meli;
            if ($productos->validateItem() != false) {
                $productos->set("meli", $cod_meli);
                $productos->edit();
                $funcionesNav->headerMove(URL . "/index.php?op=productos");
            } else {
                $error = "El código de producto en MercadoLibre ingresado es incorrecto.";
            }
        } else {
            $productos->edit();
            $funcionesNav->headerMove(URL . "/index.php?op=productos");
        }
    }
}
?>
<div class="col-md-12 ">
    <h4>Productos</h4>
    <hr/>
    <?php
    if (!empty($error)) {
        ?>
        <div class="alert alert-danger" role="alert"><?= $error; ?></div>
        <?php
    }
    ?>
    <form method="post" class="row" enctype="multipart/form-data">
        <label class="col-md-4">Título:<br/>
            <input type="text" name="titulo" value="<?= $producto["data"]["titulo"] ?>">
        </label>
        <label class="col-md-3">
            Categoría:<br/>
            <select name="categoria">
                <option value="">-- categorías --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                    if ($producto["data"]["categoria"] == $categoria["data"]["cod"]) {
                        echo "<option value='" . $categoria["data"]["cod"] . "' selected>" . mb_strtoupper($categoria["data"]["titulo"]) . "</option>";
                    } else {
                        echo "<option value='" . $categoria["data"]["cod"] . "'>" . mb_strtoupper($categoria["data"]["titulo"]) . "</option>";
                    }
                }
                ?>
            </select>
        </label>
        <label class="col-md-3">
            Subcategoría:<br/>
            <select name="subcategoria">
                <option value="">-- Sin subcategoría --</option>
                <?php
                foreach ($categoriasData as $categoria) {
                    ?>
                    <optgroup label="<?= mb_strtoupper($categoria["data"]['titulo']) ?>">
                        <?php
                        foreach ($categoria["subcategories"] as $subcategorias) {
                            if ($producto["data"]["subcategoria"] == $subcategorias["data"]["cod"]) {
                                echo "<option value='" . $subcategorias["data"]["cod"] . "' selected>" . mb_strtoupper($subcategorias["data"]["titulo"]) . "</option>";
                            } else {
                                echo "<option value='" . $subcategorias["data"]["cod"] . "'>" . mb_strtoupper($subcategorias["data"]["titulo"]) . "</option>";
                            }
                        }
                        ?>
                    </optgroup>
                    <?php
                }
                ?>
            </select>
        </label>
        <label class="col-md-2">Código:<br/>
            <input type="text" name="cod_producto" value="<?= $producto["data"]["cod_producto"] ?>">
        </label>
        <label class="col-md-3">Precio:<br/>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="number" class="form-control" min="0" value="<?= $producto["data"]["precio"] ? $producto["data"]["precio"] : '0' ?>" name="precio" required>
            </div>
        </label>
        <label class="col-md-3">Precio descuento:<br/>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="number" class="form-control" min="0" value="<?= $producto["data"]["precio_descuento"] ? $producto["data"]["precio_descuento"] : '' ?>" name="precio_descuento">
            </div>
        </label>

        <label class="col-md-1">Stock:<br/>
            <input type="number" name="stock" id="stock" min="0" value="<?= $producto["data"]["stock"] ? $producto["data"]["stock"] : '0' ?>" required>
        </label>
        <div class="clearfix">
        </div>

        <div class="clearfix"></div>
        <label class="col-md-12">Desarrollo:<br/>
            <textarea name="desarrollo" class="ckeditorTextarea"><?= $producto["data"]["desarrollo"] ?></textarea>
        </label>
        <div class="clearfix"></div>
        <label class="col-md-12">Palabras claves dividas por ,<br/>
            <input type="text" name="keywords" value="<?= $producto["data"]["keywords"] ?>">
        </label>
        <label class="col-md-12">Descripción breve<br/>
            <textarea name="description"><?= $producto["data"]["description"] ?></textarea>
        </label>
        <br/>
        <div class="col-md-12">
            <div class="form-group form-check">
                <?php
                if ($producto["data"]["meli"] == '') {
                    if (isset($_SESSION['access_token'])) {
                        ?>
                        <div class="row">
                            <div class="col-md-6 centro" style="margin-top:5px;">
                                <input type="checkbox" class="form-check-input" id="meli" value="1" name="meli" onchange="$('#cod_meli').attr('disabled',true); $('#stock').attr('min', 1);">
                                <label class="form-check-label display-inline" for="meli">¿Publicar en MercadoLibre?</label>
                            </div>
                            <div class="col-md-6 centro">
                                <label> Ya está publicado en ML? Ingresar código:</label>
                                <input type="text" class="col-md-3 display-inline" id="cod_meli" name="cod_meli" onkeypress="$('#meli').attr('disabled',true)" value="<?= isset($_POST["cod_meli"]) ? $_POST["cod_meli"] : ''; ?>">
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="ml-0 pl-0 mt-20 mb-20">
                            <?php echo '<a class="nav-link" id="mercadolibre-nav" target="_blank" href="' . $meli->getAuthUrl(URL, Meli::$AUTH_URL["MLA"]) . '"><img src="' . URL . '/img/meli.png" width="30" /> ¿Vincularme a Mercadolibre <i class="fa fa-square green">?</i></a>'; ?>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <input type="hidden" name="meli" value="<?= $producto["data"]["meli"] ?>"/>
                    <div class="col-md-3" style="margin-left: -35px;">
                        <div class="alert alert-success ml-cross" role="alert">
                            <a href="<?= CANONICAL ?>&borrarMeli=1">
                                <i class="fa fa-times green" title="Eliminar vinculación con MercadoLibre"></i>
                            </a>
                            <b> ID MERCADOLIBRE :</b> <?= $producto["data"]["meli"] ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <br/>
        <div class="col-md-12">
            <div class="row">
                <?php
                if (!empty($producto['images'])) {
                    foreach ($producto['images'] as $img) {
                        ?>
                        <div class='col-md-2 mb-20 mt-20'>
                            <div style="height:200px;background:url(<?= '../' . $img['ruta']; ?>) no-repeat center center/contain;">
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <a href="<?= URL . '/index.php?op=productos&accion=modificar&cod=' . $img['cod'] . '&borrarImg=' . $img['id'] ?>" class="btn btn-sm btn-block btn-danger">
                                        BORRAR IMAGEN
                                    </a>
                                </div>
                                <div class="col-md-5 text-right">
                                    <select onchange='$(location).attr("href", "<?= CANONICAL ?>&idImg=<?= $img["id"] ?>&ordenImg="+$(this).val())'>
                                        <?php
                                        for ($i = 0; $i <= count($producto['images']); $i++) {
                                            if ($img["orden"] == $i) {
                                                echo "<option value='$i' selected>$i</option>";
                                            } else {
                                                echo "<option value='$i'>$i</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                    <i>orden</i>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <label class="col-md-7">Imágenes:<br/>
            <input type="file" id="file" name="files[]" multiple="multiple" accept="image/*"/>
        </label>
        <br/>
        <div class="clearfix"><br/></div>
        <div class="col-md-12">
            <input type="submit" class="btn btn-primary" name="agregar" value="Modificar Producto"/>
        </div>
</div>
<script>
    $("#pes").inputSpinner();

    setInterval(ML, 1000);

    function ML() {
        if ($('#meli').prop('checked') == false && $('#cod_meli').val() == '') {
            $('#cod_meli').attr('disabled', false);
            $('#meli').attr('disabled', false);
            $('#stock').attr('min', 0);
        }
    }
</script>