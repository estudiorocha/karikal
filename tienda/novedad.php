<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
//
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$novedad = new Clases\Novedades();
//Gets
$cod = $funciones->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');
///Datos
$novedad->set("cod", $cod);
$blogData = $novedad->view();
if (empty($blogData)) {
    $funciones->headerMove(URL . '/blog');
}
$fecha_ = explode('-', $blogData['data']['fecha']);
switch ($fecha_[1]) {
    case 1:
        $mes = 'ENE';
        break;
    case 2:
        $mes = 'FEB';
        break;
    case 3:
        $mes = 'MAR';
        break;
    case 4:
        $mes = 'ABR';
        break;
    case 5:
        $mes = 'MAY';
        break;
    case 6:
        $mes = 'HUN';
        break;
    case 7:
        $mes = 'JUL';
        break;
    case 8:
        $mes = 'AGO';
        break;
    case 9:
        $mes = 'SEP';
        break;
    case 10:
        $mes = 'OCT';
        break;
    case 11:
        $mes = 'NOV';
        break;
    case 12:
        $mes = 'DIC';
        break;
    default:
        $mes = '';
        break;
}
//
//
$template->set("title", $blogData['data']['titulo'] . " | " . TITULO);
$template->set("description", ucfirst(substr(strip_tags($blogData['data']['desarrollo']), 0, 160)));
$template->set("keywords", $blogData['data']['keywords']);
$template->set("body", "home");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2><?= $blogData['data']['titulo']; ?></h2>
                    </div>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 mb-30-xs">
                <div class="row">
                    <!-- Entry Article #1 -->
                    <div class="col-xs-12 col-sm-12 col-md-12 entry">
                        <div class="entry-img">
                            <div class="your-class">
                                <img src="<?= URL . '/' . $blogData['images'][0]['ruta'] ?>">
                            </div>
                        </div>
                        <!-- .entry-img end -->
                        <div class="entry-meta clearfix">
                            <ul class="pull-left">
                                <li class="entry-date">
                                    <span><?= $mes ?></span>
                                    <?= $fecha_[2] ?> </li>
                            </ul>
                        </div>
                        <!-- .entry-meta end -->
                        <div class="entry-title">
                            <a href="#">
                                <h3><?= $blogData['data']['titulo'] ?></h3>
                            </a>
                        </div>
                        <!-- .entry-title end -->
                        <div class="entry-content">
                            <p> <?php
                                if (!empty($blogData['data']['desarrollo'])) {
                                    echo $blogData['data']['desarrollo'];
                                }
                                ?>
                            </p>
                        </div>
                        <!-- .entry-content end -->
                        <div class="entry-share text-right text-center-xs">
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_google_gmail"></a>
                                <a class="a2a_button_whatsapp"></a>
                            </div>
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->
                        </div>
                        <!-- .entry-share end -->
                    </div>
                    <!-- .entry end -->
                </div>
                <!-- .row end -->
            </div>
            <!-- entry articles end -->
            <?php
            include("assets/inc/novedades/side.inc.php");
            ?>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>

<?php
$template->themeEnd();
?>
