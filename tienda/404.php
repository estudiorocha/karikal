<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
//
$template->set("title", "Página no encontrada | " . TITULO);
$template->set("description", "");
$template->set("keywords", "");
$template->set("body", "");
$template->themeInit();
?>
<!-- 404
============================================= -->
<section class="error-page text-center">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="error-title">
                    <h1>404</h1>
                </div>
            </div>
            <!-- .col-md-6 end -->
        </div>
        <!-- .row end -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="error-content mt-50">
                    <p>No se encontro la página!</p>
                    <a class="btn btn-primary" href="<?= URL ?>">INICIO</a>
                </div>
            </div>
            <!-- .col-md-6 end-->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end  -->
</section>
<?php
$template->themeEnd();
?>
