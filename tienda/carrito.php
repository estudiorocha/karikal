<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$template->set("title", "Carrito de compra | " . TITULO);
$template->set("description", "Carrito de compra " . TITULO);
$template->set("keywords", "Carrito de compra " . TITULO);
$template->themeInit();
//Clases
$carrito = new Clases\Carrito();
$envios = new Clases\Envios();
$pagos = new Clases\Pagos();
$descuento = new Clases\Descuentos();
$usuario = new Clases\Usuarios();
//
$usuarioData = $usuario->viewSession();
$carro = $carrito->return();
if (count($carro) == 0) {
    $funciones->headerMove(URL . "/tienda");
}
if (!empty($usuarioData)) {
    $descuentoCheck = $descuento->descuentoCheck($carro, $usuarioData);
} else {
    $descuentoCheck = $descuento->descuentoCheck($carro);
}

if ($descuentoCheck["refresh"] == 1) {
    $funciones->headerMove(URL . "/carrito");
}
$metodo_get = isset($_GET["metodos-pago"]) ? $_GET["metodos-pago"] : '';
$carroEnvio = $carrito->checkEnvio();
$carroPago = $carrito->checkPago();
if ($carroPago != '') {
    $metodo_get_explode = explode(" / ", $carro[$carroPago]['titulo']);
    if (isset($metodo_get_explode[1])) {
        $metodo_get_session = $pagos->list(array("titulo = '" . $metodo_get_explode[1] . "'"), "", "");
        $metodo_get = $metodo_get_session[0]["data"]["cod"];
    } else {
        $metodo_get_session = $pagos->list(array("titulo = '" . $metodo_get_explode[0] . "'"), "", "");
        $metodo_get = $metodo_get_session[0]["data"]["cod"];
    }
}
?>
    <!-- Page Title #4
       ============================================= -->
    <section class="bg-overlay bg-overlay-gradient pb-0 pt-0" id="bre">
        <div class="bg-section">
            <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="page-title title-4 pt-90">
                        <div class="pull-left">
                            <h2>CARRITO DE COMPRA</h2>
                        </div>
                        <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                            <li>
                                <a href="<?= URL ?>">INICIO</a>
                            </li>
                            <li class="active">CARRITO</li>
                        </ol>
                    </div>
                    <!-- .page-title end -->
                </div>
                <!-- .col-md-12 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <!-- Cart
    ============================================= -->
    <div class="section-empty">
        <div class="container content pt-15">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $envio_ok = 0;

                    $metodos_de_envios = $envios->list('', '', '');

                    $precioFinal = $carrito->totalPrice();

                    if ($carroEnvio == '') {
                        ?>
                        <div id="formEnvio" class="alert alert-warning animated fadeIn">
                            <?php
                            if (isset($_POST["envio"])) {
                                if ($carroEnvio != '') {
                                    $carrito->delete($carroEnvio);
                                }
                                $envio_final = $_POST["envio"];
                                $envios->set("cod", $envio_final);
                                $envio_final_ = $envios->view();
                                $carrito->set("id", "Envio-Seleccion");
                                $carrito->set("cantidad", 1);
                                $carrito->set("titulo", $envio_final_['data']["titulo"]);
                                $carrito->set("precio", $envio_final_['data']["precio"]);
                                $carrito->add();
                                $funciones->headerMove(CANONICAL . "");
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-uppercase bold fs-20 text-center mb-10 mt-15">1º Elegí el tipo de envío para tus productos:</p>
                                </div>
                                <div class="col-md-6">
                                    <form method="post" class="mb-0 pb-0">
                                        <select name="envio" class="form-control text-uppercase mb-0 pb-0" id="envio" onchange="this.form.submit()" style="background-color: white;margin-bottom: 0px; color: black;">
                                            <option value="" selected disabled>Elegir método de envío</option>
                                            <?php
                                            foreach ($metodos_de_envios as $key => $metodos_de_envio_) {
                                                if ($metodos_de_envio_['data']["precio"] == 0) {
                                                    $metodos_de_envio_precio = "";
                                                } else {
                                                    $metodos_de_envio_precio = " -> $" . $metodos_de_envio_['data']["precio"];
                                                }
                                                echo "<option value='" . $metodos_de_envio_['data']["cod"] . "'>" . $metodos_de_envio_['data']["titulo"] . $metodos_de_envio_precio . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php
                    }
                    if (!empty($carroEnvio)) {
                        $carroPago = $carrito->checkPago();
                        if (empty($carroPago)) {
                            ?>
                            <div id="formPago" class="alert alert-warning animated fadeIn">
                                <?php
                                $metodo = $funciones->antihack_mysqli(isset($_POST["metodos-pago"]) ? $_POST["metodos-pago"] : '');
                                if ($metodo != '') {
                                    $key_metodo = $carrito->checkPago();
                                    $carrito->delete($key_metodo);
                                    $pagos->set("cod", $metodo);
                                    $pago__ = $pagos->view();
                                    $precio_final_metodo = $carrito->totalPrice();
                                    if ($pago__['data']["aumento"] != 0 || $pago__['data']["disminuir"] != '') {
                                        if ($pago__['data']["aumento"]) {
                                            $numero = (($precio_final_metodo * $pago__['data']["aumento"]) / 100);
                                            $carrito->set("titulo", "CARGO +" . $pago__['data']['aumento'] . "% / " . mb_strtoupper($pago__['data']["titulo"]));
                                            $carrito->set("precio", $numero);
                                        } else {
                                            $numero = (($precio_final_metodo * $pago__['data']["disminuir"]) / 100);
                                            $carrito->set("titulo", "DESCUENTO -" . $pago__['data']['disminuir'] . "% / " . mb_strtoupper($pago__['data']["titulo"]));
                                            $carrito->set("precio", "-" . $numero);
                                        }
                                        $funciones->headerMove(CANONICAL . "/" . $metodo);
                                    } else {
                                        $carrito->set("titulo", mb_strtoupper($pago__['data']["titulo"]));
                                        $carrito->set("precio", 0);
                                    }
                                    $carrito->set("id", "Metodo-Pago");
                                    $carrito->set("cantidad", 1);
                                    $carrito->add();
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="text-uppercase bold fs-20 text-center mb-10 mt-15">2° Elegí el medio de pago que desea relizar. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <form method="post" class="mb-0 pb-0">
                                            <select name="metodos-pago" class="form-control text-uppercase" id="pago" onchange="this.form.submit()" style="background-color: white;margin-bottom: 0px; color: black;">
                                                <option value="" selected disabled>Elegir método de pago</option>
                                                <?php
                                                $lista_pagos = $pagos->list(array(" estado = 1 "), '', '');
                                                foreach ($lista_pagos as $pago) {
                                                    $precio_total = $carrito->precioSinMetodoDePago();
                                                    if ($pago['data']["aumento"] != 0 || $pago['data']["disminuir"] != 0) {
                                                        if ($pago['data']["aumento"] > 0) {
                                                            $precio_total = (($precio_total * $pago['data']["aumento"]) / 100) + $precio_total;
                                                        } else {
                                                            $precio_total = $precio_total - (($precio_total * $pago['data']["disminuir"]) / 100);
                                                        }
                                                    }
                                                    echo "<option value='" . $pago['data']["cod"] . "'>" . $pago['data']["titulo"] . " -> Total: $" . $precio_total . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </form>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                            <?php
                        }
                    }
                    //Eliminar
                    if (isset($_GET["remover"])) {
                        $carroPago = $carrito->checkPago();
                        if ($carroPago != '') {
                            $carrito->delete($carroPago);
                        }
                        $carroEnvio = $carrito->checkEnvio();
                        if ($carroEnvio != '') {
                            $carrito->delete($carroEnvio);
                        }
                        $carrito->delete($_GET["remover"]);
                        $funciones->headerMove(URL . "/carrito");
                    }
                    ?>
                    <table class="table table-striped">
                        <thead>
                        <th>Nombre producto</th>
                        <th class="hidden-xs hidden-sm">Cantidad</th>
                        <th>Precio unidad</th>
                        <th>Precio total</th>
                        <th></th>
                        </thead>
                        <?php
                        $i = 0;
                        $precio = 0;
                        foreach ($carro as $key => $carroItem) {
                            $precio += ($carroItem["precio"] * $carroItem["cantidad"]);
                            $opciones = @implode(" - ", $carroItem["opciones"]);
                            if ($carroItem["id"] == "Envio-Seleccion" || $carroItem["id"] == "Metodo-Pago") {
                                $clase = "text-bold";
                                $none = "hidden";
                            } else {
                                $clase;
                                $none = "";
                            }
                            ?>
                            <tr>
                                <td>
                                    <?= mb_strtoupper($carroItem["titulo"]); ?>
                                    <?php
                                    if (isset($carroItem["descuento"]["monto"])) {
                                        ?>
                                        <b class="descuento-monto"><?= $carroItem["descuento"]["monto"]; ?></b>
                                        <?php
                                    }
                                    ?>
                                    <span class="amount hidden-md hidden-lg <?= $none ?>">Cantidad: <?= $carroItem["cantidad"]; ?></span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="amount <?= $none ?>"><?= $carroItem["cantidad"]; ?></span>
                                </td>
                                <td>
                                    <span class="amount <?= $none ?>"><?= "$" . $carroItem["precio"]; ?></span>
                                    <?php if (isset($carroItem["descuento"]["precio-antiguo"])) { ?>
                                        <span class="descuento-precio"><?= $carroItem["descuento"]["precio-antiguo"]; ?></span>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php
                                    if ($carroItem["precio"] != 0) {
                                        echo "$" . ($carroItem["precio"] * $carroItem["cantidad"]);
                                    } else {
                                        echo "";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="<?= URL ?>/carrito.php?remover=<?= $key ?>">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </table>
                    <hr>
                    <?php
                    if ($carroEnvio != '' && $carroPago != '') {
                        if (isset($_POST["btn_codigo"])) {
                            $codigoDescuento = $funciones->antihack_mysqli(isset($_POST["codigoDescuento"]) ? $_POST["codigoDescuento"] : '');
                            $descuento->set("cod", $codigoDescuento);
                            if (!empty($usuarioData)) {
                                $response = $descuento->refreshCart($carro, $usuarioData);
                            } else {
                                $response = $descuento->refreshCart($carro);
                            }
                            if ($response['alert'] == 1) {
                                $funciones->headerMove(URL . "/carrito");
                            } else {
                                $mensaje = '';
                                switch ($response['error']) {
                                    //su cuenta no aplica para este descuento
                                    case 2:
                                        $mensaje = 'Este descuento no puede ser utilizable.';
                                        break;
                                    //el descuento todavia no comenzo.
                                    case 3:
                                        $mensaje = 'El descuento aún no comenzó.';
                                        break;
                                    //el descuento ya finalizo.
                                    default:
                                        $mensaje = 'El descuento no existe o ya expiró.';
                                        break;
                                }
                                echo "<div class='alert alert-danger'>" . $mensaje . "</div>";
                            }
                        }
                        if ($descuentoCheck['check'] == 1) {
                            ?>
                            <div class="col-md-12">
                                <div class="alert alert-warning animated fadeIn">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>CODIGO DE DESCUENTO APLICADO: <b><?= $descuentoCheck['cod'] ?></b></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-md-12">
                                <form method="post" class="row">
                                    <div class="col-md-6 text-right">
                                        <p class="text-uppercase mt-15"><b>¿Tenés algún código de descuento para tus compras?</b></p>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="codigoDescuento" class="form-control mb-0" placeholder="CÓDIGO DE DESCUENTO">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="submit" value="USAR CÓDIGO" name="btn_codigo" class="btn btn-default btn-block pt-14 pb-14 "/>
                                    </div>
                                </form>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <!-- Cart Totals Start -->
                                <div class="col-md-12 col-sm-12" id="buy">
                                    <div class="cart_totals float-md-right text-md-right" style="text-align: center;">
                                        <hr>
                                        <div class="mb-20">
                                            <strong class="text-uppercase" style="font-size: 36px;">Total: $<?= number_format($carrito->totalPrice(), "2", ",", "."); ?></strong>
                                        </div>
                                        <?php
                                        if (!empty(floatval($carrito->totalPrice()))) {
                                            if ($carroEnvio != '' && $carroPago != '') {
                                                ?>
                                                <div class="mt-20 wc-proceed-to-checkout">
                                                    <a class="btn btn-success btn-block btn-lg" href="<?= URL ?>/pagar/<?= $metodo_get ?>">
                                                        <i class="fa fa-check"></i> Finalizar Carrito
                                                    </a>
                                                </div>
                                                <?php
                                            } elseif ($carroEnvio == '' && $carroPago != '') {
                                                ?>
                                                <div class="mt-20 wc-proceed-to-checkout">
                                                    <a class="btn btn-info btn-block" href="#bre">
                                                        <i class="fa fa-check"></i> Seleccionar Método de Envío
                                                    </a>
                                                </div>
                                                <?php
                                            } elseif ($carroEnvio != '' && $carroPago == '') {
                                                ?>
                                                <div class="mt-20 wc-proceed-to-checkout">
                                                    <a class="btn btn-info btn-block" href="#bre">
                                                        <i class="fa fa-check"></i> Seleccionar Método de Pago
                                                    </a>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="mt-20 wc-proceed-to-checkout">
                                                    <a class="btn btn-info btn-block" href="#bre">
                                                        <i class="fa fa-check"></i> Seleccionar Método de Envío
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                    <hr>
                                </div>
                                <br>
                                <!-- Cart Totals End -->
                            </div>
                            <!-- Row End -->
                            <!-- Form End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$template->themeEnd();
?>