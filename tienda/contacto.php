<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$config = new Clases\Config();
//
$contactData = $config->viewContact();
//
$template->set("title", "Contacto | " . TITULO);
$template->set("description", "Por cualquier duda o consulta no dudes en contactarte con nosotros.");
$template->set("keywords", "");
$template->set("body", "");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>CONTACTO</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">CONTACTO</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- Contact #1
============================================= -->
<section id="contact" class="contact">
    <div class="container">
        <div class="row">
            <!-- .col-md-12 end -->
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 widgets-contact mb-60-xs">
                        <div class="widget">
                            <div class="widget-contact-icon pull-left">
                                <i class="lnr lnr-map"></i>
                            </div>
                            <div class="widget-contact-info">
                                <p class="text-capitalize">Dirección</p>
                                <p class="font-heading ml-86"><?= $contactData['data']['domicilio'] ?> , <?= $contactData['data']['localidad'] ?>, <?= $contactData['data']['provincia'] ?>.</p>
                            </div>
                        </div>
                        <!-- .widget end -->
                        <div class="clearfix">
                        </div>
                        <div class="widget">
                            <div class="widget-contact-icon pull-left">
                                <i class="lnr lnr-envelope"></i>
                            </div>
                            <div class="widget-contact-info">
                                <p class="text-capitalize ">Email</p>
                                <p class="font-heading"><?= $contactData['data']['email'] ?></p>
                            </div>
                        </div>
                        <!-- .widget end -->
                        <div class="clearfix">
                        </div>
                        <div class="widget">
                            <div class="widget-contact-icon pull-left">
                                <i class="lnr lnr-phone"></i>
                            </div>
                            <div class="widget-contact-info">
                                <p class="text-capitalize">Teléfono</p>
                                <p class="font-heading"><?= $contactData['data']['telefono'] ?></p>
                            </div>
                        </div>
                        <!-- .widget end -->
                    </div>
                    <!-- .col-md-4 end -->
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
                                <script>
                                    hbspt.forms.create({
                                        portalId: "4368046",
                                        formId: "8f8f397b-1d20-43c8-bada-9c9302146a78"
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- .col-md-8 end -->
                </div>
                <!-- .row end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
