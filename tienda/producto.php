<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
//
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$producto = new Clases\Productos();
$carrito = new Clases\Carrito();
//Gets
$cod = $funciones->antihack_mysqli(isset($_GET["cod"]) ? $_GET["cod"] : '');
///Datos
$producto->set("cod", $cod);
$productData = $producto->view();
if (empty($productData)) {
    $funciones->headerMove(URL);
}
$carro = $carrito->return();
$url_limpia = CANONICAL;
$url_limpia = str_replace("?success", "", $url_limpia);
$url_limpia = str_replace("?error", "", $url_limpia);
$url_limpia = str_replace("?alertA1", "", $url_limpia);
$url_limpia = str_replace("?alertA2", "", $url_limpia);
//
$template->set("title", $productData['data']['titulo'] . " | " . TITULO);
$template->set("description", ucfirst(substr(strip_tags($productData['data']['desarrollo']), 0, 160)));
$template->set("keywords", $productData['data']['keywords']);
$template->set("body", "");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2><?= mb_strtoupper($productData['data']['titulo']) ?></h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>/tienda">TIENDA</a>
                        </li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- Shop
============================================= -->
<section class="shop pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 shop-content">
                <!-- .aret end -->
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5">
                        <!-- Product Image -->
                        <div class="producto-img product-feature-img mb-80">
                            <?php
                            if (@count($productData['images']) > 1) {
                                ?>
                                <div id="project-carousel" class="project-carousel mb-30">
                                    <?php
                                    foreach ($productData['images'] as $img) {
                                        ?>
                                        <div class="item">
                                            <img src="<?= URL ?>/<?= $img['ruta'] ?>">
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            } else {
                                foreach ($productData['images'] as $img) {
                                    ?>
                                    <div class="item">
                                        <img src="<?= URL ?>/<?= $img['ruta'] ?>">
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <!-- .project-carousel end -->
                        </div>
                        <!-- .product-img end -->
                        <div class="hidden-xs hidden-sm">
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_google_gmail"></a>
                                <a class="a2a_button_whatsapp"></a>
                            </div>
                            <!-- AddToAny END -->
                        </div>
                    </div>
                    <hr>
                    <div class="col-xs-12 col-sm-12 col-md-7 mt-5">
                        <!-- Product Content -->
                        <div class="product-content">
                            <div class="product-title text-center-xs producto-tile">
                                <h3 id="title"><?= $productData['data']['titulo'] ?></h3>
                            </div>
                            <!-- .product-img end -->
                            <div class="product-meta mb-30">
                                <div class="product-price">
                                    <p>PRECIO:
                                        $ <?= $productData['data']['precio'] ?> </p>
                                    <p>CONTADO:
                                        $ <?= $productData['data']['precio'] * 0.90 ?> </p>
                                </div>
                                <!-- .product-price end -->
                            </div>
                            <!-- .product-img end -->
                            <div class="product-desc text-center-xs">
                                <p><?= $productData['data']['description'] ?></p>
                            </div>
                            <!-- .product-desc end -->
                            <hr class="mt-10 mb-30">
                            <?php
                            if ($productData['data']['stock'] > 0 && !empty($productData['data']['precio'])) {
                                if (isset($_POST['agregar-carrito'])) {
                                    $carroEnvio = $carrito->checkEnvio();
                                    if ($carroEnvio != '') {
                                        $carrito->delete($carroEnvio);
                                    }

                                    $carroPago = $carrito->checkPago();
                                    if ($carroPago != '') {
                                        $carrito->delete($carroPago);
                                    }
                                    $carrito->set("id", $productData['data']['cod']);
                                    $carrito->set("cantidad", $_POST["cantidad"]);
                                    $carrito->set("titulo", $productData['data']['titulo']);
                                    $carrito->set("precio", $productData['data']['precio']);
                                    $carrito->set("stock", $productData['data']['stock']);
                                    $carrito->set("peso", $productData['data']['peso']);

                                    if ($carrito->add()) {
                                        $funciones->headerMove($url_limpia . "?success#title");
                                    } else {
                                        $funciones->headerMove($url_limpia . "?error#title");
                                    }
                                }
                                if (strpos(CANONICAL, "alertA1") == true) {
                                    echo "<div id='alerta' class='alert alert-danger'>Ingreso un numero incorrecto, este producto se vende por caja y la cantidad por caja es de 2 metros cuadrados.</div>";
                                }
                                if (strpos(CANONICAL, "alertA2") == true) {
                                    echo "<div id='alerta' class='alert alert-danger'>Ingreso un numero incorrecto, este producto se vende por caja y la cantidad por caja es de 3 metros lineal.</div>";
                                }
                                ?>
                                <form method="post">
                                    <?php if ($productData['data']['stock'] > 0) { ?>
                                        <div class="product-action">
                                            <div class="col-md-6">
                                                <div class="producto-quantity product-quantity pull-left pull-none-xs">
                                                    <span class="qua">CANTIDAD:</span><br>
                                                        <a onclick="$('#quantity').val(parseInt($('#quantity').val()) - 1 )">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
							                            <input type="number"
                                                               value="1"
                                                               min="0"
                                                               id="quantity"
                                                               name="cantidad"
                                                               step="1"
                                                               oninvalid="this.setCustomValidity('Ingresar un numero válido.')"
                                                               oninput="this.setCustomValidity('')"
                                                               onkeydown="return (event.keyCode!=13);" 
                                                               style="width: 75px;display: inline-block;"
                                                               required>
							                            <a onclick="$('#quantity').val( parseInt($('#quantity').val()) + 1 )">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="product-cta text-right text-center-xs producto-carrito">
                                                    <button type="submit" id="agregar" name="agregar-carrito"
                                                            class="btn btn-primary">
                                                        <i class="fa fa-shopping-cart"></i> AGREGAR AL CARRITO
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!-- .product-action end -->
                                </form>
                            <?php } ?>
                        </div>

                        <!-- .product-content -->
                    </div>
                </div>
                <!-- Pager -->
                <div class="row">
                    <hr>
                    <div class="col-md-12">
                        <p><?= $productData['data']['desarrollo'] ?></p>
                        <div class="hidden-md hidden-lg">
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_google_gmail"></a>
                                <a class="a2a_button_whatsapp"></a>
                            </div>
                            <!-- AddToAny END -->
                        </div>
                    </div>
                </div>
                <!-- .row end -->
            </div>
            <!-- .shop-content end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<div id="modalS" class="modal fade mt-120" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <i class="fa fa-check-circle fs-90 verde"></i>
                    <br>
                    <span class="text-uppercase fs-18">¡FELICITACIONES! AGREGASTE UN NUEVO PRODUCTO A TU CARRITO:</span>
                    <br/>
                    <a href="<?= URL . '/carrito' ?>" class="btn mt-20 fs-18 text-uppercase btn-success btn-block"><b>pasar por caja</b></a>

                    <a href="<?= URL . '/tienda' ?>" class="text-uppercase fs-12"><b>seguir
                            comprando</b></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalE" class="modal fade mt-120" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-center">
                    <i class="fa fa-exclamation-circle fs-90 rojo"></i>
                    <br>
                    <span class="text-uppercase fs-16">LO SENTIMOS NO CONTAMOS CON ESA CANTIDAD EN STOCK.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (strpos(CANONICAL, "success") == true) {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#modalS').modal('toggle');
        });
    </script>
    <?php
}
if (strpos(CANONICAL, "error") == true) {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#modalE').modal('toggle');
        });
    </script>
    <?php
}
$template->themeEnd();
?>

