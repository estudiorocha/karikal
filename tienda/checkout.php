<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$template->set("title", "Cierre de compra | " . TITULO);
$template->set("description", "Finalizá tu compra eligiendo tu medio de pago y la forma de envío");
$template->set("keywords", "");
$template->set("favicon", LOGO);
$template->themeInit();
$usuarios = new Clases\Usuarios();
$funciones = new Clases\PublicFunction();
$carrito = new Clases\Carrito();
$pedidos = new Clases\Pedidos();
$detalle = new Clases\DetallePedidos();
$pagos = new Clases\Pagos();
$descuento = new Clases\Descuentos();
$config = new Clases\Config();
$hub = new Clases\Hubspot();
//
$usuarioSesion = $usuarios->viewSession();
if (empty($usuarioSesion)||empty($_SESSION['carrito'])) {
    $funciones->headerMove(URL);
}

$cod_pedido = $funciones->antihack_mysqli(isset($_GET["cod_pedido"]) ? $_GET["cod_pedido"] : '');
$tipo_pedido = $funciones->antihack_mysqli(isset($_GET["tipo_pedido"]) ? $_GET["tipo_pedido"] : '');
$factura = $funciones->antihack_mysqli(isset($_GET["fact"]) ? $_GET["fact"] : '');


$pedidos->set("cod", $cod_pedido);
$pedido = $pedidos->view();

$pagos->set("cod", $tipo_pedido);
$pago = $pagos->view();

$usuarios->set("cod", $usuarioSesion['cod']);
$userData = $usuarios->view();
$factura_ = '';
if (!empty($pago['data']['leyenda'])) {
    $factura_ .= "<b>DESCRIPCIÓN DEL PAGO: </b>" . $pago['data']['leyenda'] . "<br/>";
} else {
    $factura_ .= '';
}

if (!empty($factura)) {
    $factura_ .= "<b>SOLICITÓ FACTURA A CON EL CUIT: </b>" . $userData["doc"] . "<br/>";
} else {
    $factura_ .= '';
}

$carro = $carrito->return();
if (!empty($usuarioSesion)) {
    $descuentoCheck = $descuento->descuentoCheck($carro, $usuarioSesion);
} else {
    $descuentoCheck = $descuento->descuentoCheck($carro);
}
if (isset($descuentoCheck['cod'])) {
    if (!empty($descuentoCheck['cod'])) {
        $factura_ .= "<b>SE UTILIZÓ EL CÓDIGO DE DESCUENTO: </b>" . $descuentoCheck['cod'];
    }
}
$precio = $carrito->totalPrice();

$timezone = -3;
$fecha = gmdate("Y-m-j H:i:s", time() + 3600 * ($timezone + date("I")));

//Hubspot
$descripcion = '';
foreach ($carro as $c) {
    if ($c['precio'] != 0) {
        $price = $c['precio'] * $c['cantidad'];
    } else {
        $price = "Gratis";
    }
    switch ($c['id']) {
        case "Envio-Seleccion":
            $descripcion .= "Envio: " . $c['titulo'] . " precio: $" . $price . " |||";
            break;
        case "Metodo-Pago":
            $descripcion .= "Método de pago: " . $c['titulo'] . " precio: $" . $price . " |||";
            break;
        default:
            $descripcion .= "Producto: " . $c['titulo'] . " precio: $" . $price . " |||";
            break;
    }
}
$hub->set("email", $_SESSION['usuarios']['email']);
$response = $hub->getContactByEmail();
$vid = $response['vid'];
$hub->set("vid", $vid);
$hub->set("titulo", "Pedido WEB: " . $cod_pedido);
$stage = $hub->getStage(0);
$hub->set("estado", $stage);
$hub->set("total", $precio);
$hub->set("descripcion", $descripcion);
$response = $hub->createDeal();
$hubcod = '';
if ($response != false) {
    $hubcod = $response['dealId'];
}
//
if (!empty($pedido['data'])) {
    $pedidos->set("cod", $cod_pedido);
//    $pedidos->delete();
    $pedidoViejo = $pedidos->view();

//    $pedidos->set("cod", $cod_pedido);
    $pedidos->set("total", $precio);
    $pedidos->set("estado", 0);
    $pedidos->set("tipo", $pago['data']["titulo"]);
    $pedidos->set("usuario", $usuarioSesion["cod"]);
    $pedidos->set("detalle", $factura_);
    $pedidos->set("fecha", $fecha);
    $pedidos->set("hub_cod", $hubcod);
    $pedidos->edit();

    foreach ($carro as $carroItem) {
        $detalle->set("cod", $cod_pedido);
        $detalle->set("producto", $carroItem["titulo"]);
        $detalle->set("cantidad", $carroItem["cantidad"]);
        $detalle->set("precio", $carroItem["precio"]);
        if ($carroItem['id'] == "Envio-Seleccion") {
            $detalle->set("variable1", "ME");
        } else {
            if ($carroItem['id'] == "Metodo-Pago") {
                $detalle->set("variable1", "MP");
            } else {
                $detalle->set("variable1", "");
            }
        }
        $detalle->set("variable2", "");
        $detalle->set("variable3", "");
        $detalle->set("variable4", "");
        $detalle->set("variable5", "");
        $detalle->add();
    }

    if (!empty($pedidoViejo['detail'])) {
        foreach ($pedidoViejo['detail'] as $detail) {
            $detalle->deleteById($detail['id']);
        }
    }
} else {
    $pedidos->set("cod", $cod_pedido);
    $pedidos->set("total", $precio);
    $pedidos->set("estado", 0);
    $pedidos->set("tipo", $pago['data']["titulo"]);
    $pedidos->set("usuario", $usuarioSesion["cod"]);
    $pedidos->set("detalle", $factura_);
    $pedidos->set("fecha", $fecha);
    $pedidos->set("hub_cod", $hubcod);
    $pedidos->add();

    foreach ($carro as $carroItem) {
        $detalle->set("cod", $cod_pedido);
        $detalle->set("producto", $carroItem["titulo"]);
        $detalle->set("cantidad", $carroItem["cantidad"]);
        $detalle->set("precio", $carroItem["precio"]);
        if ($carroItem['id'] == "Envio-Seleccion") {
            $detalle->set("variable1", "ME");
        } else {
            if ($carroItem['id'] == "Metodo-Pago") {
                $detalle->set("variable1", "MP");
            } else {
                $detalle->set("variable1", "");
            }
        }
        $detalle->set("variable2", "");
        $detalle->set("variable3", "");
        $detalle->set("variable4", "");
        $detalle->set("variable5", "");
        $detalle->add();
    }
}
switch ($pago['data']["tipo"]) {
    case 0:
        $pedidos->set("cod", $cod_pedido);
        $pedidos->set("estado", $pago['data']["defecto"]);
        $pedidos->changeState();
        $pedido_info = $pedidos->view();
        $stage = $hub->getStage($pago['data']["defecto"]);
        $hub->set("deal", $pedido_info['data']['hub_cod']);
        $hub->set("estado", $stage);
        $hub->updateStage();
        $funciones->headerMove(URL . "/compra-finalizada");
        break;
    case 1:
        include("vendor/mercadopago/sdk/lib/mercadopago.php");
        $config->set("id", 1);
        $paymentsData = $config->viewPayment();
        $mp = new MP ($paymentsData['data']['variable1'], $paymentsData['data']['variable2']);
        $preference_data = [
            "items" => [
                [
                    "id" => $cod_pedido,
                    "title" => "COMPRA CÓDIGO N°:" . $cod_pedido,
                    "quantity" => 1,
                    "currency_id" => "ARS",
                    "unit_price" => $precio
                ]
            ],
            "payer" => [
                "name" => $usuarioSesion["nombre"],
                "surname" => $usuarioSesion["apellido"],
                "email" => $usuarioSesion["email"]
            ],
            "back_urls" => [
                "success" => URL . "/compra-finalizada.php?estado=2",
                "pending" => URL . "/compra-finalizada.php?estado=1",
                "failure" => URL . "/compra-finalizada.php?estado=0"
            ],
            "external_reference" => $cod_pedido,
            "auto_return" => "all",
//"client_id" => $usuarioSesion["cod"],
            "payment_methods" => [
                "excluded_payment_methods" => [],
                "excluded_payment_types" => []
            ]
        ];
        $preference = $mp->create_preference($preference_data);
        $funciones->headerMove($preference["response"]["init_point"]);
        break;
    case 2:
        break;
    case 3:
        break;
    case 4:
        break;
}

$template->themeEnd();
?>
