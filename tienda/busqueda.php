<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
//
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$producto = new Clases\Productos();
$novedad = new Clases\Novedades();
//Gets
$search = $funciones->antihack_mysqli(isset($_GET["titulo"]) ? $_GET["titulo"] : '');
if (empty($search)) {
    $funciones->headerMove(URL);
}
//
$template->set("title", "Búsqueda: '" . $search . "' | " . TITULO);
$template->set("description", "Búsqueda de " . TITULO);
$template->set("keywords", "Búsqueda de " . TITULO);
$template->set("body", "home");
$template->themeInit();
///Datos

if (!empty($search)) {
    $filter = array();
    $filterProduct = array();
} else {
    $filter = '';
    $filterProduct = '';
}

if ($search != '') {
    $titleWithSpaces = strpos($search, " ");
    if ($titleWithSpaces) {
        $filterTitle = array();
        $filterTitle2 = array();
        $titleExplode = explode(" ", $search);
        foreach ($titleExplode as $titulo_) {
            array_push($filterTitle, "(titulo LIKE '%$titulo_%'  || desarrollo LIKE '%$titulo_%')");
            array_push($filterTitle2, "(titulo LIKE '%$titulo_%'  || desarrollo LIKE '%$titulo_%'|| cod_producto LIKE '%$titulo_%')");
        }
        $filterTitleImplode = implode(" OR ", $filterTitle);
        $filterTitleImplode2 = implode(" OR ", $filterTitle2);
        array_push($filter, "(" . $filterTitleImplode . ")");
        array_push($filterProduct, "(" . $filterTitleImplode2 . ")");
    } else {
        array_push($filter, "(titulo LIKE '%$search%' || desarrollo LIKE '%$search%')");
        array_push($filterProduct, "(titulo LIKE '%$search%' || desarrollo LIKE '%$search%'|| cod_producto LIKE '%$search%')");
    }
}
$productsData = $producto->list($filterProduct, '', '');
$blogData = $novedad->list($filter, '', '');
//
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>BÚSQUEDA</h2>
                        <p>Usted está buscando: "<?= $search ?>"</p>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">BÚSQUEDA</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- Services
============================================= -->
<section class="single-service pt-15">
    <div class="container">
        <div class="row">
            <!-- .sidebar end -->
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="service-title">
                            <h3>PRODUCTOS</h3>
                        </div>
                        <div class="service-content">
                            <ul class="list-unstyled">
                                <?php
                                if (!empty($productsData)) {
                                    foreach ($productsData as $product) {
                                        ?>
                                        <li>
                                            <a href="<?= URL . '/producto/' . $funciones->normalizar_link($product['data']['titulo']) . '/' . $funciones->normalizar_link($product['data']['cod']) ?>">
                                                <?= $product['data']['titulo'] ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h5> No se encontró ningún producto. </h5>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="service-title">
                            <h3>BLOG</h3>
                        </div>
                        <div class="service-content">
                            <ul class="list-unstyled">
                                <?php
                                if (!empty($blogData)) {
                                    foreach ($blogData as $blog) {
                                        ?>
                                        <li>
                                            <a href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                                <?= $blog['data']['titulo'] ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h5> No se encontró ningún blog. </h5>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- .row end -->
            </div>
            <!-- .col-md-9 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
