<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
//
$template = new Clases\TemplateSite();
$template->set("title", "Inicio | " . TITULO);
$template->set("description", "Página principal de " . TITULO);
$template->set("keywords", "");
$template->themeInit();
$slider = new Clases\Sliders();
$sliderDataW = $slider->list(array("categoria='b853a5d7e5'"), '', '');
$sliderDataM = $slider->list(array("categoria='564a819498'"), '', '');
$funciones = new Clases\PublicFunction();
$blog = new Clases\Novedades();
$blogData = $blog->list('', '', 3);
$producto = new Clases\Productos();
$productsData = $producto->list('', '', '');
$contenido = new Clases\Contenidos();
$contenido->set("cod", "5c385bb162");
$contentData = $contenido->view();
$galeria = new Clases\Galerias();
$galeria->set("cod", "b41dc5265d");
$galleryData = $galeria->view();

?>
<section id="hero-2" class="hero-2 hidden-xs hidden-sm">
    <div id="hero-slider" class="hero-slider">
        <?php
        foreach ($sliderDataW as $sliW) {
            ?>
            <!-- Slide Item #1 -->
            <div class="item">
                <div class="item-bg bg-overlay">
                    <div class="bg-section">
                        <img src="<?= $sliW['image']['ruta'] ?>" alt="Background"/>
                    </div>
                </div>
                <?php
                if (!empty($sliW['data']['titulo'])) {
                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="hero-slide">
                                    <div class="slide-heading">
                                        <p><?= $sliW['data']['subtitulo'] ?></p>
                                    </div>
                                    <div class="slide-title">
                                        <h2><?= $sliW['data']['titulo'] ?></h2>
                                    </div>
                                </div>
                            </div>
                            <!-- .col-md-12 end -->
                        </div>
                        <!-- .row end -->
                    </div>
                    <!-- .container end -->
                    <?php
                }
                ?>
            </div>
            <!-- .item end -->
            <?php
        }
        ?>
    </div>
    <!-- #hero-slider end -->
</section>

<div class="producto-img product-feature-img hidden-md hidden-lg">
    <div id="project-carousel" class="project-carousel mb-30">
        <?php
        foreach ($sliderDataM as $sli) {
            ?>
            <div class="item">
                <div style="height:400px;background:url(<?= URL . '/' . $sli['image']['ruta'] ?>) no-repeat center center/contain;">
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <!-- .project-carousel end -->
</div>
<!-- Service Block #1
============================================= -->
<section id="service-1" class="service service-1 pt-50 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
                <div class="heading heading-2 text-center">
                    <div class="heading-bg">
                        <p></p>
                        <h2>BLOG</h2>
                    </div>
                </div>
                <!-- .heading end -->
            </div>
            <!-- .col-md-6 end -->
        </div>
        <!-- .row end -->
        <div class="row">
            <?php
            foreach ($blogData as $blog) {
                ?>
                <!-- Service Block #1 -->
                <div class="col-xs-12 col-sm-4 col-md-4 service-block">
                    <a href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                        <div class="service-img">
                            <div style="height:300px;background:url(<?= $blog['images']['0']['ruta']; ?>) no-repeat center center/cover;">
                            </div>
                        </div>
                    </a>
                    <div class="service-content">
                        <div class="service-desc p-left">
                            <h4><?= $blog['data']['titulo'] ?></h4>
                            <a class="read-more"
                               href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                <i class="fa fa-plus"></i>
                                <span>VER MÁS</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- .col-md-4 end -->
                <?php
            }
            ?>
        </div>
        <!-- .row end -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-secondary mt-50" href="<?= URL ?>/blog">VER BLOG <i class="fa fa-plus ml-xs"></i></a>
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- Projects Section
============================================= -->
<section id="projects" class="projects-fullwidth projects-4col bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                <div class="heading heading-3 text-center mb-0">
                    <div class="heading-bg">
                        <p class="mb-0"></p>
                        <h2>NUESTROS PRODUCTOS</h2>
                    </div>
                </div>
                <!-- .heading end -->
            </div>
            <!-- .col-md-8 end -->
        </div>
    </div>
    <!-- .container end -->
    <div class="container">
        <div class="row">
            <!-- Projects Filter
            ============================================= -->
            <div class="col-xs-12 col-sm-12 col-md-12 projects-filter">
            </div>
            <!-- .projects-filter end -->
        </div>
        <!-- .row end -->

        <!-- Projects Item
        ============================================= -->
        <div id="projects-all" class="row">
            <?php
            foreach ($productsData as $product) {
                $link = URL . '/producto/' . $funciones->normalizar_link($product['data']["titulo"]) . '/' . $product['data']['cod'];
                ?>
                <!-- Project Item #1 -->
                <div class="col-xs-12 col-sm-6 col-md-3 project-item">
                    <div class="project-img" onclick="document.location.href='<?= $link ?>'">
                        <div style="height:300px;background:url(<?= URL . '/' . $product['images'][0]['ruta'] ?>) no-repeat center center/cover;">
                        </div>
                        <div class="project-hover">
                            <div class="project-meta">
                                <h6><?= $product['category']['data']['titulo'] ?></h6>
                                <h4>
                                    <a href="<?= URL . '/producto/' . $funciones->normalizar_link($product['data']['titulo']) . '/' . $funciones->normalizar_link($product['data']['cod']) ?>">
                                        <?= $product['data']['titulo'] ?>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <!-- .project-hover end -->
                    </div>
                    <!-- .project-img end -->
                </div>
                <!-- .project-item end -->
                <?php
            }
            ?>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- Call To Action #3
============================================= -->
<section id="cta-3" class="cta cta-3 bg-overlay bg-overlay-theme text-center">
    <div class="bg-section">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                <h2><?= $contentData['data']['titulo'] ?></h2>
                <p><?= $contentData['data']['contenido'] ?></p>
            </div>
            <!-- .col-md-8 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<!-- #cta-3 end -->
<!-- Shortcode #9
============================================= -->
<!--<section id="clients" class="shortcode-9">
    <div class="container">-->
       <!-- <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="heading heading-2 text-center">
                    <div class="heading-bg">
                        <p class="mb-0"></p>
                        <h2>NUESTRAS MARCAS</h2>
                    </div>
                </div>

            </div>
        </div>-->
        <!-- .row end -->
       <!-- <div class="row">
             /*php
            foreach ($galleryData['images'] as $galery) {
                ?>

                <div class="col-xs-12 col-sm-4 col-md-4 mt-15">
                    <div class="">
                        <div style="height:85px;background:url(<?= URL . '/' . $galery['ruta'] ?>) no-repeat center center/contain;">
                        </div>
                    </div>
                </div>


            }
           */
        </div>

    </div>

</section> -->
<!-- #clients end-->


<?php
$template->themeEnd();
?>
