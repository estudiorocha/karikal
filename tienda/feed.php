<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$id = $funciones->antihack_mysqli(isset($_GET["id"]) ? $_GET["id"] : '');
$contenido = new Clases\Contenidos();
$contenido->set("cod", $id);
$contenido_data = $contenido->view();
$template->set("title", "Redes sociales | " . TITULO);
$template->set("description", "Redes sociales de " . TITULO);
$template->set("keywords", "");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>SEGUINOS EN NUESTRAS REDES</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">REDES</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<section class="shortcode-6 text-center-xs pt-15 pb-15">
    <div class="container">
        <div class="row">
            <!-- Place <div> tag where you want the feed to appear -->
            <div id="curator-feed"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a></div>
            <!-- The Javascript can be moved to the end of the html page before the </body> tag -->
            <script type="text/javascript">
                /* curator-feed */
                (function(){
                    var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
                    i.src = "https://cdn.curator.io/published/b7d788bf-fffa-4212-b76d-a72decb119d5.js";
                    e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
                })();
            </script>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
