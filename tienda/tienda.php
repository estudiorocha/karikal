<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$producto = new Clases\Productos();
//
$productsData = $producto->list('', '', '');
//
$template->set("title", "Tienda | " . TITULO);
$template->set("description", "Tienda online de " . TITULO);
$template->set("keywords", "");
$template->set("body", "");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>TIENDA ONLINE</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">TIENDA ONLINE</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>

<!-- Projects Section
============================================= -->
<section id="projects" class="projects-grid projects-4col">
    <div class="container">
        <!-- Projects Item
        ============================================= -->
        <div id="projects-all" class="row">
            <?php
            foreach ($productsData as $product) {
                $link = URL . '/producto/' . $funciones->normalizar_link($product['data']["titulo"]) . '/' . $product['data']['cod'];
                ?>
                <!-- Project Item #1 -->
                <div class="col-xs-12 col-sm-6 col-md-3 project-item">
                    <div class="project-img mb-10">
                        <div class="tienda-ti">
                            <h6><?= $product['category']['data']['titulo'] ?></h6>
                            <h4>
                                <a href="<?= URL . '/producto/' . $funciones->normalizar_link($product['data']['titulo']) . '/' . $funciones->normalizar_link($product['data']['cod']) ?>">
                                    <?= $product['data']['titulo'] ?>
                                </a>
                            </h4>
                        </div>
                        <a href="<?= URL . '/producto/' . $funciones->normalizar_link($product['data']['titulo']) . '/' . $funciones->normalizar_link($product['data']['cod']) ?>">
                            <div style="height:300px;background:url(<?= URL . '/' . $product['images'][0]['ruta'] ?>) no-repeat center center/cover;">
                            </div>
                        </a>
                        <div class="mt-5 mb-10">
                            <div class="tienda-desc">
                                <label>PRECIO: $<?= $product['data']['precio'] ?> </label>
                                <br>
                                <label>CONTADO: $<?= $product['data']['precio'] * 0.90 ?></label>
                            </div>
                            <div>
                                <button class="btn btn-sm btn-primary pr-10 pl-10 mt-5 tienda-button"
                                        onclick="document.location.href='<?= $link ?>'"><i class="fa fa-shopping-cart"></i> AGREGAR AL CARRITO
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- .project-img end -->
                </div>
                <!-- .project-item end -->
                <?php
            }
            ?>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
