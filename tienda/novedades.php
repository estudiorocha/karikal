<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
//
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$novedad = new Clases\Novedades();
$categoria = new Clases\Categorias();
//
$template->set("title", "Blog | " . TITULO);
$template->set("description", "Blog de " . TITULO);
$template->set("keywords", "Blog de " . TITULO);
$template->set("body", "home");
$template->themeInit();
//Gets
$page = $funciones->antihack_mysqli(isset($_GET["pagina"]) ? $_GET["pagina"] : '0');
$categoryGet = $funciones->antihack_mysqli(isset($_GET["categoria"]) ? $_GET["categoria"] : '');
$search = $funciones->antihack_mysqli(isset($_GET["titulo"]) ? $_GET["titulo"] : '');
///Datos
//Páginas
$amount = 4;

if ($page > 0) {
    $page = $page - 1;
}

if ($_GET) {
    if (@count($_GET) > 1) {
        if (isset($_GET["pagina"])) {
            $anidador = "&";
        }
    } else {
        if (isset($_GET["pagina"])) {
            $anidador = "?";
        } else {
            $anidador = "&";
        }
    }
} else {
    $anidador = "?";
}

if (isset($_GET['pagina'])):
    $url = $funciones->eliminar_get(CANONICAL, 'pagina');
else:
    $url = CANONICAL;
endif;
//
if (!empty($categoryGet) || !empty($search)) {
    $filter = array();
} else {
    $filter = '';
}
if (!empty($categoryGet)) {
    $categoria->set("cod", $categoryGet);
    $categoryFilter = $categoria->view();
    if (!empty($categoryFilter['data'])) {
        $cod = $categoryFilter['data']['cod'];
        array_push($filter, "categoria='$cod'");
    }
}
if ($search != '') {
    $titleWithSpaces = strpos($search, " ");
    if ($titleWithSpaces) {
        $filterTitle = array();
        $titleExplode = explode(" ", $search);
        foreach ($titleExplode as $titulo_) {
            array_push($filterTitle, "(titulo LIKE '%$titulo_%'  || desarrollo LIKE '%$titulo_%')");
        }
        $filterTitleImplode = implode(" OR ", $filterTitle);
        array_push($filter, "(" . $filterTitleImplode . ")");
    } else {
        array_push($filter, "(titulo LIKE '%$search%' || desarrollo LIKE '%$search%')");
    }
}
//
$blogData = $novedad->list($filter, "", $amount * $page . ',' . $amount);
if (!empty($blogData)) {
    $totalPages = $novedad->paginador($filter, $amount);
}
$latestBlogData = $novedad->list('', '', 4);
$categoriesData = $categoria->list(array("area='novedades'"), '', '');
//
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>BLOG</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">BLOG</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>

<!-- Blogs
============================================= -->
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="articles col-xs-12 col-sm-12 col-md-8 mb-30-xs">
                <div class="row">
                    <?php
                    if (!empty($blogData)) {
                        foreach ($blogData as $blog) {
                            $fecha_ = explode('-', $blog['data']['fecha']);
                            switch ($fecha_[1]) {
                                case 1:
                                    $mes = 'ENE';
                                    break;
                                case 2:
                                    $mes = 'FEB';
                                    break;
                                case 3:
                                    $mes = 'MAR';
                                    break;
                                case 4:
                                    $mes = 'ABR';
                                    break;
                                case 5:
                                    $mes = 'MAY';
                                    break;
                                case 6:
                                    $mes = 'HUN';
                                    break;
                                case 7:
                                    $mes = 'JUL';
                                    break;
                                case 8:
                                    $mes = 'AGO';
                                    break;
                                case 9:
                                    $mes = 'SEP';
                                    break;
                                case 10:
                                    $mes = 'OCT';
                                    break;
                                case 11:
                                    $mes = 'NOV';
                                    break;
                                case 12:
                                    $mes = 'DIC';
                                    break;
                                default:
                                    $mes = '';
                                    break;
                            }
                            ?>
                            <!-- Entry Article #1 -->
                            <div class="col-xs-12 col-sm-12 col-md-12 entry">
                                <div class="entry-img">
                                    <a href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                        <div style="height:350px;background:url(<?= $blog['images']['0']['ruta']; ?>) no-repeat center center/contain;">
                                        </div>
                                    </a>
                                </div>
                                <!-- .entry-img end -->
                                <div class="entry-meta clearfix">
                                    <ul class="pull-left">
                                        <li class="entry-date">
                                            <span><?= $mes ?></span>
                                            <?= $fecha_[2] ?> </li>
                                    </ul>
                                </div>
                                <!-- .entry-meta end -->
                                <div class="entry-title">
                                    <h3>
                                        <a href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                            <?= $blog['data']['titulo'] ?>
                                        </a>
                                    </h3>
                                </div>
                                <!-- .entry-title end -->
                                <div class="entry-content">
                                    <p><?= ucfirst(substr(strip_tags($blog['data']['desarrollo']), 0, 350)) ?></p>
                                    <a class="entry-more" href="<?= URL . '/blog/' . $funciones->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                        <i class="fa fa-plus"></i>
                                        <span>VER MÁS</span>
                                    </a>
                                </div>
                                <!-- .entry-content end -->
                            </div>
                            <!-- .entry end -->
                            <?php
                        }
                    } else {
                        ?>
                        <div class="grid-item col-md-12">
                            <div class="advs-box niche-box-blog">
                                <div class="block-top">
                                    <div class="block-title centro">
                                        <h2>
                                            No se encontró ningún blog.
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <hr class="space m"/>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <!-- .row end -->
                <div class="row">
                    <?php
                    if (!empty($totalPages)) {
                        ?>
                        <div class="list-nav">
                            <ul class="pagination pagination-grid hide-first-last">
                                <?php
                                if (!empty($totalPages)) {
                                    if ($totalPages != 1 && $totalPages != 0) {
                                        $url_final = $funciones->eliminar_get(CANONICAL, "pagina");
                                        $links = '';
                                        $links .= "<li class='page'><a href='" . $url_final . $anidador . "pagina=1'>1</a></li>";
                                        $i = max(2, $page - 5);

                                        if ($i > 2) {
                                            $links .= "<li class='page'><a href='#'>...</a></li>";
                                        }
                                        for (; $i <= min($page + 6, $totalPages); $i++) {
                                            if ($page + 1 == $i) {
                                                $current = "active";
                                            } else {
                                                $current = "";
                                            }
                                            $links .= "<li class='page $current'><a href='" . $url_final . $anidador . "pagina=" . $i . "'>" . $i . "</a></li>";
                                        }
                                        if ($i - 1 != $totalPages) {
                                            $links .= "<li class='page'><a href='#'>...</a></li>";
                                            $links .= "<li class='page'><a href='" . $url_final . $anidador . "pagina=" . $totalPages . "'>" . $totalPages . "</a></li>";
                                        }
                                        echo $links;
                                        echo "";
                                    }
                                }
                                ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!-- entry articles end -->

            <?php
            include("assets/inc/novedades/side.inc.php");
            ?>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
