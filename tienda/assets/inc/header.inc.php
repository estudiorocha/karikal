
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php
$configH = new Clases\Config();
$marketingH = $configH->viewMarketing();
?>
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function () {
        OneSignal.init({
            appId: "<?= $marketingH["data"]["onesignal"] ?>",
        });
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?= $marketingH["data"]["google_analytics"] ?>"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', '<?= $marketingH["data"]["google_analytics"] ?>');
</script>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4368046.js"></script>
<!-- End of HubSpot Embed Code -->

<script type='text/javascript' data-cfasync='false'>window.purechatApi = {
        l: [], t: [], on: function () {
            this.l.push(arguments);
        }
    };
    (function () {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function (e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({c: '4a5e8e9b-12bd-4d0d-8a0b-420d07cd68ad', f: true});
                done = true;
            }
        };
    })();</script>
<!-- Begin Inspectlet Asynchronous Code -->
<script type="text/javascript">
    (function () {
        window.__insp = window.__insp || [];
        __insp.push(['wid', 1112248883]);
        var ldinsp = function () {
            if (typeof window.__inspld != "undefined") return;
            window.__inspld = 1;
            var insp = document.createElement('script');
            insp.type = 'text/javascript';
            insp.async = true;
            insp.id = "inspsync";
            insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js?wid=1112248883&r=' + Math.floor(new Date().getTime() / 3600000);
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(insp, x);
        };
        setTimeout(ldinsp, 0);
    })();
</script>
<!-- End Inspectlet Asynchronous Code -->

<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2011774989119878');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=2011774989119878&ev=PageView
&noscript=1"/>
</noscript>


<link rel="icon" href="<?php echo URLSITE ?>/images/logo.png">
<!-- Bootstrap -->
<link href="<?= URLSITE ?>/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Simple-Line-Icons-Webfont -->
<link href="<?= URLSITE ?>/fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" rel="stylesheet">
<!-- FlexSlider -->
<link href="<?= URLSITE ?>/scripts/FlexSlider/flexslider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="<?= URLSITE ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?= URLSITE ?>/css/owl.theme.default.css" rel="stylesheet">
<!-- Nivo Lightbox -->
<link href="<?= URLSITE ?>/scripts/Nivo-Lightbox/nivo-lightbox.css" rel="stylesheet">
<link href="<?= URLSITE ?>/scripts/Nivo-Lightbox/themes/default/default.css" rel="stylesheet">
<!-- Style.css -->
<link href="<?= URLSITE ?>/lightbox/lightbox.css" rel="stylesheet">
<link href="<?= URLSITE ?>/css/style.css" rel="stylesheet">


<!-- End Facebook Pixel Code -->
<!-- Stylesheets
============================================= -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="<?=URL?>/assets/js/jquery-2.1.1.min.js"></script>
<link href="<?= URL ?>/assets/css/external.css" rel="stylesheet">
<!-- Extrnal CSS -->
<link href="<?= URL ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<!-- Boostrap Core CSS -->
<link href="<?= URL ?>/assets/css/style.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/main-rocha.css" rel="stylesheet">
<link href="<?= URL ?>/assets/css/estilos.css" rel="stylesheet">
<!-- Style CSS -->
