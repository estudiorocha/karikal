<?php
//Clases
$configNav = new Clases\Config();
$categoriaNav = new Clases\Categorias();
$funcionesNav = new Clases\PublicFunction();
$usuarioNav = new Clases\Usuarios();
$carritoNav = new Clases\Carrito();
//$imagenNav = new Clases\Imagenes();
//Datos
$contactDataNav = $configNav->viewContact();
$socialDataNav = $configNav->viewSocial();
$categoriesDataNav = $categoriaNav->list(array("area='productos'"), 'id ASC', '');
$userDataNav = $usuarioNav->viewSession();
if (isset($_GET['logout'])) {
    $usuarioNav->logout();
}
$cartDataNav = $carritoNav->return();


?>
<?php
/*
?>

<div class="preloader">
    <div class="spinner-j">
        <img src="<?= LOGO ?>">
    </div>
    <div class="spinner">
        <div class="bounce1">
        </div>
        <div class="bounce2">
        </div>
        <div class="bounce3">
        </div>
    </div>
</div>
<header id="navbar-spy" class="full-header">
    <div id="top-bar" class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
                    <span> <i class="fa fa-phone"></i> <?= $contactDataNav['data']['telefono'] ?> |</span>
                    <span> <i class="fa fa-envelope"></i> <?= $contactDataNav['data']['email'] ?></span>
                </div>
                <!-- .col-md-6 end -->
                <div class="col-xs-12 col-sm-6 col-md-6 text-right">
                    <ul class="list-inline top-widget">
                        <li class="top-social">
                            <?php
                            if (!empty($socialDataNav['data']['facebook'])) {
                                ?>
                                <a target="_blank" href="<?= $socialDataNav['data']['facebook'] ?>">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <?php
                            }
                            if (!empty($socialDataNav['data']['twitter'])) {
                                ?>
                                <a target="_blank" href="<?= $socialDataNav['data']['twitter'] ?>">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <?php
                            }
                            if (!empty($socialDataNav['data']['instagram'])) {
                                ?>
                                <a target="_blank" href="<?= $socialDataNav['data']['instagram'] ?>">
                                    <i class="fa fa-instagram"></i>
                                </a>
                                <?php
                            }
                            ?>
                        </li>
                        <li>
                            <?php
                            if (!empty($userDataNav)) {
                                ?>
                                <a class="button-quote" href="<?= URL ?>/sesion">MI CUENTA</a>
                                <?php
                            } else {
                                ?>
                                <a class="button-quote" href="<?= URL ?>/usuarios">INGRESAR</a>
                                <?php
                            }
                            ?>
                        </li>
                    </ul>
                </div>
                <!-- .col-md-6 end -->
            </div>
        </div>
    </div>
    <nav id="primary-menu" class="navbar navbar-fixed-top style-1 nav-zindex">
        <div class="row">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="logo" href="<?=URL?>/index">
                        <img src="<?= LOGO ?>" height="70" alt="Yellow Hats">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="<?php if (strpos(CANONICAL, "index") == true || URL . '/' == CANONICAL) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/index"> <i class="fa fa-home"></i></a>
                        </li>
                        <!-- li end -->
                        <li class="<?php if (strpos(CANONICAL, "MATERIALES MUEBLES") == true) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/tienda">MATERIALES MUEBLES</a>
                        </li>
                        <li class="<?php if (strpos(CANONICAL, "MATERIALES CONSTRUCCION") == true) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/tienda">MATERIALES CONSTRUCCION</a>
                        </li>
                        <li class="<?php if (strpos(CANONICAL, "tienda") == true) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/tienda">TIENDA ONLINE</a>
                        </li>
                        <li class="<?php if (strpos(CANONICAL, "/blog") == true) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/blog">
                                BLOG
                            </a>
                        </li>

                        <li class="<?php if (strpos(CANONICAL, "/contacto") == true) {
                            echo "active";
                        } ?>">
                            <a href="<?= URL ?>/contacto">
                                <i class="fa fa-envelope-o"></i>
                            </a>
                        </li>
                        <li class="visible-xs visible-sm">
                            <?php
                            if (!empty($userDataNav)) {
                                ?>
                                <a class="button-quote" href="<?= URL ?>/sesion">MI CUENTA</a>
                                <?php
                            } else {
                                ?>
                                <a class="button-quote" href="<?= URL ?>/usuarios">INGRESAR</a>
                                <?php
                            }
                            ?>
                        </li>
                    </ul>

                    <!-- Mod-->
                        <div class="module module-search pull-left">
                            <div class="search-icon">
                                <i class="fa fa-search"></i>
                                <span class="title">Buscar</span>
                            </div>
                            <div class="search-box">
                                <form class="search-form" method="get" action="<?= URL ?>/busqueda">
                                    <div class="input-group">
                                        <input type="text" name="titulo" class="form-control" placeholder=".....">
                                        <span class="input-group-btn">
									<button class="btn" type="submit"><i class="fa fa-search"></i></button>
									</span>
                                    </div>
                                    <!-- /input-group -->
                                </form>
                            </div>
                        </div>
                    <!-- .module-search-->
                    <!-- .module-cart -->
                    <div class="module module-cart pull-left">
                        <?php
                        if (!empty($cartDataNav)) {
                            ?>
                            <a href="<?=URL?>/carrito">
                            <div class="cart-icon">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="title">CARRITO</span>
                                <span class="cart-label"><?= @count($cartDataNav) ?></span>
                            </div>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <!-- .module-cart end -->
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </nav>
</header>
<?php
*/
?>


<header class="header">
    <div class="top">
        <div class="container">
            <div class=" hidden-md hidden-lg">
                <br/>
            </div>
            <div class=" hidden-xs hidden-sm">
                <span class="item"><i class="icon-clock"></i>Lunes - Viernes : 9:00 - 18:00</span>
                <span class="item"><a href="mailto:karikal@karikal.com.ar"><i class="icon-envelope"></i>karikal@karikal.com.ar</a></span>
                <span class="item"><a href="tel:(0351) 5542470"><i class="icon-call-out"></i>  (0351) 5542470 (rot.) </a></span>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="container clearfix">
            <div class="logo"><a href="<?= URLSITE ?>/index.php"><img src="<?= URLSITE ?>/images/logo.png" alt="Architech" class="img-responsive"></a></div>
            <nav class="main-nav">
                <ul class="list-unstyled">
                    <li><a href="<?= URLSITE ?>/index.php"><i class="fa fa-home"></i></a></li>
                    <li><a href="<?= URLSITE ?>/lineas.php">Materiales Construcción</a></li>
                    <li><a href="<?= URLSITE ?>/lineas.php">Materiales Muebles</a></li>
                    <li><a href="<?= URLSITE ?>/empresa.php">NOSOTROS</a></li>
                    <li><a href="<?= URLSITE ?>/servicios.php">servicios</a></li>
                    <li>
                        <a href="<?= URLSITE ?>/novedades.php">Novedades</a>
                    </li>
                    <li>
                        <a href="<?php echo URLSITE ?>/tienda/tienda">COMPRAR ONLINE</a>
                    </li>
                    <li><a href="<?= URLSITE ?>/contacto.php"><i class="fa fa-envelope-o"></i></a></li>
                </ul>
            </nav>
            <a href="<?= URLSITE ?>/" class="responsive-menu-open"><i class="fa fa-bars"></i></a>
        </div>
    </div>
</header>
<div class="responsive-menu">
    <a href="<?= URLSITE ?>/" class="responsive-menu-close"><i class="icon-close"></i></a>
    <nav class="responsive-nav"></nav>
</div>
