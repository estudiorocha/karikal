<?php
$contenidoF = new Clases\Contenidos();
$configF = new Clases\Config();
$socialDataF = $configF->viewSocial();
$contenidoF->set("cod","3add3b8400");
$linksF = $contenidoF->view();
$contenidoF->set("cod","8ce9cf98cb");
$underF = $contenidoF->view()
?>
<footer id="footer" class="footer-1">
    <!-- Widget Section
    ============================================= -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 widgets-links pt-50 pb-50">
                <div class="col-xs-12 col-sm-12 col-md-4 widget-about text-center-xs mb-30-xs">
                    <div class="pull-none-xs">
                        <img src="<?=LOGO?>" height="120" alt="logo"/>
                    </div>
                    <div class="">
                        <p>
                            <?=$underF['data']['contenido']?>
                        </p>
                        <!--<h5 class="text-capitalize text-white">yellow hats</h5>
                        <p class="mb-0">Yellow Hats is a leading of A-grade commercial, industrial and residential projects in USA. Since its foundation the company has doubled its turnover year on year, with its staff numbers swelling accordingly.</p>
                        -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 widget-navigation mb-30-xs">
                    <?=$linksF['data']['contenido']?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 widget-services text-center-xs">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fkarikaloficial%2F&tabs&width=340&height=181&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="181" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

                </div>
            </div>
        </div>
    </div>
    <!-- Social bar
    ============================================= -->
    <div class="widget-social">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mb-30-xs mb-30-sm">
                    <div class="widget-social-icon text-right pull-none-xs">
                        <?php
                        if (!empty($socialDataF['data']['facebook'])) {
                            ?>
                            <a target="_blank" href="<?= $socialDataF['data']['facebook'] ?>">
                                <i class="fa fa-facebook"></i><i class="fa fa-facebook"></i
                            </a>
                            <?php
                        }
                        if (!empty($socialDataF['data']['twitter'])) {
                            ?>
                            <a target="_blank" href="<?= $socialDataF['data']['twitter'] ?>">
                                <i class="fa fa-twitter"></i><i class="fa fa-twitter"></i>
                            </a>
                            <?php
                        }
                        if (!empty($socialDataF['data']['instagram'])) {
                            ?>
                            <a target="_blank" href="<?= $socialDataF['data']['instagram'] ?>">
                                <i class="fa fa-instagram"></i><i class="fa fa-instagram"></i>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 copyrights text-center pt-0 pb-0">
                <p class="text-capitalize">© 2019 <?=TITULO?>. todos los derechos reservados by111
                    <a href="https://www.estudiorochayasoc.com/" target="_blank" >Estudio Rocha & Asociados</a>
                </p>
            </div>
        </div>
    </div>
</footer>


 

<!-- Footer Scripts
============================================= -->
<script src="<?=URL?>/assets/js/jquery-2.1.1.min.js"></script>
<script src="<?=URL?>/assets/js/plugins.js?v=1.0.0"></script>
<script src="<?=URL?>/assets/js/functions.js?v=1.2.0"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.your-class').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
    });
</script>
<script>
    $("#area2").keyup(function(){
        var cajas = Math.ceil($("#area2").val() / 2);
        $("#cantidad2").val(cajas);
    });

    function m2pisoflotante() {
        var cajas2 = Math.ceil($("#area").val() / 2);
        var precio = "$" + (($("#precio").val()*2) * cajas2);
        $("#cantidad").val(cajas2);
        $("#cantidadFinal").val(cajas2);
        $("#quantity").val(cajas2);
        $("#precioFinal").val(precio);
    };
</script>

<script>
    function mlineal(){
        m1 = document.getElementById("precioml").value;
        m2 = Math.ceil(document.getElementById("metrosml").value / 3.04);
        precio = m1*3;
        r = precio*m2;
        document.getElementById("resultado").value = r;
        $('#quantity').val(m2);
    }
</script>

<script>
    function zocalo(){
        m1 = document.getElementById("precio3").value;
        m2 = document.getElementById("area3").value;
        r = m1*m2;
        document.getElementById("resultado3").value = r;
    }

</script>
<script>
    $("#provincia").change(function () {
        $("#provincia option:selected").each(function () {
            elegido = $(this).val();
            $.ajax({
                type: "GET",
                url: "<?=URL ?>/assets/inc/localidades.inc.php",
                data: "elegido=" + elegido,
                dataType: "html",
                success: function (data) {
                    $('#localidad option').remove();
                    var substr = data.split(';');
                    for (var i = 0; i < substr.length; i++) {
                        var value = substr[i];
                        $("#localidad").append(
                            $("<option></option>").attr("value", value).text(value)
                        );
                    }
                }
            });
        });
    })
</script>