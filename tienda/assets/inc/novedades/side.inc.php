<?php
$latestBlogData = $novedad->list('', '', 4);
$funcionesNav = new Clases\PublicFunction();
?>
<div class="col-xs-12 col-sm-12 col-md-4">
    <div class="sidebar">
        <!-- Recent Posts
============================================= -->
        <div class="widget widget-recent">
            <div class="widget-title">
                <h3>ÚLTIMOS BLOG</h3>
            </div>
            <div class="widget-content">
                <?php
                if (!empty($latestBlogData)) {
                    foreach ($latestBlogData as $blog) {
                        ?>
                        <div class="recent-entry">
                            <a href="<?= URL . '/blog/' . $funcionesNav->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                <div style="height:170px;background:url(<?= URL . '/' . $blog['images']['0']['ruta']; ?>) no-repeat center center/contain;">
                                </div>
                            </a>
                            <div class="recent-desc m-left">
                                <a href="<?= URL . '/blog/' . $funcionesNav->normalizar_link($blog['data']["titulo"]) . '/' . $blog['data']['cod'] ?>">
                                    <?= mb_strtoupper($blog['data']['titulo']) ?>
                                </a>
                            </div>
                        </div>
                        <!-- .recent-entry end -->
                        <?php
                    }
                }
                ?>
            </div>
            <!-- .widget-content end -->
        </div>
    </div>
</div>
<!-- sidebar end -->