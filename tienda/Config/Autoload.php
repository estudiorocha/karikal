<?php

namespace config;

class autoload
{
    public static function runSitio()
    {
        //require_once "Config/Minify.php";
        session_start();
        $_SESSION["cod_pedido"] = isset($_SESSION["cod_pedido"]) ? $_SESSION["cod_pedido"] : strtoupper(substr(md5(uniqid(rand())), 0, 7));
        define('SALT', hash("sha256", "salt@estudiorochayasoc.com.ar"));
        define('URL', "https://" . $_SERVER['HTTP_HOST'] . "/Karikal/tienda");
        define('URLSITE', "https://" . $_SERVER['HTTP_HOST'] . "/Karikal");
        define('CANONICAL', "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        define('EMAIL', "webestudiorocha@gmail.com");
        define('LOGO', URLSITE . "/images/logo.png");
        define('FAVICON', URL . "/assets/img/logo.png");
        define('TITULO', 'Karikal');
        spl_autoload_register(function ($clase) {
            $ruta = str_replace("\\", "/", $clase) . ".php";
            include_once $ruta;
        });
    }

    public static function runSitio2()
    {
        spl_autoload_register(function ($clase) {
            $ruta = str_replace("\\", "/", $clase) . ".php";
            include_once "../../" . $ruta;
        });
    }

    public static function runAdmin()
    {
        require_once "../Config/Minify.php";
        session_start();
        $_SESSION["cod_pedido"] = isset($_SESSION["cod_pedido"]) ? $_SESSION["cod_pedido"] : strtoupper(substr(md5(uniqid(rand())), 0, 7));
        define('TITULO_ADMIN', "ESTUDIO ROCHA CMS");
        define('URLSITE', "https://" . $_SERVER['HTTP_HOST'] . "/Karikal");
        define('URL', "https://" . $_SERVER['HTTP_HOST'] . "/Karikal/tienda/admin");
        define('CANONICAL', "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        define('SALT', hash("sha256", "salt@estudiorochayasoc.com.ar"));
        define('LOGO', URLSITE . "/images/logo.png");
        define('FAVICON', URL . "/assets/favicon.png");

        require_once "../Clases/Zebra_Image.php";
        spl_autoload_register(function ($clase) {
            $ruta = str_replace("\\", "/", $clase) . ".php";
            include_once "../" . $ruta;
        });
    }
}
