<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();

//Clases
$imagenes = new Clases\Imagenes();
$landing = new Clases\Landing();
$categorias = new Clases\Categorias();
$enviar = new Clases\Email();
$config = new Clases\Config();
//Productos
$cod = isset($_GET["cod"]) ? $_GET["cod"] : '';
$landing->set("cod", $cod);
$landing_ = $landing->view();
$imagenes->set("cod", $landing_['data']['cod']);
$imagenData = $imagenes->list(array("cod = '" . $landing_['data']['cod'] . "'"), '', '');
$landingData = $landing->list('', '', '');
$landingData = $landing->list('', '', '');
$categorias->set("cod", $landing_['data']["categoria"]);
$categoria = $categorias->view();
$i = 0;
$fecha = explode("-", $landing_['data']['fecha']);
$template->set("title", ucfirst($landing_['data']['titulo'])." | ".TITULO);
$template->set("description", $landing_['data']['description']);
$template->set("keywords", $landing_['data']['keywords']);
$template->set("favicon", FAVICON);
$template->themeInit();

switch ($categoria['data']["titulo"]) {
    case "Informacion":
        $titulo_form = "Solicitá más información";
        $boton_form = "¡Pedir más info!";
        break;
    case "Compra":
        $titulo_form = "Llená el formulario y comprá online";
        $boton_form = "¡Comprar!";
        break;
    case "Sorteo":
        $titulo_form = "Participá del sorteo";
        $boton_form = "¡Participar!";
        break;
    case "Evento":
        $titulo_form = "Inscribite al evento";
        $boton_form = "¡Inscribirme!";
        break;
    default:
        $titulo_form = "Completar el formulario";
        $boton_form = "¡Enviar mis datos!";
        break;
}
$banner = new Clases\Banner();
$bannerData = $banner->list(array("categoria='34b8ccddc2'"), 'RAND()', '1');
$emailData = $config->viewEmail();
//
?>
    <!-- Page Title #4
        ============================================= -->
    <section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
        <div class="bg-section">
            <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="page-title title-4 pt-49 pb-84">
                        <div class="pull-left">
                            <h2><?= ucfirst($landing_['data']['titulo']); ?></h2>
                        </div>
                    </div>
                    <!-- .page-title end -->
                </div>
                <!-- .col-md-12 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <!-- page-title end -->
    <div id="sns_wrapper" class="mb-10 mt-15">
        <div id="sns_content" class="wrap">
            <div class="container">
                <div class="row">
                    <div class="blogs-page col-md-8">
                        <div class="postWrapper v1">
                            <?php if (count($imagenData) >= 1) { ?>
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <?php foreach ($imagenData as $img) { ?>
                                            <div class="item <?php if ($i == 0) {
                                                echo "active";
                                            } ?>">
                                                <img src="<?= URL . '/' . $img['ruta']; ?>" alt="<?= $landing_['data']['titulo']; ?>" width="100%">
                                            </div>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if (count($imagenData) > 1) {
                                        ?>
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                            <div class="post-content fs-16">
                                <p class="fs-20">
                                    <?= $landing_['data']['desarrollo']; ?>
                                </p>
                                <div class="mt-15">
                                    <!-- AddToAny BEGIN -->
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                        <a class="a2a_button_facebook"></a>
                                        <a class="a2a_button_twitter"></a>
                                        <a class="a2a_button_google_plus"></a>
                                        <a class="a2a_button_pinterest"></a>
                                        <a class="a2a_button_whatsapp"></a>
                                        <a class="a2a_button_facebook_messenger"></a>
                                    </div>
                                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                                    <!-- AddToAny END -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blogs-page col-md-4 ">
                        <div>
                            <h3><?= $titulo_form ?></h3>
                            <hr/>
                            <?php
                            if (isset($_POST["enviar"])) {
                                $nombre = $funciones->antihack_mysqli(isset($_POST["nombre"]) ? $_POST["nombre"] : '');
                                $apellido = $funciones->antihack_mysqli(isset($_POST["apellido"]) ? $_POST["apellido"] : '');
                                $telefono = $funciones->antihack_mysqli(isset($_POST["telefono"]) ? $_POST["telefono"] : '');
                                $email = $funciones->antihack_mysqli(isset($_POST["email"]) ? $_POST["email"] : '');
                                $consulta = $funciones->antihack_mysqli(isset($_POST["mensaje"]) ? $_POST["mensaje"] : '');
                                $land = $funciones->antihack_mysqli(isset($_POST["landing"]) ? $_POST["landing"] : '');

                                $mensajeFinalAdmin = "<b>Nombre</b>: " . $nombre . " " . $apellido . " <br/>";
                                $mensajeFinalAdmin .= "<b>Teléfono</b>: " . $telefono . "<br/>";
                                $mensajeFinalAdmin .= "<b>Email</b>: " . $email . "<br/>";
                                $mensajeFinalAdmin .= "<b>Consulta</b>: " . $consulta . "<br/>";
                                //ADMIN
                                $enviar->set("asunto", "Consulta Web - " . $land);
                                $enviar->set("emisor", $emailData['data']['remitente']);
                                $enviar->set("receptor", $emailData['data']['remitente']);
                                $enviar->set("mensaje", $mensajeFinalAdmin);
                                if ($enviar->emailEnviar() == 1) {
                                    echo '<div class="alert alert-success" role="alert">¡Consulta enviada correctamente!</div>';
                                    $funciones->headerMove(URL . '/gracias');
                                } else {
                                    echo '<div class="alert alert-danger" role="alert">¡No se ha podido enviar la consulta!</div>';
                                }
                            }
                            ?>
                            <form method="post" class="row" id="Formulario de Contacto">
                                <label class="col-xs-12 col-sm-12 col-md-6">
                                    Nombre <span style="color:red">(*)</span>:<br/>
                                    <input type="text" name="nombre" class="form-control" required/>
                                </label>
                                <label class="col-xs-12 col-sm-12 col-md-6">
                                    Apellido <span style="color:red">(*)</span>:<br/>
                                    <input type="text" name="apellido" class="form-control" required/>
                                </label>
                                <label class="col-xs-12 col-sm-12 col-md-12">
                                    Landing:<br/>
                                    <input type="text" readonly name="landing" class="form-control" value="<?= mb_strtoupper($landing_['data']["titulo"]) ?>"/>
                                </label>
                                <label class="col-xs-12 col-sm-12 col-md-12">
                                    Teléfono:<br/>
                                    <input type="text" name="telefono" class="form-control"/>
                                </label>
                                <label class="col-xs-12 col-sm-12 col-md-12">
                                    Email <span style="color:red">(*)</span>:<br/>
                                    <input type="email" name="email" class="form-control" required/>
                                </label>
                                <label class="col-xs-12 col-sm-12 col-md-12">
                                    Mensaje:<br/>
                                    <textarea name="mensaje" class="form-control"></textarea>
                                </label>
                                <label class="col-xs-12 col-sm-12  col-md-12">
                                    <input type="submit" name="enviar" class="btn btn-block btn-success" value="<?= $boton_form ?>"/>
                                </label>
                            </form>
                            <hr/>
                        </div>
                        <!--
                        <div class="mt-40 text-center">
                            <h5><b>Comunicate también por:</b></h5>
                            <div>
                                <a target="_blank" href="https://wa.me/" class="btn btn-block btn-success fs-18">
                                    <i class="ifoot fa fa-whatsapp" aria-hidden="true"></i> WhatsApp
                                </a>
                                <a target="_blank" href="tel:" class="btn btn-block btn-info fs-19">
                                    <i class="ifoot fa fa-mobile" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$template->themeEnd();
?>