<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$usuario = new Clases\Usuarios();
$usuarioSesion = $usuario->viewSession();
if ($usuarioSesion['invitado'] == "1" || empty($usuarioSesion) ) {
    $funciones->headerMove(URL . "/index");
}
///Datos
//
$template->set("title", "Panel de usuario | " . TITULO);
$template->set("description", "Panel de usuario de " . TITULO);
$template->set("keywords", "");
$template->set("body", "contacts");
$template->themeInit();
?>
<!-- Page Title #4
           ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>PANEL DE USUARIO</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>/sesion">USUARIO</a>
                        </li>
                        <li class="active">PANEL</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<div class="section-empty">
    <div class="container content pt-15">
        <div class="row">
            <div class="col-md-12 pt-15">
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?= URL ?>/sesion/pedidos" class="boton-cuenta btn btn-lg btn-block mb-15">
                            <i class="fa fa-list fa-2x mt-10"></i>
                            <h4>Mis Pedidos</h4>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="<?= URL ?>/sesion/cuenta" class="boton-cuenta btn btn-lg btn-block mb-15">
                            <i class="fa fa-edit fa-2x mt-10"></i>
                            <h4>Mis datos</h4>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="<?= URL ?>/sesion?logout" class="boton-cuenta btn btn-lg btn-block mb-15">
                            <i class="fa fa-sign-out fa-2x mt-10"></i>
                            <h4>Salir</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 float-md-right">
                <div class="categories_product_area">
                    <div class="row">
                        <?php
                        $op = isset($_GET["op"]) ? $_GET["op"] : 'cuenta';
                        if ($op != '') {
                            include("assets/inc/sesion/" . $op . ".php");
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$template->themeEnd();
?>
