-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-08-2019 a las 12:23:42
-- Versión del servidor: 5.6.26-log
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jv000256_nueva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `cod` varchar(255) DEFAULT NULL,
  `cod_producto` varchar(255) DEFAULT NULL,
  `titulo` text,
  `desarrollo` text,
  `stock` int(11) DEFAULT '0',
  `precio` float DEFAULT '0',
  `precio_descuento` float DEFAULT NULL,
  `precio_mayorista` float DEFAULT '0',
  `categoria` varchar(255) DEFAULT NULL,
  `subcategoria` varchar(255) DEFAULT NULL,
  `peso` float DEFAULT '0',
  `keywords` text,
  `description` text,
  `variable1` text,
  `variable2` text,
  `variable3` text COMMENT 'cantidad incremental',
  `variable4` text COMMENT 'cantidad * metros ej: x m²',
  `variable5` text COMMENT 'medidas',
  `variable6` text COMMENT 'densidad',
  `variable7` text COMMENT 'garantía',
  `variable8` text,
  `variable9` text,
  `variable10` text,
  `fecha` date DEFAULT NULL,
  `meli` varchar(255) DEFAULT NULL,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `cod`, `cod_producto`, `titulo`, `desarrollo`, `stock`, `precio`, `precio_descuento`, `precio_mayorista`, `categoria`, `subcategoria`, `peso`, `keywords`, `description`, `variable1`, `variable2`, `variable3`, `variable4`, `variable5`, `variable6`, `variable7`, `variable8`, `variable9`, `variable10`, `fecha`, `meli`, `url`) VALUES
(4, 'f07f5b7d32', '012', 'PISO FLOTANTE PINO PONDEROSA AC4', '<p>&bull;&nbsp;Este color pertenece a la colecci&oacute;n <strong>TOP FLOOR PREMIUN, </strong>&nbsp;el <strong>&uacute;timo en salir al mercado.</strong>&nbsp;</p>\r\n\r\n<p>&bull; Representa la <strong>nobleza del pino q</strong>ue nace en Estados Unidos y Canad&aacute;, con sus tonalidades anaranjadas, y sus surcos oscuros, da una <strong>percepci&oacute;n &uacute;nica, de altura y potencia.&nbsp;</strong></p>\r\n\r\n<p>&bull; Representa un<strong> estilo r&uacute;stico</strong> que al estar acompa&ntilde;ado con mobiliarios nobles, hace del ambiente, un <strong>ambiente m&aacute;s confortable, &uacute;nico y a la moda,</strong> ya que este color<strong> es TENDENCIA.&nbsp;</strong></p>\r\n', 9000, 785, NULL, NULL, '5aa4d02c4f', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m².', '1200mm x 190mm x 9mm (espesor)', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(5, '1c83f13e86', '021', 'PISO FLOTANTE PINOTEA AC4', '<p>&bull;&nbsp;<span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Color<strong> marr&oacute;n oscuro</strong>, con<strong> vetas</strong> en las mismas tonalidades intercambi&aacute;ndose con un <strong>marr&oacute;n m&aacute;s claro.</strong></span></span></p>\r\n\r\n<p>&bull;&nbsp;<span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Si bien su <strong>tonalidad es oscura,</strong> da aspecto de s<strong>eriedad, formalidad, nobleza, concentraci&oacute;n y calidez</strong>,</span></span></p>\r\n\r\n<p>&bull;&nbsp;E<span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">s <strong>ideal para oficinas, hoteles, ambientes de trabajo,</strong> como as&iacute; tambi&eacute;n da un toque de c<strong>onfort y bienestar en salas de juegos, de m&uacute;sica y aquel espacio ideal del hogar para aseo personal. </strong></span></span></p>\r\n', 9998, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm  ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(6, '15b207bade', '055', 'PISO FLOTANTE CAOBA ASERRADO AC4', '<p>&bull; Color oscuro, donde <strong>predomina el marr&oacute;n</strong>, con<strong> vetas en tonalidades m&aacute;s oscuras, casi llegando al negro.</strong><br />\r\n&bull; Con una t<strong>extura semi- vetead</strong>a, ideal para un <strong>playroom, un living.</strong><br />\r\n&bull; Capaz de <strong>combinarse con mobiliarios r&uacute;sticos de madera y metal, </strong>con paredes en tonalidades claras, para que resalte el color caoba<strong> brindando confort y calidez </strong>en el ambiente.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 9999, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(7, '31b572badb', '48', 'PISO FLOTANTE CASTAÑO TORONTO ', '<p>&bull; Color<strong> marr&oacute;n claro</strong> mezclado con <strong>vetas en tonalidades grises y marrones</strong> m&aacute;s oscuras.</p>\r\n\r\n<p>&bull;&nbsp;Ideal para <strong>lograr un ambiente m&aacute;s luminos</strong>o.&nbsp;</p>\r\n\r\n<p>&bull;&nbsp; <strong>Dise&ntilde;ado</strong> exclusivamente para <strong>habitaciones del hogar</strong></p>\r\n\r\n<p>&bull; <strong>Capaz de ser combinado con mobiliario en tonalidades m&aacute;s fuertes y claras </strong>y una de las paredes con alg&uacute;n color salt&oacute;n, para destacar la naturaleza del piso y el ambiente.</p>\r\n', 9999, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(8, 'b334b03710', '067', 'PISO FLOTANTE GUATAMBU AC4', '<p>&bull;&nbsp;<span style=\"font-size:10.5pt\"><span style=\"background-color:white\"><span style=\"color:#1d2129\">Color <strong>marr&oacute;n oscuro</strong> con <strong>vetas suaves y alargadas</strong> formando &quot;aureolas&quot; con un contorno m&aacute;s oscuro dentro de la tonalidad.&nbsp;</span></span></span></p>\r\n\r\n<p><br />\r\n&bull;&nbsp;<span style=\"font-size:10.5pt\"><span style=\"background-color:white\"><span style=\"color:#1d2129\">Capaz de ser combinado con muebles en colores oscuros y color metal resaltando la materialidad de los mismos, logrando un<strong> confort &uacute;nico</strong>.&nbsp;</span></span></span></p>\r\n\r\n<p><br />\r\n&bull;&nbsp;<span style=\"font-size:10.5pt\"><span style=\"background-color:white\"><span style=\"color:#1d2129\">Este color suele usarse para <strong>locales comerciales </strong>de indumentaria,<strong> restaurantes, hall de entrada, sala de espera, dormitorios,</strong> entre otros</span></span></span></p>\r\n', 9974, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(9, '952b29ca31', '005', 'PISO FLOTANTE LAPACHO AC4', '<p>&bull; Es un <strong>color gris&aacute;ceo</strong>, con vetas en tonalidades grises m&aacute;s oscuras.<br />\r\n&bull; Es uno de los colores que m&aacute;s eligen nuestros clientes, ya que<strong> es agradable y presenta armon&iacute;a en el ambiente </strong>junto con el resto del mobiliario y paredes.<br />\r\n&bull; Es un <strong>color de TENDENCIA,</strong> &nbsp;queda bien tanto en l<strong>ocales comerciales, hoteles, salas de espera, casinos, hospitales, jardines infantes, como en &nbsp;dormitorios, hall de entrada, living, comedor, entre otros espacios del hogar.&nbsp;</strong><br />\r\n&bull; Con este color, se percibe un <strong>lugar c&aacute;lido, de confort, natural, que inspira dise&ntilde;o, innovaci&oacute;n, tendencia, y sentido de permanencia.&nbsp;</strong></p>\r\n', 9980, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(10, '058d21311c', '013', 'PISO FLOTANTE NOGAL AC4', '<p>&bull; C<strong>olor marr&oacute;n tirando a un rojizo</strong>, con vetas en tonalidades m&aacute;s oscuras llegando casi al negro.&nbsp;</p>\r\n\r\n<p><br />\r\n&bull; Es un color agradable, que<strong> presenta armon&iacute;a</strong></p>\r\n\r\n<p>&bull; Los clientes <strong>lo eligen para dormitorios matrimoniales</strong>, pero queda muy bien tambi&eacute;n en resto-bares, &nbsp;bibliotecas, librer&iacute;as, pasillos de hoteles, entre otros. Ya que, es <strong>recomendable optar</strong> <strong>por colores oscuros para ambientes m&aacute;s luminosos y amplios</strong>.&nbsp;<br />\r\n&bull; Es un color que si bien es algo oscuro, levanta el ambiente gracias a la buena combinaci&oacute;n de los muebles. (Preferentemente de madera vista o gastada y color metal).&nbsp;</p>\r\n\r\n<p>&bull; <strong>Representa&nbsp; lujo, estilo, y elegancia.&nbsp;</strong></p>\r\n', 9957, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(11, '1b85802a2e', '022', 'PISO FLOTANTE ROBLE MISIONERO AC4', '<p>&bull; Es un<strong> color marr&oacute;n </strong>con vetas tirando a un rojizo, intercambiandose en colores mas fuertes y mas claros por lo que &nbsp;es <strong>f&aacute;cil de combinar</strong>.</p>\r\n\r\n<p><br />\r\n&bull;&nbsp;El roble genera un entorno tranquilo y sosegado</p>\r\n\r\n<p>&bull;&nbsp;Representa el <strong>sol, la diversi&oacute;n y el optimismo</strong>.</p>\r\n\r\n<p>&bull; Es ideal para un&nbsp;<strong>dormitorio juvenil o infantil, </strong>as&iacute; como tambi&eacute;n para decorar un<strong> ambiente moderno y urbano</strong>.</p>\r\n\r\n<p>&bull; Si quer&eacute;s que un <strong>ambiente</strong>,&nbsp;el comedor (por ejemplo), parezca <strong>m&aacute;s amplio, el roble misionero es la mejor opci&oacute;n.</strong></p>\r\n', 9898, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(12, '9b90f0c48b', '019', 'PISO FLOTANTE ROBLE PATAGÓNICO AC4', '<p>&bull;&nbsp;Es un color en<strong> tonalidades gris&aacute;ceas,</strong> llegando casi a un blanco.&nbsp;<br />\r\n&bull; Los colores m&aacute;s claros representan la <strong>sencillez</strong> y la<strong> pureza</strong>,&nbsp;otorgan luz,</p>\r\n\r\n<p>&bull;<strong> Aporta luminosidad y sensaci&oacute;n de espacio</strong>, por lo que es ideal para espacios peque&ntilde;os para que parezcan m&aacute;s amplios,</p>\r\n\r\n<p>&bull; Ideal para el<strong> comedor,</strong> y otras habitaciones del hogar, como as&iacute; tambi&eacute;n para<strong>&nbsp;locales comerciales.</strong>&nbsp;</p>\r\n', 9945, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(13, '8da10fdb4d', '024', 'PISO FLOTANTE ROBLE REAL AC4', '<p>&bull; Es un <strong>color marr&oacute;n claro , </strong>el cual en sus<strong> vetas </strong>se&nbsp;<strong>&nbsp;</strong>mezcla con <strong>tonalidades naranjas</strong>, algo <strong>rojizas.</strong></p>\r\n\r\n<p>&bull;<strong>&nbsp;</strong>El roble real da un aspecto c&aacute;lido y natural que transmite una <strong>sensaci&oacute;n de lujo y nunca pasa de moda</strong></p>\r\n\r\n<p>&bull;<strong> </strong>Ideal para<strong> livings, dormitorios, restaurantes.&nbsp;</strong></p>\r\n\r\n<p>&bull;&nbsp;Da sensaci&oacute;n de <strong>amplitud </strong>al ambiente, brindando luminosidad al mismo.</p>\r\n\r\n<p>&bull; Ideal para combinar con mobiliarios en tonalidades m&aacute;s fuertes, <strong>otorgando confort, lujo, y sentido de permanencia.&nbsp;</strong></p>\r\n', 9945, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(14, '4bed816aae', '014', 'PISO FLOTANTE CEREZO ARGENTINO AC4', '<p>Color <strong>marr&oacute;n oscuro,</strong> con peque&ntilde;as <strong>vetas en marrones a&uacute;n m&aacute;s ocuros.&nbsp;</strong></p>\r\n\r\n<p>Es i<strong>deal para ambientes muy amplios, con mucha luz.&nbsp;</strong></p>\r\n\r\n<p>Se suele colocar en<strong> dormitorios matrimoniales, bibliotecas, livngs.</strong></p>\r\n\r\n<p>Da una <strong>percepci&oacute;n de formalidad, concentraci&oacute;n, elegancia, lujo.&nbsp;</strong></p>\r\n\r\n<p>Aconsejamos c<strong>ombinarlo con colores c&aacute;lidos, m&aacute;s claros o en las tonalidades tierras</strong> para generar un<strong> ambiente m&aacute;s serio, r&uacute;stico y estructurado</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', 995, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(16, '24aa8cdefe', NULL, 'PISO FLOTANTE ALABAMA AC4', '<p><strong>Alabama</strong> pose una textura <strong>similar&nbsp;al parquet</strong>, en<strong> tonalidades gris&aacute;ceas.</strong></p>\r\n\r\n<p>Es otro de los colores pertenecientes a nuestra<strong> colecci&oacute;n &quot;Premiun&quot;, </strong>uno de los <strong>&uacute;ltimos</strong> colores<strong> en salir al mercado</strong></p>\r\n\r\n<p>Es un<strong> color de tendencia</strong> enla vanguardia del mercado.&nbsp;</p>\r\n\r\n<p>Queda <strong>muy bien en oficinas, holl de entrada, sala de reuniones</strong>, como asi tambi&eacute;n en <strong>ambientes de un hogar</strong> como el living,&nbsp;el comedor.</p>\r\n\r\n<p>Estas tonalidades gris&aacute;ceas<strong> se combinan muy f&aacute;cilment</strong>e, con<strong> colores</strong> de paredes m&aacute;s <strong>fuertes</strong> como un rojo, fucsia, y<strong> textiles estampados</strong> en colores alegres para equilibrar lo sobrio del gris con alg&uacute;n color m&aacute;s calido permiento un <strong>mejor confort</strong> y un<strong> ambiente en armon&iacute;a.&nbsp;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 5996, 785, NULL, NULL, '5aa4d02c4f', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(17, '04b906cff8', NULL, 'PISO FLOTANTE CONCRETO AC4', '<p>Color<strong> gris</strong>, con textura<strong> similar al cemento.</strong></p>\r\n\r\n<p>Pertenece a la colecci&oacute;n premiun, ya que es<strong> nuevo en el mercado</strong>.&nbsp;</p>\r\n\r\n<p>Es un<strong> color de tendencia</strong>, que combina con todos los colores, y queda bien en cualquier ambiente.&nbsp;</p>\r\n\r\n<p>Da un<strong> aspecto m&aacute;s fabril e insdustrial</strong> por lo que es<strong> ideal para oficinas y&nbsp;comercios</strong>, generando un compos&eacute; con otros mobiliarios industriales que est&aacute;n de moda.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 2050, 785, NULL, NULL, '5aa4d02c4f', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(18, 'c6077a743d', '52', 'PISO FLOTANTE ABEDUL OTAWA', '<p>Conformado por<strong> Tablero MDF</strong>&nbsp;<strong>&nbsp;&nbsp;</strong></p>\r\n\r\n<p>Color <strong>marr&oacute;n claro,</strong> intercambi&aacute;ndose con <strong>manchas en tonalidades un poco m&aacute;s oscuras</strong>, con <strong>vetas en marr&oacute;n oscuro.</strong>&nbsp;</p>\r\n\r\n<p>Debido a su densidad, es <strong>ideal para dormitorios.&nbsp;</strong></p>\r\n\r\n<p>Dise&ntilde;ado&nbsp;y&nbsp; pensado para un <strong>dormitorio m&aacute;s cl&aacute;sico, hac&iacute;endolo m&aacute;s noble y confortable.&nbsp;</strong></p>\r\n\r\n<p>Queda muy bien acompa&ntilde;ado con <strong>mobiliarios en madera con textiles en estampados m&aacute;s vivos., </strong>generando un <strong>compos&eacute; &uacute;nico.&nbsp;</strong></p>\r\n\r\n<p>Genera un <strong>ambiente con mayor luz, otorgando amplitud</strong>, por lo que es ideal para espacios peque&ntilde;os y poco luminosos.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 2125, 726, NULL, NULL, 'a9d95c85bd', NULL, 14, NULL, NULL, NULL, NULL, '2', 'x m²', '186mm x 1195mm X 9mm ', '900', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL),
(19, 'dadc9381cc', NULL, 'ZÓCALO', '<p>Nuestros z&oacute;calos Top Floo<strong>r, son fabricados mediante un revestimiento de laminado decorativo</strong> del mismo dise&ntilde;o que los pisos.&nbsp;</p>\r\n\r\n<p><strong>No se resecan, no se descascaran, ni se rayan&nbsp;</strong></p>\r\n\r\n<p>Gracias a su<strong> espesor de 15 mm </strong>permite una <strong>mayor luz de dilataci&oacute;n</strong> entre el piso y el z&oacute;calo.&nbsp;</p>\r\n\r\n<p>Poseen en su <strong>dorso</strong>, una <strong>ranura para pasar el cable,</strong> quedando m&aacute;s est&eacute;tico.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 963, 199, NULL, NULL, '021d90d49c', NULL, 2, NULL, NULL, NULL, NULL, '3', 'x m', '3,04 ml X 75 mm de alto x 15 mm de espesor ', '700', NULL, NULL, NULL, NULL, '2019-05-10', NULL, NULL),
(26, 'bae7f9cac3', '001', 'MANTA BAJO PISO ', '<p>La manta bajo es una mata de&nbsp;<strong>&nbsp;espuma de polietileno ,</strong> viene en rollos, el rollo tiene 20 m x 1 m de ancho y tiene un espesor de 2 mm.</p>\r\n\r\n<p><strong>Es de vital importancia su colocaci&oacute;n&nbsp;</strong></p>\r\n\r\n<p>SUS FUNCIONES&nbsp;</p>\r\n\r\n<p><strong>Nivela&nbsp;</strong></p>\r\n\r\n<p>Nivela&nbsp;las posibles imperfecciones superficiales del piso ya existente, expandi&eacute;ndose en las depresiones y comprimi&eacute;ndose en las aristas.&nbsp;</p>\r\n\r\n<p>B<strong>arrera de humedad </strong></p>\r\n\r\n<p>Una de las caracter&iacute;sticas de la manta bajo piso&nbsp;es su<strong>&nbsp;impermeabilidad</strong>, esto nos da la seguridad que, estando selladas sus uniones concientemente, el piso Top Floor estar&aacute; protegido de la humedad que pueda surgir del piso antiguo</p>\r\n\r\n<p><strong>Aislante sonoro</strong></p>\r\n\r\n<p><strong>&nbsp;</strong>El uso de la manta bajo piso&nbsp;<strong>reduce considerablemente el ruido ocasionado por el propio uso</strong>, otorg&aacute;ndole adem&aacute;s una&nbsp;<strong>confortable sensaci&oacute;n al caminar.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1000, 41, NULL, NULL, NULL, NULL, NULL, 'manta bajo piso  islante ', 'La manta bajo es una mata de  espuma de polietileno , viene en rollos, el rollo tiene 20 m x 1 m de ancho y tiene un espesor de 2 mm. \r\n\r\nEs de vital importancia su colocación \r\n\r\nSUS FUNCIONES \r\n\r\nNivela \r\n\r\nNivela las posibles imperfecciones superficiales del piso ya existente, expandiéndose en las depresiones y comprimiéndose en las aristas. \r\n\r\nBarrera de humedad \r\n\r\nUna de las características de la manta bajo piso es su impermeabilidad, esto nos da la seguridad que, estando selladas sus uniones concientemente, el piso Top Floor estará protegido de la humedad que pueda surgir del piso antiguo\r\n\r\nAislante sonoro\r\n\r\n El uso de la manta bajo piso reduce considerablemente el ruido ocasionado por el propio uso, otorgándole además una confortable sensación al caminar.', NULL, NULL, '20', 'x m².', '20M X 1M  ', 'Alta ', NULL, NULL, NULL, NULL, '2019-07-04', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoria` (`categoria`),
  ADD KEY `subcategoria` (`subcategoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`cod`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `productos_ibfk_2` FOREIGN KEY (`subcategoria`) REFERENCES `subcategorias` (`cod`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
