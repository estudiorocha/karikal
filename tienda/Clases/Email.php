<?php namespace Clases;
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
//require '../vendor/autoload.php';

class Email
{
    private $asunto;
    private $receptor;
    private $emisor;
    private $mensaje;
    private $config;

    public function __construct()
    {
        $this->config = new Config();
    }

    public function set($atributo, $valor)
    {
        $this->$atributo = $valor;
    }

    public function get($atributo)
    {
        return $this->$atributo;
    }

    public function emailEnviar()
    {
        require "vendor/autoload.php";

        $pie = "<br/>Saludos <br/><div><br/>9 de Septiembre 1171 / X2400BSL <br/>0351-5542470/3564422741<br/><b style='margin-top:20px;'><b style='margin-top:20px;'>Horarios:</b><br/>Lunes y viernes: 9 AM a 6 PM<br/>Sábado y domingo: cerrado<br/><br/></div>";

        $mail = new PHPMailer(true);
        $mensaje = '<body style="background: #eee;min-height:700px;margin:0;padding:0"><div style="background: #fff;width:700px;margin:20px auto;padding:20px"><div><br/><img src=".LOGO." width="200"/><br/><hr/></div>' . $this->mensaje . ' ' . $pie . '<br/></div></body>';
        $emailData = $this->config->viewEmail();
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();
            $mail->CharSet = 'UTF-8';
            // Set mailer to use SMTP
            $mail->Host = $emailData['data']['smtp'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $emailData['data']['email'];                 // SMTP username
            $mail->Password = $emailData['data']['password'];                           // SMTP password
            $mail->SMTPSecure = $emailData['data']['smtp_secure'];                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $emailData['data']['puerto'];                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($this->emisor, TITULO);
            $mail->addAddress($this->receptor, '');     // Add a recipient

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $this->asunto;
            $mail->Body = $mensaje;
            $mail->AltBody = strip_tags($mensaje);

            $mail->send();
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function emailEnviarCurl()
    {
        require "../../vendor/autoload.php";

        $pie = "<br/>Saludos <br/><div style='font-size:12px'><br/><b style='margin-top:20px;'>Barrio Norte</b><br/>Av. Santa Fe 2074<br/>Tel: 011 4511 9696<br/><b style='margin-top:20px;'>Belgrano</b><br/>Av. Cabildo 2311<br/>Tel: 011 4786 7072<br/><b style='margin-top:20px;'>Horarios:</b><br/>Lunes a viernes: 8 AM a 10 PM<br/>Sábado: 9 AM a 8 PM<br/>Domingo: cerrado<br/></div>";

        $mail = new PHPMailer(true);
        $mensaje = '<body style="background: #eee;min-height:700px;margin:0;padding:0"><div style="background: #fff;width:700px;margin:20px auto;padding:20px"><div><br/><img src="http://c1590047.ferozo.com/assets/img/logo%20png-02.png" width="200"/><br/><hr/></div>' . $this->mensaje . ' ' . $pie . '<br/></div></body>';
        $emailData = $this->config->viewEmail();
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();
            $mail->CharSet = 'UTF-8';
            // Set mailer to use SMTP
            $mail->Host = $emailData['data']['smtp'];  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $emailData['data']['email'];                 // SMTP username
            $mail->Password = $emailData['data']['password'];                           // SMTP password
            $mail->SMTPSecure = $emailData['data']['smtp_secure'];                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $emailData['data']['puerto'];                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($this->emisor, TITULO);
            $mail->addAddress($this->receptor, '');     // Add a recipient

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $this->asunto;
            $mail->Body = $mensaje;
            $mail->AltBody = strip_tags($mensaje);

            $mail->send();
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }
}

?>