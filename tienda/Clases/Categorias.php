<?php

namespace Clases;

class Categorias
{

    //Atributos
    public $id;
    public $cod;
    public $titulo;
    public $area;

    private $con;
    private $subcategoria;
    private $imagenes;

    //Metodos
    public function __construct()
    {
        $this->con = new Conexion();
        $this->subcategoria = new Subcategorias();
        $this->imagenes = new Imagenes();
    }

    public function set($atributo, $valor)
    {
        if (strlen($valor)) {
            $valor = "'" . $valor . "'";
        } else {
            $valor = "NULL";
        }
        $this->$atributo = $valor;
    }

    public function get($atributo)
    {
        return $this->$atributo;
    }

    public function add()
    {
        $sql = "INSERT INTO `categorias`(`cod`, `titulo`, `area`) 
                  VALUES ({$this->cod},{$this->titulo},{$this->area})";
        $query = $this->con->sql($sql);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function edit()
    {
        $sql = "UPDATE `categorias` 
                  SET cod =  {$this->cod} ,
                      titulo =  {$this->titulo} ,
                      area =  {$this->area}  
                  WHERE `id`= {$this->id} ";
        $query = $this->con->sql($sql);
        return true;

    }


    public function delete()
    {
        $sql = "DELETE FROM `categorias` WHERE `cod`  = {$this->cod}";
        $query = $this->con->sqlReturn($sql);
        if (!empty($this->imagenes->list(array("cod={$this->cod}"), 'orden ASC', ''))) {
            $this->imagenes->cod = $this->cod;
            $this->imagenes->deleteAll();
        }

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function view()
    {
        $sql = "SELECT * FROM `categorias` WHERE cod = {$this->cod} LIMIT 1";
        $categorias = $this->con->sqlReturn($sql);
        $row = mysqli_fetch_assoc($categorias);
        $img = $this->imagenes->view($row['cod']);
        $sub = $this->subcategoria->list(array("categoria='" . $row['cod'] . "'"), '', '');
        $row_ = array("data" => $row, "subcategories" => $sub, "image" => $img);
        return $row_;
    }

    function list($filter, $order, $limit)
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }

        $sql = "SELECT * FROM `categorias` $filterSql  ORDER BY $orderSql $limitSql";
        $categorias = $this->con->sqlReturn($sql);
        if ($categorias) {
            while ($row = mysqli_fetch_assoc($categorias)) {
                $img = $this->imagenes->view($row['cod']);
                $sub = $this->subcategoria->list(array("categoria='" . $row['cod'] . "'"), '', '');
                $array[] = array("data" => $row, "subcategories" => $sub, "image" => $img);
            }
            return $array;
        }

    }
}
