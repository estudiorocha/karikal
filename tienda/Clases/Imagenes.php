<?php

namespace Clases;

class Imagenes
{

    //Atributos
    public $id;
    public $link;
    public $ruta;
    public $orden;
    public $cod;
    private $con;

    //Metodos
    public function __construct()
    {
        $this->con = new Conexion();
    }

    public function set($atributo, $valor)
    {
        if (!empty($valor)) {
            $valor = "'" . $valor . "'";
        } else {
            $valor = "NULL";
        }
        $this->$atributo = $valor;
    }

    public function get($atributo)
    {
        return $this->$atributo;
    }

    public function add()
    {
        $sql = "INSERT INTO `imagenes`(`ruta`, `cod`, `orden`) VALUES ({$this->ruta}, {$this->cod},0)";
        $query = $this->con->sql($sql);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }

    }

    public function edit()
    {
        $sql = "UPDATE `imagenes` SET ruta = {$this->ruta}, cod = {$this->cod} WHERE `id`={$this->id}";
        $query = $this->con->sql($sql);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }

    }

    public function editAllCod($cod)
    {
        $sql = "UPDATE `imagenes` SET cod = {$this->cod} WHERE `cod`='$cod'";
        $query = $this->con->sql($sql);
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }

    }

    public function delete()
    {
        $sql = "SELECT * FROM `imagenes` WHERE id = {$this->id}";
        $imagen = $this->con->sqlReturn($sql);
        while ($row = mysqli_fetch_assoc($imagen)) {
            $sqlDelete = "DELETE FROM `imagenes` WHERE `id` = '" . $row['id'] . "'";
            $query = $this->con->sqlReturn($sqlDelete);
            unlink("../" . $row["ruta"]);
        }
    }

    public function deleteAll()
    {
        $sql = "SELECT * FROM `imagenes` WHERE cod = {$this->cod} ORDER BY cod DESC";
        $imagen = $this->con->sqlReturn($sql);

        while ($row = mysqli_fetch_assoc($imagen)) {
            unlink("../" . $row["ruta"]);
        }

        $sqlDelete = "DELETE FROM `imagenes` WHERE cod = {$this->cod}";
        $query = $this->con->sql($sqlDelete);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function view($cod)
    {
        $sql = "SELECT * FROM `imagenes` WHERE cod = '$cod' ORDER BY id ASC";
        $imagenes = $this->con->sqlReturn($sql);
        $row = mysqli_fetch_assoc($imagenes);
        if ($row === NULL) {
            return $row;
        } else {
            return $row;
        }

    }


    function list($filter, $order, $limit)
    {
        $array = array();
        if (is_array($filter)) {
            $filterSql = "WHERE ";
            $filterSql .= implode(" AND ", $filter);
        } else {
            $filterSql = '';
        }

        if ($order != '') {
            $orderSql = $order;
        } else {
            $orderSql = "id DESC";
        }

        if ($limit != '') {
            $limitSql = "LIMIT " . $limit;
        } else {
            $limitSql = '';
        }

        $sql = "SELECT * FROM `imagenes` $filterSql  ORDER BY $orderSql $limitSql";
        $image = $this->con->sqlReturn($sql);

        if ($image) {
            while ($row = mysqli_fetch_assoc($image)) {
                $array[] = $row;
            }
            return $array;
        }

    }

    public function setOrder()
    {
        $sql = "UPDATE `imagenes` SET orden = {$this->orden} WHERE id = {$this->id}";
        $query = $this->con->sql($sql);

        if (!empty($query)) {
            return true;
        } else {
            return false;
        }

    }
}
