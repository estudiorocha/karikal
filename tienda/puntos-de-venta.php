<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
////Clases
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$contenido = new Clases\Contenidos();
//
$template->set("title", "Puntos de venta | " . TITULO);
$template->set("description", "Busca el lugar más cercano a tu ubicación.");
$template->set("keywords", "");
$template->set("body", "");
$template->themeInit();
?>
<!-- Page Title #4
    ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-90">
                    <div class="pull-left">
                        <h2>PUNTOS DE VENTA</h2>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>">INICIO</a>
                        </li>
                        <li class="active">PUNTOS DE VENTA</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>

<!-- Shortcode #6
============================================= -->
<section class="shortcode-6 text-center-xs pt-15 pb-15">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/d/embed?mid=zgEyO3EuYd7Q.kphtqvlxyYuI" width="100%" height="770"></iframe>
            </div>
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>
<?php
$template->themeEnd();
?>
