<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();
$template->set("title", "Dejanos tus datos | " . TITULO);
$template->set("description", "Dejanos tus datos");
$template->set("keywords", "Dejanos tus datos");
$template->set("favicon", FAVICON);
$template->themeInit();
$carrito = new Clases\Carrito();
$usuarios = new Clases\Usuarios();
$hub = new Clases\Hubspot();
$usuarioSesion = $usuarios->viewSession();

$cod_pedido = $_SESSION["cod_pedido"];
$tipo_pedido = $funciones->antihack_mysqli(isset($_GET["metodos-pago"]) ? $_GET["metodos-pago"] : '');
if ($tipo_pedido == '') {
    $funciones->headerMove(URL . "/carrito");
}
$error = '';
$error2 = '';
?>
<!-- Page Title #4
       ============================================= -->
<section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
    <div class="bg-section">
        <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="page-title title-4 pt-49 pb-84">
                    <div class="pull-left">
                        <h2>COMPRA N°: <?= $cod_pedido ?></h2>
                        <p>Llená el siguiente formulario para poder finalizar tu compra</p>
                    </div>
                    <ol class="breadcrumb pull-right text-right hidden-xs hidden-sm">
                        <li>
                            <a href="<?= URL ?>/carrito">CARRITO</a>
                        </li>
                        <li class="active">PEDIDO</li>
                    </ol>
                </div>
                <!-- .page-title end -->
            </div>
            <!-- .col-md-12 end -->
        </div>
        <!-- .row end -->
    </div>
    <!-- .container end -->
</section>

<div class="section-empty">
    <div class="container content pt-15">
        <div class="">
            <?php
            if (empty($usuarioSesion)) {
                ?>
                <div class="alert alert-success" role="alert">Si ya estás registrado, <a href="<?= URL . '/usuarios?link=pagar/' . $tipo_pedido ?>">iniciar sesión</a>.</div>
                <?php
                if (isset($_POST["registrarmeBtn"])) {
                    $error = 0;
                    $cod = substr(md5(uniqid(rand())), 0, 10);
                    $nombre = $funciones->antihack_mysqli(isset($_POST["nombre"]) ? $_POST["nombre"] : "");
                    $apellido = $funciones->antihack_mysqli(isset($_POST["apellido"]) ? $_POST["apellido"] : "");
                    $doc = $funciones->antihack_mysqli(isset($_POST["doc"]) ? $_POST["doc"] : "");
                    $email = $funciones->antihack_mysqli(isset($_POST["email"]) ? $_POST["email"] : "");
                    $password1 = $funciones->antihack_mysqli(isset($_POST["password1"]) ? $_POST["password1"] : "");
                    $password2 = $funciones->antihack_mysqli(isset($_POST["password2"]) ? $_POST["password2"] : "");
                    $direccion = $funciones->antihack_mysqli(isset($_POST["direccion"]) ? $_POST["direccion"] : "");
                    $localidad = $funciones->antihack_mysqli(isset($_POST["localidad"]) ? $_POST["localidad"] : "");
                    $provincia = $funciones->antihack_mysqli(isset($_POST["provincia"]) ? $_POST["provincia"] : "");
                    $telefono = $funciones->antihack_mysqli(isset($_POST["telefono"]) ? $_POST["telefono"] : "");
                    $invitado = $funciones->antihack_mysqli(isset($_POST["invitado"]) ? $_POST["invitado"] : '0');
                    $fecha = $funciones->antihack_mysqli(isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d"));

                    $factura = $funciones->antihack_mysqli(isset($_POST["factura"]) ? $_POST["factura"] : '');

                    $usuarios->set("nombre", $nombre);
                    $usuarios->set("apellido", $apellido);
                    $usuarios->set("doc", $doc);
                    $usuarios->set("email", $email);
                    $usuarios->set("password", $password1);
                    $usuarios->set("direccion", $direccion);
                    $usuarios->set("localidad", $localidad);
                    $usuarios->set("provincia", $provincia);
                    $usuarios->set("telefono", $telefono);
                    $usuarios->set("invitado", !empty($invitado) ? 0 : 1);
                    $usuarios->set("fecha", $fecha);
                    $usuarios->set("estado", 1);
                    $usuarios->set("minorista", 1);

                    $hub->set("nombre", $nombre);
                    $hub->set("apellido", $apellido);
                    $hub->set("email", $email);
                    $hub->set("direccion", $direccion);
                    $hub->set("telefono", $telefono);
                    $hub->set("celular", "-");
                    $hub->set("localidad", $localidad);
                    $hub->set("provincia", $provincia);
                    $hub->set("postal", "-");

                    $email_data = $usuarios->validateV2();
                    if ($email_data['status']) {
                        $cod = $email_data['data']['cod'];
                    } else {
                        $cod = substr(md5(uniqid(rand())), 0, 10);
                    }
                    $usuarios->set("cod", $cod);
                    if (!empty($factura)) {
                        $fact = '&fact=1';
                    } else {
                        $fact = '';
                    }
                    switch ($invitado) {
                        //checkbox marcado
                        case 1:
                            //si existe el email, edita
                            if ($email_data['status']) {
                                //pregunta si esta registrado
                                if ($email_data['data']['invitado'] == 0) {
                                    $error = "Ya existe un usuario registrado con este email.";
                                    $error2 = "<a href='" . URL . "/usuarios?link=pagar/" . $tipo_pedido . "'>Ingresar</a>";
                                } else {
                                    //si invitado es 1
                                    if ($password1 != $password2) {
                                        $error = "Error las contraseñas no coinciden.";
                                    } else {
                                        $usuarios->edit();
                                        $usuarios->set("password", $password1);
                                        $usuarios->set("email", $email);
                                        $usuarios->login();
                                        $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . $fact);
                                    }
                                }
                            } else {
                                //si no existe, agrega el usuario
                                if ($password1 != $password2) {
                                    $error = "Error las contraseñas no coinciden.";
                                } else {
                                    $hub->addContact();
                                    $usuarios->add();
                                    $usuarios->set("password", $password1);
                                    $usuarios->set("email", $email);
                                    $usuarios->login();
                                    $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . $fact);
                                }
                            }
                            break;
                        //checkbox desmarcado
                        default:
                            //si el email exite
                            if ($email_data['status']) {
                                //si el email tiene invitado 1
                                if ($email_data['data']['invitado'] == 1) {
                                    $usuarios->guestSession();
                                    $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . $fact);
                                } else {
                                    $error = "Ya existe un usuario registrado con este email.";
                                    $error2 = "<a href='" . URL . "/usuarios?link=pagar/" . $tipo_pedido . "'>Ingresar</a>";
                                }
                            } else {
                                //el email no existe
                                $hub->addContact();
                                $usuarios->firstGuestSession();
                                $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . $fact);
                            }
                            break;
                    }
                }
                ?>
                <div class="<?= empty($error) ? 'oculto' : 'alert alert-warning'?>" role="alert">
                    <?= !empty($error) ? $error : '' ?>
                    <?= !empty($error2) ? $error2 : '' ?>
                </div>
                <div class="col-md-12">
                    <form method="post" class="row">
                        <div class="row">
                            <input type="hidden" value="<?= $tipo_pedido ?>" name="metodos-pago"/>
                            <div class="col-md-6">Nombre:<br/>
                                <input class="form-control  mb-10" type="text"
                                       value="<?php echo isset($_POST["nombre"]) ? $_POST["nombre"] : '' ?>"
                                       placeholder="Escribir nombre" name="nombre" required/>
                            </div>
                            <div class="col-md-6">Apellido:<br/>
                                <input class="form-control  mb-10" type="text"
                                       value="<?php echo isset($_POST["apellido"]) ? $_POST["apellido"] : '' ?>"
                                       placeholder="Escribir apellido" name="apellido" required/>
                            </div>
                            <div class="col-md-12">Email:<br/>
                                <input class="form-control  mb-10" type="email"
                                       value="<?php echo isset($_POST["email"]) ? $_POST["email"] : '' ?>"
                                       placeholder="Escribir email" name="email" required/>
                            </div>
                            <div class="col-md-12">Teléfono:<br/>
                                <input class="form-control  mb-10" type="text"
                                       value="<?php echo isset($_POST["telefono"]) ? $_POST["telefono"] : '' ?>"
                                       placeholder="Escribir telefono" name="telefono" required/>
                            </div>
                            <div class="col-md-4">Provincia
                                <div class="input-group w-100">
                                    <select class="pull-right form-control h40" name="provincia" id="provincia" required>
                                        <option value="" selected disabled>Provincia</option>
                                        <?php $funciones->provincias() ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">Localidad
                                <div class="input-group w-100">
                                    <select class="form-control h40" name="localidad" id="localidad" required>
                                        <option value="" selected disabled>Localidad</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">Dirección:<br/>
                                <input class="form-control  mb-10" type="text"
                                       value="<?php echo isset($_POST["direccion"]) ? $_POST["direccion"] : '' ?>"
                                       placeholder="Escribir dirección" name="direccion" required/>
                            </div>
                            <label class="col-md-12 col-xs-12 mt-10 mb-10 crear text-uppercase" style="font-size:16px">
                                <input type="checkbox" name="invitado" id="ch-usuario" value="1" onchange="registerUser()">
                                ¿Deseas crear una cuenta de usuario y dejar tus datos grabados para la próxima compra?
                            </label>
                            <div class="col-md-6 col-xs-6 password" style="display: none;">Contraseña:<br/>
                                <input class="form-control  mb-10" type="password"
                                       value=""
                                       id="password-1"
                                       placeholder="Escribir password" name="password1"/>
                            </div>
                            <div class="col-md-6 col-xs-6 password" style="display: none;">Repetir Contraseña:<br/>
                                <input class="form-control  mb-10" type="password"
                                       value=""
                                       id="password-2"
                                       placeholder="Escribir repassword" name="password2"/>
                            </div>
                            <label class="col-md-12 col-xs-12 mt-10 mb-10 text-uppercase"  style="font-size:16px">
                                <input type="checkbox" id="c-factura" name="factura" value="1" onclick="facturaA()">
                                Solicitar FACTURA A
                            </label>
                            <div class="col-md-12 col-xs-12 factura" style="display: none;">CUIT:<br/>
                                <input class="form-control mb-10" type="number" id="doc"
                                       value="<?php echo isset($_POST["doc"]) ? $_POST["doc"] : '' ?>"
                                       placeholder="Escribir CUIT" name="doc"/>
                            </div>
                            <div class="col-md-12 col-xs-12 mb-50">
                                <input class="btn btn-success btn-block btn-lg" type="submit" value="¡Finalizar la compra!"
                                       name="registrarmeBtn"/>
                            </div>
                        </div>
                    </form>
                    <br>
                </div>
                <?php
            } else {
                ?>
                <div class="col-md-12">
                    <?php
                    if (isset($_POST["l-pagar"])) {

                        $factura = $funciones->antihack_mysqli(isset($_POST["factura"]) ? $_POST["factura"] : '');
                        switch ($factura) {
                            case 1:
                                $doc = $funciones->antihack_mysqli(isset($_POST["doc"]) ? $_POST["doc"] : '');
                                if (!empty($doc)) {
                                    $usuarios->set("cod", $_SESSION['usuarios']['cod']);
                                    $usuarios->editSingle("doc", $doc);
                                    $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . "&fact=1");
                                } else {
                                    $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido . "&fact=1");
                                }
                                break;
                            default:
                                $funciones->headerMove(URL . "/checkout/" . $cod_pedido . "/" . $tipo_pedido);
                                break;
                        }
                    }
                    ?>
                    <div class="alert alert-info" role="alert">
                        Comprueba si tus datos son correctos:
                    </div>
                    <div class="col-md-12">
                        <form method="post" class="row">
                            <div class="row">
                                <input type="hidden" value="<?= $tipo_pedido ?>" name="metodos-pago"/>
                                <div class="col-md-6">Nombre:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['nombre'] ?>"
                                           placeholder="Escribir nombre" name="nombre" required readonly/>
                                </div>
                                <div class="col-md-6">Apellido:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['apellido'] ?>"
                                           placeholder="Escribir apellido" name="apellido" required readonly/>
                                </div>
                                <div class="col-md-12">Email:<br/>
                                    <input class="form-control  mb-10" type="email"
                                           value="<?= $_SESSION['usuarios']['email'] ?>"
                                           placeholder="Escribir email" name="email" required readonly/>
                                </div>
                                <div class="col-md-12">Dirección:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['direccion'] ?>"
                                           placeholder="Escribir dirección" name="direccion" required readonly/>
                                </div>
                                <div class="col-md-6">Localidad:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['localidad'] ?>"
                                           placeholder="Escribir localidad" name="localidad" required readonly/>
                                </div>
                                <div class="col-md-6">Provincia:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['provincia'] ?>"
                                           placeholder="Escribir provincia" name="provincia" required readonly/>
                                </div>
                                <div class="col-md-12">Teléfono:<br/>
                                    <input class="form-control  mb-10" type="text"
                                           value="<?= $_SESSION['usuarios']['telefono'] ?>"
                                           placeholder="Escribir telefono" name="telefono" required readonly/>
                                </div>
                                <hr>
                                <?php
                                if (!empty($_SESSION['usuarios']['doc'])) {
                                    ?>
                                    <label class="col-md-12 col-xs-12 mt-10 mb-10" style="font-size:16px">
                                        <input type="checkbox" name="factura" value="1">
                                        Solicitar FACTURA A
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="col-md-12 col-xs-12 mt-10 mb-10" style="font-size:16px">
                                        <input type="checkbox" id="c-factura" name="factura" value="1" onclick="facturaA()">
                                        Solicitar FACTURA A
                                    </label>
                                    <div class="col-md-12 col-xs-12 factura" style="display: none;">CUIT:<br/>
                                        <input class="form-control  mb-10" type="number" id="doc"
                                               value="<?php echo isset($_POST["doc"]) ? $_POST["doc"] : '' ?>"
                                               placeholder="Escribir CUIT" name="doc"/>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="col-md-12 col-xs-12 mb-50">
                                    <input class="btn btn-success btn-block btn-lg" type="submit" value="¡Finalizar la compra!"
                                           name="l-pagar"/>
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php
$template->themeEnd();
?>
<script>
    function facturaA() {
        $('.factura').slideToggle();
        if ($('#c-factura').prop("checked")) {
            $('#doc').prop('required', true);
        } else {
            $('#doc').prop('required', false);
        }
    }

    function registerUser() {
        $('.password').slideToggle();
        if ($('#ch-usuario').prop("checked")) {
            $('#password-1').prop('required', true);
            $('#password-2').prop('required', true);
        } else {
            $('#password-1').prop('required', false);
            $('#password-2').prop('required', false);
        }
    }
</script>

