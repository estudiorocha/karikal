<?php
require_once "Config/Autoload.php";
Config\Autoload::runSitio();
$template = new Clases\TemplateSite();
$funciones = new Clases\PublicFunction();

$template->set("title", "Gracias por enviarnos un mensaje | " . TITULO);
$template->set("description", "Gracias por enviarnos tu mensaje, pronto responderemos");
$template->set("keywords", "");
$template->set("favicon", LOGO);
$template->themeInit();
//
?>
    <!-- Page Title #4
            ============================================= -->
    <section class="bg-overlay bg-overlay-gradient pb-0 pt-0">
        <div class="bg-section">
            <img src="<?= URL ?>/assets/images/page-title/3.jpg" alt="Background"/>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="page-title title-4 pt-49 pb-84">
                        <div class="pull-left">
                            <h2>Gracias!</h2>
                        </div>
                    </div>
                    <!-- .page-title end -->
                </div>
                <!-- .col-md-12 end -->
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </section>
    <div class="container">
        <div id="sns_wrapper" class="mt-15 mb-15">
            <div id="sns_content" class="wrap">
                <div class="container text-center pt-50">
                    <h1>¡GRACIAS POR CONTACTARTE CON NOSOTROS!</h1>
                    <!--<h4>En pocos minutos un operador se estará comunicando con vos.</h4>-->
                    <i class="fa fa-check-circle fs-50" style="font-size:80px !important;color:green;margin-top:50px;"></i>
                </div>
            </div>
        </div>
    </div>
<?php
$template->themeEnd();
?>