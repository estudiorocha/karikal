<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<title>Servicios :: Karikal</title>
</head>
<body>

	<?php include("inc/nav.inc.php") ?>
	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background02.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Nuestros</div>
				<div class="title">Servicios</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Servicios</li>
				</ol>
			</div> 
		</div> 
	</div>  
	<div class="section white no-padding-bottom">
		<div class="inner">
			<div class="container">
				<?php Traer_Contenidos("servicios") ?>				
			</div> 
		</div>
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>