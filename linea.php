<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<?php 
	$id = isset($_GET["id"]) ? $_GET["id"] : '';
	//$id = explode("/",$id);
	//$id = mb_strtoupper($id[1]);  
	$data = Portfolio_TraerPorCod($id);
	$title = "$id :: Karikal";
	$keywords = "";
	$description = "";
	$canonical = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$autor = "Karikal";
	$made = "karikal@karikal.com.ar";
	?>
	<title><?php echo $title; ?></title>

	<meta http-equiv="title" content="<?php echo $title; ?>" />

	<meta name="description" lang="es" content="<?php echo $description; ?>">
	<meta name="keywords" lang="es" content="<?php echo $keywords; ?>">
	<meta name="author" lang="es" content="<?php echo $autor; ?>">
	<link href="<?php echo BASE_URL ?>/img/logo.png" rel="Shortcut Icon">

	<link rev="made" href="<?php echo $made; ?>"> 
	<meta name="copyright" content="2017">
	<link rel="canonical" href="<?php echo $canonical; ?>"> 
	<meta name="revisit-after" content="15 days"> 
	<meta name="distribution" content="global"> 
	<meta name="robots" content="all"> 
	<meta name="rating" content="general"> 
	<meta name="content-language" content="es-ar"> 

	<meta name="DC.identifier" scheme="URI" content="<?php echo $canonical; ?>">
	<meta name="DC.format" scheme="IMT" content="text/html">
	<meta name="DC.coverage" content="Argentina"> 
	<meta name="DC.title" content="<?php echo $title; ?>" />
	<meta name="DC.subject" content="<?php echo $description; ?>">
	<meta name="DC.description" content="<?php echo $description; ?>">
	<meta name="DC.language" content="es-ar">

	<meta http-equiv="window-target" content="_top">   

	<meta name="robots" content="all" /> 

	<meta http-equiv="content-language" content="es-ES" />

	<meta name="google" content="notranslate" />

	<meta name="geo.region" content="AR-X" />
	<meta name="geo.placename" content="San Francisco" />
	<meta name="geo.position" content="-31.4242727;-62.0961843" />
	<meta name="ICBM" content="-31.4242727, -62.0961843" /> 

	<meta content="public" name="Pragma">
	<meta http-equiv="pragma" content="public">
	<meta http-equiv="cache-control" content="public"> 
</head>
<body>

	<?php include("inc/nav.inc.php") ?> 
	<div class="page-title" style="background-image: url('<?php echo  BASE_URL."/".$data["imagen1_portfolio"]; ?>');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Producto</div>
				<div class="title"><?php echo  $data["nombre_portfolio"]; ?></div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active"><?php echo  $data["nombre_portfolio"]; ?></li>
				</ol>
			</div> 
		</div> 
	</div>  
	<div class="section white no-padding-bottom"> 
		<div class="container modelo"> <br/>
			<br/>
			<?
			$contenido = $data["descripcion_portfolio"]; 
			echo  $contenido; 
			?>
		</div>
	</div> <!-- end .section -->

	<?php include("inc/footer.inc.php") ?> 
</body>
</html>