<div class="section white  no-padding-bottom">
	<div class="inner">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<h3><span>CLIENTES QUE</span>CONFÍAN</h3>
				</div> 
				<div class="col-sm-10">
					<div class="clients-slider"> 
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/burger.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/coca.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/univ.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/agustino.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/samsung.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/ciudad.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/quorum.jpg" class="img-responsive"  /></div>
						<div class="client"><img src="<?php echo BASE_URL; ?>/img/marcas/rock.jpg" class="img-responsive"  /></div>
					</div>
				</div> 
			</div>
			<div class="column-spacer"></div><br/><br/><br/>
			<div class="row">
				<div class="col-sm-4">
					<h3><span>Nuestras</span>Novedades</h3>
					<div class="blog-post-slider owl-carousel">
						<?php Notas_Read_Front_Index_Slide() ?>
					</div> 
				</div> 
				<div class="col-sm-4">
					<h3><span>Nuestros</span>Clientes dicen</h3>
					<div class="testimonial-slider owl-carousel">
						<?php Testimonio_Read_Front() ?> 
					</div> 
				</div> 
				<div class="column-spacer"></div>
				<div class="col-sm-4">
					<h3><span>Suscribite</span>Newsletter</h3>
					<div class="newsletter-form-wrapper">
						<form  method="get" id="mc-embedded-subscribe-form" class="newsletter-form">
							<input type="email" id="mce-EMAIL" name="EMAIL" placeholder="Ingresar email" />
							<button type="submit" id="mc-embedded-subscribe" name="subscribe">Ok</button>
						</form> 
						<p>Para suscribirse a nuestras noticias y últimas actualizaciones, ingrese su correo electrónico.</p>
					</div> 
				</div> 
			</div> 
			<div class="column-spacer"></div>			
		</div> 
	</div> 
</div> 
<!--
<div class="container">
	<div class="call-to-action clearfix">
		<div class="image"><img src="images/cta.png" alt="worker"></div>
		<div class="content">
			<h5>Lets Discuss The Project</h5>
			<p>Sedquis viverra enim. Vivamus aliqet rutrusm dui a varius.viverra enim. Vivamus aliqet rutrusm dui a varius.</p>
		</div> 
		<div class="cta-button"><a href="" class="button no-border white">Contact Us</a></div>	
	</div> 
</div>
-->
<div class="column-spacer"></div>			
<br/><br/>
<footer class="footer">
	<div class="container">
		<?php Traer_Contenidos("contacto") ?>
	</div> 
	<div class="bottom clearfix">
		<div class="container">
			<div class="copyright">Copyright &copy; 2017. Estudio Rocha & Asociados</div>
			<div class="social-icons">
				<a href=""><i class="fa fa-facebook"></i></a>
				<a href=""><i class="fa fa-twitter"></i></a>
				<a href=""><i class="fa fa-linkedin"></i></a>
				<a href=""><i class="fa fa-google-plus"></i></a>
			</div> 
		</div> 
	</div> 
</footer> 


<script src="<?php echo BASE_URL ?>/js/jquery-1.11.2.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/bootstrap.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/lightbox.js"></script>

<script src="<?php echo BASE_URL ?>/js/jquery.inview.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

<script src="<?php echo BASE_URL ?>/scripts/FlexSlider/jquery.flexslider-min.js"></script>

<script src="<?php echo BASE_URL ?>/js/owl.carousel.min.js"></script>

<script src="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/nivo-lightbox.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/isotope.pkgd.min.js"></script>
<script src="<?php echo BASE_URL ?>/js/imagesloaded.pkgd.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/jquery.waypoints.min.js"></script>
<script src="<?php echo BASE_URL ?>/js/jquery.counterup.min.js"></script>

<script src="<?php echo BASE_URL ?>/js/scripts.js"></script>
