<?php 
include("PHPMailer/class.phpmailer.php"); 
include("admin/dal/data.php"); 
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">	
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="<?php echo BASE_URL ?>/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Simple-Line-Icons-Webfont -->
<link href="<?php echo BASE_URL ?>/fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" rel="stylesheet">
<!-- FlexSlider -->
<link href="<?php echo BASE_URL ?>/scripts/FlexSlider/flexslider.css" rel="stylesheet">
<!-- Owl Carousel -->
<link href="<?php echo BASE_URL ?>/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo BASE_URL ?>/css/owl.theme.default.css" rel="stylesheet">
<!-- Nivo Lightbox -->
<link href="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/nivo-lightbox.css" rel="stylesheet">
<link href="<?php echo BASE_URL ?>/scripts/Nivo-Lightbox/themes/default/default.css" rel="stylesheet">
<!-- Style.css -->
<link href="<?php echo BASE_URL ?>/lightbox/lightbox.css" rel="stylesheet">
<link href="<?php echo BASE_URL ?>/css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="icon" href="<?php echo BASE_URL ?>/images/logo.png">

		<!-- Start of HubSpot Embed Code -->
		<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4368046.js"></script>
		<!-- End of HubSpot Embed Code -->

		<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '4a5e8e9b-12bd-4d0d-8a0b-420d07cd68ad', f: true }); done = true; } }; })();</script>