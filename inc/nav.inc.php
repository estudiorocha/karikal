<header class="header">
	<div class="top">
		<div class="container">
			<div class=" hidden-md hidden-lg">
				<br/>
			</div>
			<div class=" hidden-xs hidden-sm">
				<span class="item"><i class="icon-clock"></i>Lunes - Viernes : 9:00 - 18:00</span>
				<span class="item"><a href="mailto:karikal@karikal.com.ar"><i class="icon-envelope"></i>karikal@karikal.com.ar</a></span>
				<span class="item"><a href="tel:(0351) 5542470"><i class="icon-call-out"></i>  (0351) 5542470 (rot.) </a></span>
			</div>
		</div>
	</div>
	<div class="navigation">
		<div class="container clearfix">
			<div class="logo"><a href="<?php echo BASE_URL ?>/index.php"><img src="<?php echo BASE_URL ?>/images/logo.png" alt="Architech" class="img-responsive"></a></div>
			<nav class="main-nav">
				<ul class="list-unstyled">
					<li><a href="<?php echo BASE_URL ?>/index.php"><i class="fa fa-home"></i></a></li>
					<li><a href="<?php echo BASE_URL ?>/lineas.php">Materiales Construcción</a>
						<ul> 
							<?php Categoria_Read_Productos_Nav("construccion") ?>
						</ul>
					</li>
					<li><a href="<?php echo BASE_URL ?>/lineas.php">Materiales Muebles</a>
						<ul> 
							<?php Categoria_Read_Productos_Nav("muebles") ?>
						</ul>
					</li>
					<li><a href="<?php echo BASE_URL ?>/empresa.php">NOSOTROS</a></li>
					<li><a href="<?php echo BASE_URL ?>/servicios.php">servicios</a></li>
					<li>
						<a href="<?php echo BASE_URL ?>/novedades.php">Novedades</a>					 
					</li>
                    <li>
                        <a href="<?php echo BASE_URL ?>/tienda/tienda">COMPRAR ONLINE</a>
                    </li>
					<li><a href="<?php echo BASE_URL ?>/contacto.php"><i class="fa fa-envelope-o"></i></a></li>
				</ul>
			</nav>
			<a href="<?php echo BASE_URL ?>/" class="responsive-menu-open"><i class="fa fa-bars"></i></a>
		</div>
	</div>
</header>
<div class="responsive-menu">
	<a href="<?php echo BASE_URL ?>/" class="responsive-menu-close"><i class="icon-close"></i></a>
	<nav class="responsive-nav"></nav>
</div>
