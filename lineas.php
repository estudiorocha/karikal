<!DOCTYPE html>
<html lang="en">
<head>
	<?php include("inc/header.inc.php") ?>
	<title>Líneas de Producto :: Karikal</title>
</head>
<body>

	<?php include("inc/nav.inc.php") ?>
	<div class="page-title" style="background-image: url('<?php echo BASE_URL ?>/images/background03.jpg');">
		<div class="inner">
			<div class="container">
				<div class="sub-title">Nuestras</div>
				<div class="title">Líneas de Producto</div>
				<ol class="breadcrumb">
					<li><a href="">Home</a></li>
					<li class="active">Líneas de Producto</li>
				</ol>
			</div> 
		</div> 
	</div>  
	<div class="  white ">
		<div class="inner">
			<div class="container">
				<?php Traer_Contenidos("lineas") ?>			
				<div class="clearfix"></div>
				<h1 style="font-size: 20px">Materiales de Construcción<hr/></h1>				
				<div class="row"><?php Portfolio_Read_Front("construccion","") ?></div>
				<div class="clearfix"></div>
				<h1 style="font-size: 20px">Materiales de Muebles<hr/></h1>				
				<div class="row"><?php Portfolio_Read_Front("muebles","") ?></div>
			</div> 
		</div>
	</div> 
	<?php include("inc/footer.inc.php") ?>
</body>
</html>